# README #

Author: Ricardo Ragel (delatorre@us.es) 
Project: ARCAS (27/11/15)

**ARCAS FINAL DEMO**

La idea era realizar una simulación que reproduzca el experimento que se iba a realizar en el TestBed de CATEC. 
Un bonebreaker cogería una pieza y la soltaría en otro lugar, mientras 2 hummingbirds lo seguirían monitorizando 
las operaciones de ensamblaje. 

# Ultima version:

	/Copias de Seguridad WorkSpace Hydro/5

# Instalation:
	
	Linux: 	Ubuntu 12.04
	ROS:	Hydro
	Gazebo:	Gazebo multi-robot simulator, version 1.9.6

# Dependencias:

	ros-hydro-cmake-modules
	ros-hydro-message-to-tf
	ros-hydro-moveit-*
	ros-hydro-hector-*	(?)

# Añadir a .bashrc para dar la ruta de los modelos a Gazebo:	
	export GAZEBO_MODEL_PATH=~/catkin_ws/src/FunctionalLayerPackages/Control/ual/models:~/catkin_ws/src/FunctionalLayerPackages/Control/ual/Media/models:~/catkin_ws/src/FunctionalLayerPackages/Motion_Planning/bonebraker_moveit/Media/models/bars:~/catkin_ws/src/FunctionalLayerPackages/Motion_Planning/bonebraker_moveit/test:~/catkin_ws/src/FunctionalLayerPackages/Motion_Planning/bonebraker_moveit/Media/models/tables:~/catkin_ws/src/FunctionalLayerPackages/Motion_Planning/bonebraker_moveit/Media/models/stage

# Compilacion:
	Añadir FuntionalLayerPackages a ~/catkin_ws/src
	CATKIN ($ catkin_make)
	Error conocido: Posible dependencia cruzada entre 'quad_action_moveit_controller' y 'moveit_simple_controller_manager'. Sacar el segundo, compilar, volver a meterlo y compilar.

# Ejecución (ejemplo):
	
	Gazebo + Moveit + RViZ + Dummy Bars Controller:
	$ roslaunch bonebraker_moveit main_multi_bonebraker_1_final_demo.launch
	
	Ejecución de waypoints para el bonebreaker (UAV+arm) y los humminbirds
	$ rosrun bonebraker_moveit_interface final_demo

# Notas:

	-	Se utilizo la parte del Motion_Planning para multi robot, aunque solo se lanzara un bonebreaker. Veasé desde
		.../Motion_Planning/bonebraker_moveit/launch/multi_bonebraker/one_robot/final_demo/main_multi_bonebraker_1_final_demo.launch
	
	-	Para el bonebreaker se modifico con Yamnia el plugin de los sensores (
		.../FunctionalLayerPackages/Simulation/hector_quadrotor_gazebo_plugins/urdf/multi_quadrotor_sensors.urdf.xacro). Dado que ocurria
		un error en los hummingbirds surante simulación se usa para ellos el original.
	
	-	La parte para dos bonebreakers a la vez funciona perfectamente tanto en simulacion como en moveit, aunque no se probo a coger 
		ninguna barra. Puede probarse usando: 
			
				MULTI-ROBOT:	
								$ roslaunch bonebraker_moveit main_multi_bonebraker_2.launch
								$ rosrun bonebraker_moveit_interface test_multi_robot_2