/*************************************************************************
 *
 * FADA-CATEC
 * __________________
 *
 *  [2013] FADA-CATEC
 *  All Rights Reserved.
 *
 **/

#include <ros/ros.h>
#include <arcas_msgs/QuadStateEstimationWithCovarianceStamped.h>
#include <arcas_msgs/QuadControlReferencesStamped.h>

using namespace arcas_msgs;
using namespace std;

ros::Publisher pub;
ros::Time init_time = ros::Time(0);
ros::Time end_time = ros::Time(0);
double delay_average = 0;
int samples = 0;

/*
 * Last UAL State (for actual Position)
 * */
arcas_msgs::QuadStateEstimationWithCovarianceStamped lastUALState;
void UALStateCallback(const arcas_msgs::QuadStateEstimationWithCovarianceStamped::ConstPtr& s);

/*!
 * \brief The number of the UAV node. Its range will be 0 to 10.
 */
string uavID;
int uavIntID;

int main(int argc, char** argv)
{
    if (argc < 1)
    {
        cout << "This program has one input parameter.\n"<<
                "The input parameter is the number of the UAV." << endl;
        return -1;
    }
    // The UAV ID is stored in a global variable
    uavID="ual_";
    uavID.append(string(argv[1]));
    try
    {
        uavIntID = boost::lexical_cast<int>(argv[1]);
    }
    catch(boost::bad_lexical_cast const&)
    {
        perror("The first argument is not a number.");
        return 1;
    }

    string topicname;
    topicname = uavID;
    topicname.append("/quad_state_estimation");

    ros::init(argc,argv,"dynamic_tf");

    ros::NodeHandle n;

    ros::Subscriber subState = n.subscribe(topicname, 1, UALStateCallback);

    pub = n.advertise<arcas_msgs::QuadControlReferencesStamped>("/ual_1/quad_control_references", 0);

    arcas_msgs::QuadControlReferencesStamped test;
    test.quad_control_references.heading = 200;
    pub.publish(test);
    init_time = ros::Time::now();

    ros::AsyncSpinner spinner_(2);
    spinner_.start();
    while(ros::ok())
    {
        //If you want to execute code here, use asyncSpinner...
        usleep(100000);
    }

    std::cout << "\nFinishing test..." << std::endl;
    std::cout << "Samples: " << samples << std::endl;
    std::cout << "Delay average: " << delay_average/samples << " ns" << std::endl;
}

void UALStateCallback(const arcas_msgs::QuadStateEstimationWithCovarianceStamped::ConstPtr& s)
{
    lastUALState = *s;
    if(lastUALState.quad_state_estimation_with_covariance.altitude == 200){
        end_time = ros::Time::now();
        ros::Duration duration = end_time - init_time;
        delay_average += duration.toNSec();
        samples++;

        arcas_msgs::QuadControlReferencesStamped test;
        test.quad_control_references.heading = 200;
        pub.publish(test);
        init_time = ros::Time::now();
    }
}


