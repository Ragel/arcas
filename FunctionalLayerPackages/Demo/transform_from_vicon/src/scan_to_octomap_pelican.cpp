/*
 * scan_to_octomap.cpp
 *
 * Created on: 05/12/2013
 *      Author: Andreas Pfrunder
*/

// ROS Standard Includes
#include <ros/ros.h>
#include <iostream>

// Messagetypes
#include <arcas_msgs/QuadStateEstimationWithCovarianceStamped.h>
#include <arcas_msgs/QuadControlReferencesStamped.h>
#include <sensor_msgs/LaserScan.h>
#include <visualization_msgs/Marker.h>
#include <nav_msgs/Path.h>

// Conversion includes
#include<geometry_msgs/Quaternion.h>
#include <tf/transform_datatypes.h>

// TF publication includes
#include <tf/transform_broadcaster.h>

// Quaternion includes
#include <transform_from_vicon/rotateOP/Quaternion.h>

nav_msgs::Path path;

class Transformation
{
public:
    Transformation(std::string uav, uint id): uav(uav), id(id)
    {
        std::string temp_uav = uav;
        temp_uav.append("/quad_state_estimation");
        std::string temp_ref = uav;
        temp_ref.append("/quad_control_references");

        //Initialize subscribers
        quad_state_sub_ = n_.subscribe(temp_uav.c_str(),1,&Transformation::quadStateCallback, this);
        ref_sub_ = n_.subscribe(temp_ref.c_str(),1,&Transformation::refCallback, this);

        std::ostringstream os;
        os << "arrow_marker" << id;
        mark_pub_ = n_.advertise<visualization_msgs::Marker>(os.str().c_str(), 0);
        path_pub_ = n_.advertise<nav_msgs::Path>("path_msgs", 0);
    }

    void quadStateCallback(const arcas_msgs::QuadStateEstimationWithCovarianceStamped quad_state)
    {

        q_s2.fromEuler(quad_state.quad_state_estimation_with_covariance.attitude.roll,
                       quad_state.quad_state_estimation_with_covariance.attitude.pitch,
                       quad_state.quad_state_estimation_with_covariance.attitude.yaw,
                       rotateOp::TransformationTypes::EULER123);

        static tf::TransformBroadcaster br;
        tf::Transform transform;
        transform.setOrigin( tf::Vector3(quad_state.quad_state_estimation_with_covariance.position.x,
                                         quad_state.quad_state_estimation_with_covariance.position.y,
                                         quad_state.quad_state_estimation_with_covariance.position.z) );

        tf::Quaternion orientation;
        orientation.setX(q_s2.getX());
        orientation.setY(q_s2.getY());
        orientation.setZ(q_s2.getZ());
        orientation.setW(q_s2.getW());

        transform.setRotation(orientation);
        br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/map", uav));
        curr_pos_x = quad_state.quad_state_estimation_with_covariance.position.x;
        curr_pos_y = quad_state.quad_state_estimation_with_covariance.position.y;
        curr_pos_z = quad_state.quad_state_estimation_with_covariance.position.z;
    }

    void refCallback(const arcas_msgs::QuadControlReferencesStamped quad_ref)
    {
        visualization_msgs::Marker marker;
        marker.id = id;
        // Set the frame ID and timestamp.  See the TF tutorials for information on these.
        marker.header.frame_id = "/map";
        marker.header.stamp = ros::Time::now();

        // Set the namespace and id for this marker.  This serves to create a unique ID
        // Any marker sent with the same namespace and id will overwrite the old one
        marker.ns = "";
        marker.id = 0;

        // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
        marker.type = visualization_msgs::Marker::ARROW;

        // Set the marker action.  Options are ADD and DELETE
        marker.action = visualization_msgs::Marker::ADD;

        // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
        geometry_msgs::Point p;
        p.x = curr_pos_x;
        p.y = curr_pos_y;
        p.z = curr_pos_z;
        marker.points.push_back(p);
        geometry_msgs::Point p2;
        p2.x = quad_ref.quad_control_references.position_ref.x;
        p2.y = quad_ref.quad_control_references.position_ref.y;
        p2.z = quad_ref.quad_control_references.position_ref.z;
        marker.points.push_back(p2);

        // Set the scale of the marker -- 1x1x1 here means 1m on a side
        marker.scale.x = 0.1;
        marker.scale.y = 0.2;
        marker.scale.z = 0.0;

        // Set the color -- be sure to set alpha to something non-zero!
        marker.color.r = 0.0f;
        marker.color.g = 1.0f;
        marker.color.b = 0.0f;
        marker.color.a = 1.0;

        marker.lifetime = ros::Duration();

        // Publish the marker
        mark_pub_.publish(marker);

        path.header.frame_id = "/map";
        path.header.stamp = ros::Time::now();
        geometry_msgs::PoseStamped pose;
        pose.header.frame_id = "/map";
        pose.header.stamp = ros::Time::now();
        pose.pose.position.x = quad_ref.quad_control_references.position_ref.x;
        pose.pose.position.y = quad_ref.quad_control_references.position_ref.y;
        pose.pose.position.z = quad_ref.quad_control_references.position_ref.z;
        pose.pose.orientation.w = 1;
        pose.pose.orientation.x = 1;
        pose.pose.orientation.y = 1;
        pose.pose.orientation.z = 1;
        path.poses.push_back(pose);
        path_pub_.publish(path);
    }



private:
    ros::NodeHandle n_;
    ros::Subscriber quad_state_sub_;
    ros::Subscriber ref_sub_;
    ros::Publisher mark_pub_;
    ros::Publisher path_pub_;
    std::string uav;
    rotateOp::Quaternion q_s2;
    uint id;
    double curr_pos_x;
    double curr_pos_y;
    double curr_pos_z;
};


int main(int argc, char **argv)
{
    //Initialize ROS and specify the node name;
    ros::init(argc, argv, "scan_to_octomap");

    std::string uav;
    Transformation *publishers[argc-1];
    for(int i=1; i < argc; i++)
    {
        uav = "/ual_";
        uav.append(argv[i]);
        publishers[i-1] = new Transformation(uav, atoi(argv[i]));
    }

    ros::spin();
    ros::shutdown();

    return 0;
}
