/*************************************************************************
 *
 * FADA-CATEC
 * __________________
 *
 *  [2013] FADA-CATEC
 *  All Rights Reserved.
 *
 **/

#include <ros/ros.h>
#include <arcas_msgs/QuadStateEstimationWithCovarianceStamped.h>
#include <visualization_msgs/Marker.h>
// Conversion includes
#include<geometry_msgs/Quaternion.h>
#include <tf/transform_datatypes.h>
// TF publication includes
#include <tf/transform_broadcaster.h>
#include <transform_from_vicon/rotateOP/Quaternion.h>

//pixels
#define PI 3.1415926535897932384626433832795028841971693993751058f
using namespace arcas_msgs;
using namespace rotateOp;
using namespace std;

//Transformations
tf::Transform uavTF, cameraTF, cameraOpticTF, uavTFVis,
propeller1TFVis, propeller2TFVis, propeller3TFVis, propeller4TFVis;
double counter = 0;

/*BONEBRAKER 2
UAV_Vicon -> -0.075 z from arms.
It is added in the camera transformation
*/

double height = 0.030532 - 0.075;
double distanceCam = 0.441;
double angle = 24.334; //degrees


/*
 * Last UAL State (for actual Position)
 * */
arcas_msgs::QuadStateEstimationWithCovarianceStamped lastUALState;
void UALStateCallback(const arcas_msgs::QuadStateEstimationWithCovarianceStamped::ConstPtr& s);

/*!
 * \brief Visualization publisher
 */
ros::Publisher vis_pub;
ros::Publisher vis_propeller1_pub;
ros::Publisher vis_propeller2_pub;
ros::Publisher vis_propeller3_pub;
ros::Publisher vis_propeller4_pub;
ros::Publisher vis_asus_pub;

/*!
 * \brief The number of the UAV node. Its range will be 0 to 10.
 */
string uavID;
int uavIntID;

int main(int argc, char** argv)
{
    if (argc < 1)
    {
        cout << "This program has one input parameter.\n"<<
                "The input parameter is the number of the UAV." << endl;
        return -1;
    }

    // The UAV ID is stored in a global variable
    uavID="ual_";
    uavID.append(string(argv[1]));

    try
    {
        uavIntID = boost::lexical_cast<int>(argv[1]);
    }
    catch(boost::bad_lexical_cast const&)
    {
        perror("The first argument is not a number.");
        return 1;
    }

    string topicname;


    //Recopilamos parametros de ros para nuestro uav
    topicname = uavID;
    topicname.append("/quad_state_estimation");

    ros::init(argc,argv,"dynamic_tf");

    ros::NodeHandle n;

    ros::Subscriber subState = n.subscribe(topicname, 1, UALStateCallback);

    cameraTF.setOrigin(tf::Vector3(sin((45*PI)/180.0)*distanceCam, sin((45*PI)/180.0)*distanceCam, height));
    //Same that VICON
    //    Quaternion quatCamera;
    //    quatCamera.fromEuler(((180-24)*PI)/180.0,
    //                       0,
    //                       -135*PI/180.0,
    //                       TransformationTypes::EULER123);

    //    tf::Quaternion camAux(quatCamera.getX(), quatCamera.getY(), quatCamera.getZ(),quatCamera.getW());
    //    std::cout << "Quat X "<< camAux.inverse().getX() << std::endl;
    //    std::cout << "Quat Y "<< camAux.inverse().getY() << std::endl;
    //    std::cout << "Quat Z "<< camAux.inverse().getZ() << std::endl;
    //    std::cout << "Quat W "<< camAux.inverse().getW() << std::endl;
    //tf::Quaternion q_camera(camAux.inverse().getX(),camAux.inverse().getY(),camAux.inverse().getZ(),camAux.inverse().getW());

    tf::Quaternion q_camera(-0.3741,
                            -0.9031,
                            0.1947,
                            0.0807);
    cameraTF.setRotation(q_camera);

//First
//    cameraOpticTF.setOrigin(tf::Vector3(-0.01171, 0.07257, 0.14271));
//    tf::Quaternion q_cameraOptic(0.008715,
//                            0.01943,
//                            0.01207,
//                            0.9997);
//    cameraOpticTF.setRotation(q_cameraOptic);

    //Second calibration
    tf::Quaternion q_cameraOptic(0.000755,
                            -0.008544,
                            -0.002678,
                            0.999959);
    cameraOpticTF.setOrigin(tf::Vector3(0.00299, -0.06213, -0.129633));
    cameraOpticTF.setRotation(q_cameraOptic);

    vis_pub = n.advertise<visualization_msgs::Marker>(
                "visualization_quad", 0 );
    vis_propeller1_pub= n.advertise<visualization_msgs::Marker>(
                "visualization_propeller1", 0 );
    vis_propeller2_pub= n.advertise<visualization_msgs::Marker>(
                "visualization_propeller2", 0 );
    vis_propeller3_pub= n.advertise<visualization_msgs::Marker>(
                "visualization_propeller3", 0 );
    vis_propeller4_pub= n.advertise<visualization_msgs::Marker>(
                "visualization_propeller4", 0 );
    vis_asus_pub= n.advertise<visualization_msgs::Marker>(
                "visualization_asus", 0 );

    ros::AsyncSpinner spinner_(4);
    spinner_.start();
    ros::Rate rate(500);
    while(ros::ok())
    {
        //If you want to execute code here, use asyncSpinner...
        rate.sleep();
    }
}

void UALStateCallback(const arcas_msgs::QuadStateEstimationWithCovarianceStamped::ConstPtr& s)
{
    lastUALState = *s;
    Quaternion q_s2;
    q_s2.fromEuler(lastUALState.quad_state_estimation_with_covariance.attitude.roll ,
                   lastUALState.quad_state_estimation_with_covariance.attitude.pitch,
                   lastUALState.quad_state_estimation_with_covariance.attitude.yaw,
                   TransformationTypes::EULER123);

    static tf::TransformBroadcaster br;
    uavTF.setOrigin( tf::Vector3(lastUALState.quad_state_estimation_with_covariance.position.x,
                                 lastUALState.quad_state_estimation_with_covariance.position.y,
                                 lastUALState.quad_state_estimation_with_covariance.position.z) );
    tf::Quaternion orientation;
    orientation.setX(q_s2.getX());
    orientation.setY(q_s2.getY());
    orientation.setZ(q_s2.getZ());
    orientation.setW(q_s2.getW());
    uavTF.setRotation(orientation);

    br.sendTransform(tf::StampedTransform(uavTF, ros::Time::now(),
                                          "/map", "base_link"));

    br.sendTransform(tf::StampedTransform(cameraTF, ros::Time::now(),
                                          "base_link", "camera_link"));

    br.sendTransform(tf::StampedTransform(cameraOpticTF, ros::Time::now(),
                                          "camera_link", "optical_frame"));

    uavTFVis.setOrigin( tf::Vector3(lastUALState.quad_state_estimation_with_covariance.position.x,
                                    lastUALState.quad_state_estimation_with_covariance.position.y,
                                    lastUALState.quad_state_estimation_with_covariance.position.z - 0.28) );
    Quaternion q_s2_Vis;
    q_s2_Vis.fromEuler(lastUALState.quad_state_estimation_with_covariance.attitude.roll ,
                       lastUALState.quad_state_estimation_with_covariance.attitude.pitch,
                       lastUALState.quad_state_estimation_with_covariance.attitude.yaw, //See frame camera good
                       TransformationTypes::EULER123);
    tf::Quaternion orientationVis;
    orientationVis.setX(q_s2_Vis.getX());
    orientationVis.setY(q_s2_Vis.getY());
    orientationVis.setZ(q_s2_Vis.getZ());
    orientationVis.setW(q_s2_Vis.getW());
    uavTFVis.setRotation(orientationVis);
    br.sendTransform(tf::StampedTransform(uavTFVis, ros::Time::now(),
                                          "/map", "base_link_vis"));

    visualization_msgs::Marker marker;
    marker.header.frame_id = "base_link_vis";
    marker.header.stamp = ros::Time();
    marker.id = 0;
    marker.type = visualization_msgs::Marker::MESH_RESOURCE;
    marker.mesh_resource = "package://ual/Media/models/bonebraker/bonebraker.dae";
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = 1;
    marker.scale.y = 1;
    marker.scale.z = 1;
    marker.mesh_use_embedded_materials = true;
    marker.color.a = 1.0;
    marker.color.r = 0.05;
    marker.color.g = 0.15;
    marker.color.b = 0.45;
    vis_pub.publish( marker );

    /////////////////////// Propellers

    propeller1TFVis.setOrigin( tf::Vector3(0.4,
                                           0,
                                           0.285) );
    propeller2TFVis.setOrigin( tf::Vector3(0,
                                           0.42,
                                           0.285) );
    propeller3TFVis.setOrigin( tf::Vector3(-0.4,
                                           0,
                                           0.285) );
    propeller4TFVis.setOrigin( tf::Vector3(0,
                                           -0.4,
                                           0.285) );
    Quaternion q_s2_VisPropeller1;
    if(lastUALState.quad_state_estimation_with_covariance.position.z > 0.2){

        q_s2_VisPropeller1.fromEuler(0 ,
                                     0,
                                     0 + counter+ 1.5, //See frame camera good
                                     TransformationTypes::EULER123);
        counter+=0.1;
        if(counter > 3.14){
            counter = 0.0;
        }
    } else {
        q_s2_VisPropeller1.fromEuler(0 ,
                                     0,
                                     counter + 1.5, //See frame camera good
                                     TransformationTypes::EULER123);
    }
    tf::Quaternion orientationPropeller1Vis;
    orientationPropeller1Vis.setX(q_s2_VisPropeller1.getX());
    orientationPropeller1Vis.setY(q_s2_VisPropeller1.getY());
    orientationPropeller1Vis.setZ(q_s2_VisPropeller1.getZ());
    orientationPropeller1Vis.setW(q_s2_VisPropeller1.getW());
    propeller1TFVis.setRotation(orientationPropeller1Vis);
    propeller2TFVis.setRotation(orientationPropeller1Vis);
    propeller3TFVis.setRotation(orientationPropeller1Vis);
    propeller4TFVis.setRotation(orientationPropeller1Vis);
    br.sendTransform(tf::StampedTransform(propeller1TFVis, ros::Time::now(),
                                          "base_link_vis", "base_link_propeller1_vis"));
    br.sendTransform(tf::StampedTransform(propeller2TFVis, ros::Time::now(),
                                          "base_link_vis", "base_link_propeller2_vis"));
    br.sendTransform(tf::StampedTransform(propeller3TFVis, ros::Time::now(),
                                          "base_link_vis", "base_link_propeller3_vis"));
    br.sendTransform(tf::StampedTransform(propeller4TFVis, ros::Time::now(),
                                          "base_link_vis", "base_link_propeller4_vis"));
    visualization_msgs::Marker markerPropeller1;
    markerPropeller1.header.frame_id = "base_link_propeller1_vis";
    markerPropeller1.header.stamp = ros::Time();
    markerPropeller1.id = 0;
    markerPropeller1.type = visualization_msgs::Marker::MESH_RESOURCE;
    markerPropeller1.mesh_resource = "package://ual/Media/models/bonebraker/propellers.dae";
    markerPropeller1.action = visualization_msgs::Marker::ADD;
    markerPropeller1.pose.position.x = 0;
    markerPropeller1.pose.position.y = 0;
    markerPropeller1.pose.position.z = 0;
    markerPropeller1.pose.orientation.x = 0.0;
    markerPropeller1.pose.orientation.y = 0.0;
    markerPropeller1.pose.orientation.z = 0.0;
    markerPropeller1.pose.orientation.w = 1.0;
    markerPropeller1.scale.x = 1;
    markerPropeller1.scale.y = 1;
    markerPropeller1.scale.z = 0.65;
    markerPropeller1.color.a = 1.0;
    markerPropeller1.color.r = 0.5;
    markerPropeller1.color.g = 0.3;
    markerPropeller1.color.b = 0.3;
    markerPropeller1.mesh_use_embedded_materials = true;
    vis_propeller1_pub.publish( markerPropeller1 );

    visualization_msgs::Marker markerPropeller2 = markerPropeller1;
    markerPropeller2.header.frame_id = "base_link_propeller2_vis";
    vis_propeller2_pub.publish( markerPropeller2 );

    visualization_msgs::Marker markerPropeller3 = markerPropeller1;
    markerPropeller3.header.frame_id = "base_link_propeller3_vis";
    vis_propeller3_pub.publish( markerPropeller3 );

    visualization_msgs::Marker markerPropeller4 = markerPropeller1;
    markerPropeller4.header.frame_id = "base_link_propeller4_vis";
    vis_propeller4_pub.publish( markerPropeller4 );

    /////////////////////// Asus Camera
    visualization_msgs::Marker markerAsus;
    markerAsus.header.frame_id = "camera_link";
    markerAsus.header.stamp = ros::Time();
    markerAsus.id = 0;
    markerAsus.type = visualization_msgs::Marker::CUBE;
    // markerAsus.mesh_resource = "package://hector_sensors_description/meshes/asus_camera/asus_camera_simple.dae";
    markerAsus.action = visualization_msgs::Marker::ADD;
    markerAsus.pose.position.x = 0;
    markerAsus.pose.position.y = 0;
    markerAsus.pose.position.z = 0;
    markerAsus.pose.orientation.x = 0.0;
    markerAsus.pose.orientation.y = 0.0;
    markerAsus.pose.orientation.z = 0.0;
    markerAsus.pose.orientation.w = 1.0;
    markerAsus.scale.x = 0.05;
    markerAsus.scale.y = 0.05;
    markerAsus.scale.z = 0.05;
    markerAsus.color.a = 1.0;
    markerAsus.color.r = 0.0;
    markerAsus.color.g = 0.0;
    markerAsus.color.b = 0.0;
    markerAsus.mesh_use_embedded_materials = false;
    vis_asus_pub.publish( markerAsus );
}


