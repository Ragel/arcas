cmake_minimum_required(VERSION 2.8.3)
project(transform_from_vicon)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  roscpp
  arcas_msgs
  arcas_actions_msgs
  tf
  geometry_msgs
  sensor_msgs
)

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES ual
  CATKIN_DEPENDS arcas_actions_msgs arcas_msgs roscpp
#  DEPENDS
)

###########
## Build ##
###########

include_directories(
   include
)

add_executable(node src/scan_to_octomap_pelican.cpp)
add_dependencies(node arcas_msgs_generate_messages_cpp arcas_actions_msgs_generate_messages_cpp)
target_link_libraries(node ${catkin_LIBRARIES})

add_executable(dynamic_tf src/dynamic_tf.cpp)
add_dependencies(dynamic_tf arcas_msgs_generate_messages_cpp)
target_link_libraries(dynamic_tf ${catkin_LIBRARIES})
