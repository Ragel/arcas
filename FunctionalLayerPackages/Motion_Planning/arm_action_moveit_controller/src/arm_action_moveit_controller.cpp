#include <ros/ros.h>
#include <ros/time.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <actionlib/server/simple_action_server.h>
#include <control_msgs/FollowJointTrajectoryAction.h>

#include <arcas_msgs/ArmControlReferencesStamped.h>
#include <arcas_msgs/ArmStateEstimationStamped.h>

// Include the motion_action_handler extend arm client
#include <motion_action_handler/actions/extend_arm_action_client.h>

// Files
#include <iostream>
#include <fstream>

#define PI 3.14159265

#define TIME_SAFE_FACTOR 1.0	//[sec] Sleep between executed references safe factor
#define MIN_RAD_TO_REACH 0.01  	//[rad] Minimum angle error to set final position as reached

#define DEBUG false

#ifdef DEBUG
   // Crea un fichero de salida
   ofstream fs("arm_ref_results.txt");
   // Iteration
   int iter;
#endif

class ArmController
{
public:

	ArmController(std::string name, std::string robot_id) :
	as_(nh_, name, false),
	action_name_(name),
	aalExtensionActionWrapper(robot_id)
	{
		as_.registerGoalCallback(boost::bind(&ArmController::goalCB, this));
		as_.registerPreemptCallback(boost::bind(&ArmController::preemptCB, this));

		char pose_arm_topic[50];
		sprintf(pose_arm_topic, "/aal_%s/arm_state_estimation", robot_id.c_str());
		sub_pose_arm_ = nh_.subscribe(pose_arm_topic, 1, &ArmController::analysisCB, this);

		char move_arm_topic[50];
		sprintf(move_arm_topic, "/aal_%s/arm_control_references", robot_id.c_str());
		pub_move_arm_ = nh_.advertise<arcas_msgs::ArmControlReferencesStamped>(move_arm_topic, 1, this);

		has_active_goal_ = false;
		
	   //Joint names in gazebo.
	   joint_names_[0]="shoulder_y_arm_joint";
	   joint_names_[1]="shoulder_p_arm_joint";
	   joint_names_[2]="elbow_0_p_arm_joint";
	   joint_names_[3]="elbow_0_r_arm_joint";
	   joint_names_[4]="wirst_0_p_arm_joint";
	   joint_names_[5]="wirst_1_r_arm_joint";
	   
	   // Append robot id to joints
	   std::string _robot_id = "_";
	   _robot_id.append(robot_id);
	   joint_names_[0].append(_robot_id);
	   joint_names_[1].append(_robot_id);
	   joint_names_[2].append(_robot_id);
	   joint_names_[3].append(_robot_id);
	   joint_names_[4].append(_robot_id);
	   joint_names_[5].append(_robot_id);

		//~ ROS_INFO("Debug: %s", joint_names_[7].c_str());

		as_.start();
		
		ROS_INFO_STREAM("Node ready!");
	}

	~ArmController(void)
	{
	}

	void goalCB()
	{
		ROS_INFO_STREAM("Arm Action Controller: received new trajectory");
		if(has_active_goal_)
		{
			ROS_INFO_STREAM("Arm Action Controller: last trajectory stopped to execute the newest");
			// Marks the current goal as canceled.
			as_.setPreempted();
			has_active_goal_ = false;
		}
		
		// Accept the new trajectory
		i_ = 0;
		goal_ = as_.acceptNewGoal()->trajectory;
		
		// If arm is not extended -> Extend it and cancel trajectory
		if(arm_state.arm_state != arcas_msgs::ArmStateEstimation::EXTENDED) 
		{
			ROS_INFO_STREAM("Extracting arm extension() and cancelling trajectory");
			aalExtensionActionWrapper.extendArm();
			as_.setPreempted();
			has_active_goal_ = false;
		}
		
		/// IMPORTANT NOTE; If during the trajectory execution we need to read the position topic, the execution must be do by the topic callback --> analysisCB()
		/// Why? Because if goalCB() calling persist during the execution the ros::spin() is not called, so the topic is not read.
		// If arm is extended --> Execute the trajectory
		//~ else 
		//~ {
			//~ executeTrajectory();
		//~ }
	}

	void preemptCB()
	{
		ROS_INFO("%s: Preempted", action_name_.c_str());
		as_.setPreempted();
		has_active_goal_ = false;
	}

	void analysisCB(const arcas_msgs::ArmStateEstimationStamped& arm)
	{
		arm_state = arm.arm_state_estimation;
		
		if (!as_.isActive())
			return;
			
		executeTrajectoryStep();
		
	}

    //! Execute the next waypont of the active trajectory
	void executeTrajectoryStep()
	{
		has_active_goal_ = true;
		goal_size_ = goal_.points.size();

		// Goal trajectory loop
		if(i_ != goal_size_)
		{		
			ROS_INFO("Arm Action Controller: executing point %d/%d", i_+1,goal_size_);
			
			// Arm joints loop
			for(int j = 0; j<6; j++)
			{
				
				if(strcmp(goal_.joint_names[j].c_str(), joint_names_[0].c_str()) == 0)
				{
					arm_ref.arm_control_references.position_ref[0] = goal_.points[i_].positions[j];
					arm_ref.arm_control_references.velocity_ref[0] = goal_.points[i_].velocities[j];
				}
				if(strcmp(goal_.joint_names[j].c_str(), joint_names_[1].c_str()) == 0)
				{
					arm_ref.arm_control_references.position_ref[1] = -(goal_.points[i_].positions[j]+PI);
					arm_ref.arm_control_references.velocity_ref[1] = goal_.points[i_].velocities[j];
				}
				if(strcmp(goal_.joint_names[j].c_str(), joint_names_[2].c_str()) == 0)
				{
					arm_ref.arm_control_references.position_ref[2] = -(goal_.points[i_].positions[j]-PI);
					arm_ref.arm_control_references.velocity_ref[2] = goal_.points[i_].velocities[j];
				}
				if(strcmp(goal_.joint_names[j].c_str(), joint_names_[3].c_str()) == 0)
				{
					arm_ref.arm_control_references.position_ref[3] = goal_.points[i_].positions[j];
					arm_ref.arm_control_references.velocity_ref[3] = goal_.points[i_].velocities[j];
				}
				if(strcmp(goal_.joint_names[j].c_str(), joint_names_[4].c_str()) == 0)
				{
					arm_ref.arm_control_references.position_ref[4] = -goal_.points[i_].positions[j];
					arm_ref.arm_control_references.velocity_ref[4] = goal_.points[i_].velocities[j];
				}
				if(strcmp(goal_.joint_names[j].c_str(), joint_names_[5].c_str()) == 0)
				{
					arm_ref.arm_control_references.position_ref[5] = goal_.points[i_].positions[j] + PI/2.0;
					arm_ref.arm_control_references.velocity_ref[5] = goal_.points[i_].velocities[j];
				}
			}
			
			// Gripper referece = current
			arm_ref.arm_control_references.position_ref[6] = arm_state.position[6];
			
			// Get elapse time between trajectory points
			if(i_ != 0)
			{
				elapse_time = getElapseTime(goal_.points[i_], goal_.points[i_-1]);
			}
			
			// Console Debugging
			//~ printPositionInfo(i_);
			printCmdInfo(i_);
			ROS_INFO("Arm Action Controller: Time From Start: %f sec", goal_.points[i_].time_from_start.toNSec()/1.0e9);
			
			// Send reference to simulated robot-arm
			pub_move_arm_.publish(arm_ref);
			
			// Execution time --> Sleep
			ROS_INFO("Arm Action Controller: sleeping %f sec", TIME_SAFE_FACTOR * elapse_time);
			ros::Duration(TIME_SAFE_FACTOR * elapse_time).sleep();
			
			i_++;
		}
		
		// Wait and check arm reachs the final position
		if(i_ == goal_size_ && isCloseToFinalPosition() && has_active_goal_)
		{
			// Set goal as succees
			as_.setSucceeded();
			has_active_goal_ = false;
		}
	}
	
	//! Return the elapse time (sec) between two waypoints
	double getElapseTime(trajectory_msgs::JointTrajectoryPoint P, trajectory_msgs::JointTrajectoryPoint P_1)
	{  
		return (double)(P.time_from_start.toNSec() - P_1.time_from_start.toNSec())/1.0e9;
	}	
	
	//! Return true if arm is very close to the lastPosition
	bool isCloseToFinalPosition()
	{
		int target_angles_reached = 0;
		int lastPosIndex = goal_size_ - 1;

		if(fabs(arm_ref.arm_control_references.position_ref[0] - arm_state.position[0]) <= MIN_RAD_TO_REACH)
			target_angles_reached++;
		else
			ROS_INFO("Shoulder Y not reached: %f", fabs(arm_ref.arm_control_references.position_ref[0] - arm_state.position[0]));
			
		if(fabs(arm_ref.arm_control_references.position_ref[1] - arm_state.position[1]) <= MIN_RAD_TO_REACH)
			target_angles_reached++;
		else
			ROS_INFO("Shoulder P not reached: %f", fabs(arm_ref.arm_control_references.position_ref[1] - arm_state.position[1]));

		if(fabs(arm_ref.arm_control_references.position_ref[2] - arm_state.position[2]) <= MIN_RAD_TO_REACH)
			target_angles_reached++;
		else
			ROS_INFO("Elbow P not reached: %f", fabs(arm_ref.arm_control_references.position_ref[2] - arm_state.position[2]));
						
		if(fabs(arm_ref.arm_control_references.position_ref[3] - arm_state.position[3]) <= MIN_RAD_TO_REACH)
			target_angles_reached++;
		else
			ROS_INFO("Elbow R not reached: %f", fabs(arm_ref.arm_control_references.position_ref[3] - arm_state.position[3]));
			
		if(fabs(arm_ref.arm_control_references.position_ref[4] - arm_state.position[4]) <= MIN_RAD_TO_REACH)
			target_angles_reached++;
		else
			ROS_INFO("Wirst P not reached: %f", fabs(arm_ref.arm_control_references.position_ref[4] - arm_state.position[4]));

		if(fabs(arm_ref.arm_control_references.position_ref[5] - arm_state.position[5]) <= MIN_RAD_TO_REACH)
			target_angles_reached++;
		else
			ROS_INFO("Wirst R not reached: %f", fabs(arm_ref.arm_control_references.position_ref[5] - arm_state.position[5]));

	
		ROS_INFO("Angles reached: %d", target_angles_reached);
		
		if(target_angles_reached >= 6 )
		{
			ROS_INFO("All angles reached: [%f, %f, %f, %f, %f, %f]", fabs(arm_ref.arm_control_references.position_ref[0] - arm_state.position[0]), fabs(arm_ref.arm_control_references.position_ref[1] - arm_state.position[1]), fabs(arm_ref.arm_control_references.position_ref[2] - arm_state.position[2]), fabs(arm_ref.arm_control_references.position_ref[3] - arm_state.position[3]), fabs(arm_ref.arm_control_references.position_ref[4] - arm_state.position[4]), fabs(arm_ref.arm_control_references.position_ref[5] - arm_state.position[5]) );
			return true;
		}
		else
			return false;
	}

	void printPositionInfo(int k)
	{
		ROS_INFO_STREAM("Arm Action Controller: Estimated Position (" << k+1 << ")"
				<<"\n" <<  "Shoulder Y" << ": " << arm_state.position[0]
				<<"\n" <<  "Shoulder P" << ": " << arm_state.position[1]
				<<"\n" <<  "Elbow P" << ": " << arm_state.position[2]
				<<"\n" <<  "Elbow R" << ": " << arm_state.position[3]
				<<"\n" <<  "Wirst P" << ": " << arm_state.position[4]
				<<"\n" <<  "Wirts R" << ": " << arm_state.position[5]);
				
	}

	void printCmdInfo(int k)
	{
		ROS_INFO_STREAM("Arm Action Controller: ref to execute (" << k+1 << ")"
				<<"\n" <<  "Shoulder Y" << ": " << arm_ref.arm_control_references.position_ref[0] << "rad, " << arm_ref.arm_control_references.velocity_ref[0] << "rad/s"
				<<"\n" <<  "Shoulder P" << ": " << arm_ref.arm_control_references.position_ref[1] << "rad, " << arm_ref.arm_control_references.velocity_ref[1] << "rad/s"
				<<"\n" <<  "Elbow P" << ": " << arm_ref.arm_control_references.position_ref[2] << "rad, " << arm_ref.arm_control_references.velocity_ref[2] << "rad/s"
				<<"\n" <<  "Elbow R" << ": " << arm_ref.arm_control_references.position_ref[3] << "rad, " << arm_ref.arm_control_references.velocity_ref[3] << "rad/s"
				<<"\n" <<  "Wirst P" << ": " << arm_ref.arm_control_references.position_ref[4] << "rad, " << arm_ref.arm_control_references.velocity_ref[4] << "rad/s"
				<<"\n" <<  "Wirst R" << ": " << arm_ref.arm_control_references.position_ref[5] << "rad, " << arm_ref.arm_control_references.velocity_ref[5] << "rad/s");
		
	    if(DEBUG)
		{
		// Header
		fs << k + 1 << "\t"  << arm_ref.arm_control_references.position_ref[0] << "\t" << arm_ref.arm_control_references.position_ref[1] << "\t" << arm_ref.arm_control_references.position_ref[2] << "\t" << arm_ref.arm_control_references.position_ref[3] << "\t" << arm_ref.arm_control_references.position_ref[4] << "\t" << arm_ref.arm_control_references.position_ref[5] << "\t" << arm_ref.arm_control_references.velocity_ref[0] << "\t" << arm_ref.arm_control_references.velocity_ref[1] << "\t" << arm_ref.arm_control_references.velocity_ref[2] << "\t" << arm_ref.arm_control_references.velocity_ref[3] << "\t" << arm_ref.arm_control_references.velocity_ref[4] << "\t" << arm_ref.arm_control_references.velocity_ref[5] << endl; 
		}
	}

protected:

  ros::NodeHandle nh_;
  actionlib::SimpleActionServer<control_msgs::FollowJointTrajectoryAction> as_;
  std::string action_name_;
  int goal_size_, i_;
  ros::Time t_inicio;
  trajectory_msgs::JointTrajectory goal_;
  control_msgs::FollowJointTrajectoryResult result_;
  control_msgs::FollowJointTrajectoryFeedback feedback_;
  ros::Subscriber sub_pose_arm_;
  ros::Publisher pub_move_arm_;
  arcas_msgs::ArmStateEstimation arm_state;
  arcas_msgs::ArmControlReferencesStamped arm_ref;
  bool has_active_goal_;
  double elapse_time;
  AALExtensionActionWrapper aalExtensionActionWrapper;		// Arm extension ActionClient
  std::string joint_names_[6];
};

int main(int argc, char** argv)
{
   if (argc < 2)
   {
      std::cout << "This program need one input parameter.\n"<<
            "The first input parameter is the number of the UAV." << std::endl;
      return -1;
   }
	
  std::string node_name = "arm_action_moveit_controller_";
  node_name.append(argv[1]);	
  ros::init(argc, argv, node_name);
  
  std::string action_name = "arm_trajectory_action_";
  action_name.append(argv[1]);	  
  
  if(DEBUG)
  {
	// Header
	fs << "% Index\t" << "% Shoulder Y [rad]\t" << "Shoulder P [rad]\t" << "Elbow P [rad]\t" << "Elbow R [rad]\t" << "Wirst P [rad]\t" << "Wirst R [rad]\t" << "Shoulder Y [rad/s]\t" << "Shoulder P [rad/s]\t" << "Elbow P [rad/s]\t" << "Elbow R [rad/s]\t" << "Wirst P [rad/s]\t" << "Wirst R [rad/s]\t" << endl; 
  }
  
  ArmController arm_control(action_name, argv[1]);
  ros::spin();

  return 0;
}
