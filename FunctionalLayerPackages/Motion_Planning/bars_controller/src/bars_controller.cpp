/***********************************************************************
 * Bars Gazebo Controller Node
 * 
 * Summary: 		This node control the bars pose in Gazebo when one of these
 * --------			is attached for a UAV gripper. Also attach/detach the bars
 * 					to model as collision objects in moveit.
 * 
 * Prodecure:		1) 	Gazebo Bar Contact Sensors -- Gripper:
 * ----------			Receive contacts (contactsTopicCallback) and mark bars as
 * 						ATTACHED_TO_UAV or FREE if it's in contact with the gripper.
 * 
 * 					2)	Gazebo Get and Set Link State Position Topic:
 * 						Receive gripper pose (getLinkStatesTopicCallback) and set
 * 						this pose + offset (add_offset_from_wrist_1) to the bar if 
 * 						it's marked as ATTACHED_TO_UAV.
 * 			
 * 					3)	Moveit Attached Objects:
 * 						Moveit bars control (main) attaching or detaching the bars
 * 						from the robot model if ATTACHED_TO_UAV or FREE 
 * 						(attach_bar_to_the_gripper_in_moveit / 
 * 						detach_bar_from_the_gripper_in_moveit)
 * 
 * 					4)	Gazebo Bar Contact Sensors -- Bases:
 * 						If a bar is marked as FREE and it's in contact with a bar_base
 * 						(contactsTopicCallback), it's marked as ATTACHED_TO_BASE and so
 * 						the bar is posed artificially wrt the base in contact. Each base
 * 						has 3 differents floors and the is positioned wrt the 
 * 						base considering which is the bar in contact with.
 * 
 * 
 * Important notes: 
 * ---------------
 * 					A) 	It's several depening on the bonebreaker and bars URDF 
 *  					link names, So if the URDF is changed, this node must
 * 				   		be modified.
 * 					
 * 					B) 	For more than 3 bars or for multi-UAV maybe the easier way
 * 						to implements these is using the sprinf() and sscanf() as
 * 						to artificially bar positioning on the bases. 
 * 
 * Author: 	Ricardo Ragel de la Torre
 * Date:	19-May-15
 * ********************************************************************/

#include "ros/ros.h"

// Gazebo
#include "gazebo_msgs/ContactsState.h"
#include "gazebo_msgs/LinkStates.h"
#include "gazebo_msgs/LinkState.h"

// Transformations
#include <tf/LinearMath/Quaternion.h>
#include <tf//LinearMath/Transform.h>

// MoveIt!
//~ #include <moveit_msgs/PlanningScene.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit/planning_scene/planning_scene.h>

// Mesh class definitions
#include <geometric_shapes/shape_operations.h>

// Bars names definitions at Gazebo
#include <bars_controller/bars_gazebo_names.h>

// Bar N attaching state
int bar_1_state;
int bar_2_state;
int bar_3_state;
#define FREE 					0
#define ATTACHED_TO_UAV 		1
#define ATTACHED_TO_BASE		2
#define CYCLES_TO_SET_FREE 		10

// UAV that grasp the bar
int bar_1_attached_uav_id = 0;
int bar_2_attached_uav_id = 0;
int bar_3_attached_uav_id = 0;

// Global Topic Publisher to set the bar pose in Gazebo
ros::Publisher set_link_state_pub;

// Global Topic Publisher to attach bars to the robot in MoveIt!
ros::Publisher planning_scene_diff_pub;

// Vars to attach the objects
moveit_msgs::AttachedCollisionObject attached_object;
moveit_msgs::PlanningScene planning_scene_msg;

//! Attach the bar N (argument) to the robot in moveit
//! Note: 		
//!		This function sets the gripper and bar N collisions allowables.
//!		The collisions between the bar and the world and the rest of the 
//!		aren't allowables, and the planner will fail if that occurs.
//! Known bug:
//! 	The attached objects aren't coloured in RVIZ if it's in collision,
//!		but the planner fails correctly if that occurs.
void attach_bar_to_the_gripper_in_moveit(int bar_id)
{
  // Wirst_1_* string 
  char wirst_1_string[100];
  char claw_0_string[100];
  char claw_1_string[100];
  
  /* The id of the object */
  switch (bar_id)
  {
	case 1:
			ROS_INFO("Attaching bar %d to the robot %d in moveit", bar_id, bar_1_attached_uav_id);
			// Set the gripper strings (depends on uav id)
			sprintf (wirst_1_string, "wirst_1_%d", bar_1_attached_uav_id);
			sprintf (claw_0_string, "claw_0_%d", bar_1_attached_uav_id);
			sprintf (claw_1_string, "claw_1_%d", bar_1_attached_uav_id);
			// Define the attached object message
			attached_object.object.id = "bar_1";
			attached_object.link_name = wirst_1_string;
			/* The header must contain a valid TF frame*/
			attached_object.object.header.frame_id = wirst_1_string;			
			break;
	
	case 2:
			ROS_INFO("Attaching bar %d to the robot %d in moveit", bar_id, bar_2_attached_uav_id);
			sprintf (wirst_1_string, "wirst_1_%d", bar_2_attached_uav_id);
			sprintf (claw_0_string, "claw_0_%d", bar_2_attached_uav_id);
			sprintf (claw_1_string, "claw_1_%d", bar_2_attached_uav_id);
			// Define the attached object message
			attached_object.object.id = "bar_2";
			attached_object.link_name = wirst_1_string;
			/* The header must contain a valid TF frame*/
			attached_object.object.header.frame_id = wirst_1_string;
			break;
	
	case 3:
			ROS_INFO("Attaching bar %d to the robot %d in moveit", bar_id, bar_3_attached_uav_id);
			sprintf (wirst_1_string, "wirst_1_%d", bar_3_attached_uav_id);
			sprintf (claw_0_string, "claw_0_%d", bar_3_attached_uav_id);
			sprintf (claw_1_string, "claw_1_%d", bar_3_attached_uav_id);
			// Define the attached object message
			attached_object.object.id = "bar_3";
			attached_object.link_name = wirst_1_string;
			/* The header must contain a valid TF frame*/
			attached_object.object.header.frame_id = wirst_1_string;
			break;
		
	default: 
			//~ ROS_INFO("ERROR: bar_id (= %d) incorrect", bar_id);
			attached_object.object.id = "bar_1";
  }
  
  /* The allowed collisionable robot links */
  attached_object.touch_links.push_back(claw_0_string);
  attached_object.touch_links.push_back(claw_1_string);
  attached_object.touch_links.push_back(wirst_1_string);

  // Define the mesh to be attached
  shapes::Mesh* m = shapes::createMeshFromResource("package://bonebraker_moveit/Media/models/bars/horizontal_bar_mod_for_moveit.stl");
  shape_msgs::Mesh co_mesh;
  shapes::ShapeMsg co_mesh_msg;
  shapes::constructMsgFromShape(m,co_mesh_msg);
  co_mesh = boost::get<shape_msgs::Mesh>(co_mesh_msg);
  attached_object.object.meshes.push_back(co_mesh);

  geometry_msgs::Pose mesh_pose;
  tfScalar roll = M_PI/2.0;
  tfScalar pitch = 0.0;
  tfScalar yaw = -M_PI/2.0;
  tf::Quaternion q;
  q.setRPY(roll,pitch,yaw);
  mesh_pose.orientation.x = q.x();
  mesh_pose.orientation.y = q.y();
  mesh_pose.orientation.z = q.z();
  mesh_pose.orientation.w = q.w();
  mesh_pose.position.x =  0.05;
  mesh_pose.position.y = 0.0;
  mesh_pose.position.z =  0.0;
  attached_object.object.mesh_poses.push_back(mesh_pose);

  // Corresponding operation to be specified as an ADD operation
  attached_object.object.operation = attached_object.object.ADD;
  
  planning_scene_msg.is_diff = true;
  planning_scene_msg.robot_state.attached_collision_objects.push_back(attached_object);
  planning_scene_diff_pub.publish(planning_scene_msg);
}

//! Detach the bar N (argument) to the robot in moveit
void detach_bar_from_the_gripper_in_moveit(int bar_id)
{
  ROS_INFO("Detaching bar %d from the robot in moveit", bar_id);	
	
  /* First, define the DETACH object message*/
  switch (bar_id)
  {
	case 1:
			attached_object.object.id = "bar_1";
			break;
	
	case 2:
			attached_object.object.id = "bar_2";
			break;
	
	case 3:
			attached_object.object.id = "bar_3";
			break;
  	
  	default: 
			//~ ROS_ERROR("bar_id (= %d) incorrect", bar_id);
			attached_object.object.id = "bar_1";
  }
	
  //~ attached_object.link_name = WIRST_1_STRING;
  attached_object.object.operation = attached_object.object.REMOVE;
  planning_scene_msg.robot_state.attached_collision_objects.clear();
  planning_scene_msg.robot_state.attached_collision_objects.push_back(attached_object);
  planning_scene_diff_pub.publish(planning_scene_msg);
}

//! Modificate bar pose adding a offset (rotated) respect the wirst_1 link
//! Arg:
//!		The function should receive the LinkState msg equal to the wirst_1 pose
//!		to be modificated with the offset and to be used to set the bar pose
void add_offset_from_wrist_1(gazebo_msgs::LinkState &link_state)
{
	tf::Vector3 offset_from_wirst_1(0.05, 0.0, 0.0);	// bar grasp pose in the wirst_1 frame 
	tf::Quaternion q(link_state.pose.orientation.x, link_state.pose.orientation.y, link_state.pose.orientation.z, link_state.pose.orientation.w);
	tf::Transform transf;
	transf.setRotation(q);
	offset_from_wirst_1 = transf(offset_from_wirst_1);		
	link_state.pose.position.x = link_state.pose.position.x + offset_from_wirst_1.getX();
	link_state.pose.position.y = link_state.pose.position.y + offset_from_wirst_1.getY();
	link_state.pose.position.z = link_state.pose.position.z + offset_from_wirst_1.getZ();
}


//! Set the bar 'bar_id' artificially in Gazebo WRT the 'attached_base_id' 
//! being in contact with 'attached_base_floor_id'
void set_the_bar_on_the_base(int bar_id, int attached_base_id, int attached_base_floor_id)
{
	gazebo_msgs::LinkState link_state;	// Link state message to send to gazebo
	tf::Quaternion q;
		
	// Fill the bar link
	switch(bar_id)
	{
		case 1:
				link_state.link_name = BAR_1_LINK_STRING;
				break;
		case 2:
				link_state.link_name = BAR_2_LINK_STRING;
				break;
		case 3:
				link_state.link_name = BAR_3_LINK_STRING;
				break;
		
		default: 
				//~ ROS_ERROR("bar_id (= %d) incorrect", bar_id);
				;
	}
	
	// Fill the reference frame according to the based id where attach the bar
	char buff[100];
	sprintf (buff, "bar_bases::bar_base_%d_ref", attached_base_id);
	link_state.reference_frame = buff;
		
	switch(attached_base_floor_id)
	{
		case 1: 					
				// Set the bar pose in the base
				link_state.pose.position.x = 0.0;
				link_state.pose.position.y = 0.33;
				link_state.pose.position.z = 0.06;
				
				q.setRPY(0.0,M_PI_2,0.0);
				link_state.pose.orientation.x = q.x();
				link_state.pose.orientation.y = q.y();
				link_state.pose.orientation.z = q.z();
				link_state.pose.orientation.w = q.w();
				// Publish the link pose to Gazebo
				set_link_state_pub.publish(link_state);

				break;
		
		case 2:
				// Set the bar pose in the base
				link_state.pose.position.x = 0.33;
				link_state.pose.position.y = 0.0;
				link_state.pose.position.z = 0.06;
				
				// Publish the link pose to Gazebo
				q.setRPY(0.0,M_PI_2,-M_PI_2);
				link_state.pose.orientation.x = q.x();
				link_state.pose.orientation.y = q.y();
				link_state.pose.orientation.z = q.z();
				link_state.pose.orientation.w = q.w();
				set_link_state_pub.publish(link_state);

				break;
				
		case 3:
				ROS_ERROR("The base floor 3 has not been implemented yet!!");
				break;
				
		default:
				ROS_ERROR("The bases only have implemented 3 floors!!");
				;
	}
}


//! This topic receive the contactsState from Gazebo. Each msg is from one 
//! bar bumper sensor and it contains the links in contact.
//! If the link is the bar N --> Set the bar_N_state as attached if is in 
//! contact with one of the gripper finger, else is marked as not_attached 
//! (would be in contact with the gripper palm = wirts_1)
//! Note:
//! 	Added counters to not set the bar status as FREE for a instantaneous
//!    not contact.
void contactsTopicCallback(const gazebo_msgs::ContactsState msg)
{
  int states_size = msg.states.size();	// number of contacts in the received message
  int bar_id = 0;						// bar identification
  //~ int attached_uav_id = 0;				// uav			'' --> Passed to global
  int attached_base_id = 0;				// bar			''
  int attached_base_floor_id = 0;		// bar floor	'' 
  char uav_rest_of_msg[200];
  char claw_0_collision_string[200];
  char claw_1_collision_string[200];
  
  // If exist some contact..
  if(states_size>0)
  {
	  // For Debug
	  //~ ROS_INFO("States size: [%d]", states_size);
	  //~ for(int i = 0; i<states_size; i++)
	  //~ {
		//~ ROS_INFO("State %d --> Collision1_name: [%s]", i, msg.states[i].collision1_name.c_str());
		//~ ROS_INFO("State %d --> Collision2_name: [%s]", i, msg.states[i].collision2_name.c_str());
	  //~ }
	  
	  // Static counters to set a bar as FREE
	  static int free_counter_1 = 0;
	  static int free_counter_2 = 0;
	  static int free_counter_3 = 0;

	  // Object with wich the is in contact
	  std::string object_in_collision = msg.states[0].collision2_name.c_str();
	  
	  // Check what bar is in this received message (collision1_name field by default, although could be 
	  // in the collision2_name field, so the other object_in_collision was the first)
	  if(!strcmp(BAR_1_COL_STRING, msg.states[0].collision1_name.c_str()))
	  {
		bar_id = 1;
	  }
	  else if(!strcmp(BAR_1_COL_STRING, msg.states[0].collision2_name.c_str()))
	  {
		bar_id = 1;
		object_in_collision = msg.states[0].collision1_name.c_str();
	  }
	  if(!strcmp(BAR_2_COL_STRING, msg.states[0].collision1_name.c_str()))
	  {
		bar_id = 2;
	  }
	  else if(!strcmp(BAR_2_COL_STRING, msg.states[0].collision2_name.c_str()))
	  {
		  bar_id = 2;
		  object_in_collision = msg.states[0].collision1_name.c_str();
	  }
	  if(!strcmp(BAR_3_COL_STRING, msg.states[0].collision1_name.c_str()))
	  {
		bar_id = 3;
	  }
	  else if(!strcmp(BAR_3_COL_STRING, msg.states[0].collision2_name.c_str()))
	  {
		  bar_id = 3;
		  object_in_collision = msg.states[0].collision1_name.c_str();
	  }

	  // For this bar, set if it's attached or not if the contact is with a gripper link (collision2_name field)
	  switch(bar_id)
	  {
		  case 1:	
					//~ ROS_INFO("Bar 1 in contact");
					
					// Read all the contacts and check if the bar is in contact with the UAV gripper
					for(int i = 0; i<states_size; i++)
					{
						if( !strncmp( object_in_collision.c_str(), UAV_STRING, strlen(UAV_STRING) ) )
						{						
							// Get the uav id in contact with the bar
							sscanf(object_in_collision.c_str(), "uav_%d::%s", &bar_1_attached_uav_id, uav_rest_of_msg);
							// Set the uav id in the claws strings
							sprintf (claw_0_collision_string, "claw_0_%d::claw_0_%d_collision", bar_1_attached_uav_id, bar_1_attached_uav_id);
							sprintf (claw_1_collision_string, "claw_1_%d::claw_1_%d_collision", bar_1_attached_uav_id, bar_1_attached_uav_id);
							// Check if the contact is with the gripper
							if( !strncmp( uav_rest_of_msg, claw_0_collision_string, strlen(claw_0_collision_string) ) || !strncmp( uav_rest_of_msg, claw_1_collision_string, strlen(claw_1_collision_string) ) )
							{													
								ROS_INFO("Bar 1 in contact with uav %d gripper", bar_1_attached_uav_id);							
								bar_1_state = ATTACHED_TO_UAV;
								free_counter_1 = 0;
								
								break;
							}
							else
							{
								ROS_WARN("Bar 1 in contact with uav %d, but is not with the gripper", bar_1_attached_uav_id);							
								// If any contact is with a gripper links --> Start setting FREE the bar state
								// NOTE: (i == states_size -1) added to only execute this one time (for the last contact point and not for each contact point) if it isn't contact and is attached
								if(bar_1_state == ATTACHED_TO_UAV && (i == states_size -1))	
								{
									free_counter_1++;
									ROS_WARN("Attached bar 1 is going to be free (%d/%d)", free_counter_1, CYCLES_TO_SET_FREE);
									if(free_counter_1 == CYCLES_TO_SET_FREE)
										bar_1_state = FREE;
								}
							}	
						}
						else
						{
							// If any contact is with a gripper links --> Start setting FREE the bar state
							// NOTE: (i == states_size -1) added to only execute this one time (for the last contact point and not for each contact point) if it isn't contact and is attached
							if(bar_1_state == ATTACHED_TO_UAV && (i == states_size -1))	
							{
								free_counter_1++;
								ROS_WARN("Attached bar 1 is going to be free (%d/%d)", free_counter_1, CYCLES_TO_SET_FREE);
								if(free_counter_1 == CYCLES_TO_SET_FREE)
									bar_1_state = FREE;
							}
						}
					}
					
					// If the bar is not in contact with the UAV gripper (is FREE), check if it's in contact with any base
					if(bar_1_state == FREE)
					{
						for(int i = 0; i<states_size; i++)
						{
							
							//For debug ...
							//~ ROS_INFO("State %d --> Collision2_name: [%s]", i, object_in_collision.c_str());
							//~ ROS_INFO("strcmp Result: %d",strncmp(object_in_collision.c_str(), BAR_BASES_GROUP_STRING, strlen(BAR_BASES_GROUP_STRING)));
														
							if( !strncmp( object_in_collision.c_str(), BAR_BASES_GROUP_STRING, strlen(BAR_BASES_GROUP_STRING) ) )
							{	
								// Set bar 1 state as attached to a base
								bar_1_state = ATTACHED_TO_BASE;	
								
								// Get the base and the floor (3 per base) that the bar is in contact
								sscanf(object_in_collision.c_str(), "bar_bases::center_link::center_link_collision_bar_base_%d_floor_%d", &attached_base_id, &attached_base_floor_id);
								ROS_INFO("Bar 1 in contact with base %d floor %d, setting pose artificially...", attached_base_id, attached_base_floor_id);
								// Note: This msg is show only one time because to pass to FREE need to be first in ATTACHED_TO UAV state
								
								// Set the bar pose artificially in the base position using the set_link_state topic
								set_the_bar_on_the_base(bar_id, attached_base_id, attached_base_floor_id);
								
								break;
							}
						}
					}
					
					
					break;
					
		  case 2:	
					//~ ROS_INFO("Bar 2 in contact");
					for(int i = 0; i<states_size; i++)
					{
						if( !strncmp( object_in_collision.c_str(), UAV_STRING, strlen(UAV_STRING) ) )
						{						
							sscanf(object_in_collision.c_str(), "uav_%d::%s", &bar_2_attached_uav_id, uav_rest_of_msg);
							sprintf (claw_0_collision_string, "claw_0_%d::claw_0_%d_collision", bar_2_attached_uav_id, bar_2_attached_uav_id);
							sprintf (claw_1_collision_string, "claw_1_%d::claw_1_%d_collision", bar_2_attached_uav_id, bar_2_attached_uav_id);
							if( !strncmp( uav_rest_of_msg, claw_0_collision_string, strlen(claw_0_collision_string) ) || !strncmp( uav_rest_of_msg, claw_1_collision_string, strlen(claw_1_collision_string) ) )
							{													
								ROS_INFO("Bar 2 in contact with uav %d gripper", bar_2_attached_uav_id);							
								bar_2_state = ATTACHED_TO_UAV;
								free_counter_2 = 0;
								
								break;
							}
							else
							{
								ROS_WARN("Bar 2 in contact with uav %d, but is not with the gripper", bar_2_attached_uav_id);
								if(bar_2_state == ATTACHED_TO_UAV && (i == states_size -1))
								{
									free_counter_2++;
									ROS_WARN("Attached bar 2 is going to be free (%d/%d)", free_counter_2, CYCLES_TO_SET_FREE);
									if(free_counter_2 == CYCLES_TO_SET_FREE)
										bar_2_state = FREE;
								}		
							}	
						}
						else
						{
							if(bar_2_state == ATTACHED_TO_UAV && (i == states_size -1))
							{
								free_counter_2++;
								ROS_WARN("Attached bar 2 is going to be free (%d/%d)", free_counter_2, CYCLES_TO_SET_FREE);
								if(free_counter_2 == CYCLES_TO_SET_FREE)
									bar_2_state = FREE;
							}
						}
						
						// If the bar is not in contact with the UAV gripper (is FREE), check if it's in contact with any base
						if(bar_2_state == FREE)
						{
							for(int i = 0; i<states_size; i++)
							{							
								if( !strncmp( object_in_collision.c_str(), BAR_BASES_GROUP_STRING, strlen(BAR_BASES_GROUP_STRING) ) )
								{	
									// Set bar 2 state as attached to a base
									bar_2_state = ATTACHED_TO_BASE;	
									
									// Get the base and the floor (3 per base) that the bar is in contact
									sscanf(object_in_collision.c_str(), "bar_bases::center_link::center_link_collision_bar_base_%d_floor_%d", &attached_base_id, &attached_base_floor_id);
									ROS_INFO("Bar 2 in contact with base %d floor %d, setting pose artificially...", attached_base_id, attached_base_floor_id);
									// Note: This msg is show only one time because to pass to FREE need to be first in ATTACHED_TO UAV state
									
									// Set the bar pose artificially in the base position using the set_link_state topic
									set_the_bar_on_the_base(bar_id, attached_base_id, attached_base_floor_id);
									
									break;
								}
							}
						}
					}
					break;
		  case 3:	
					//~ ROS_INFO("Bar 3 in contact");
					for(int i = 0; i<states_size; i++)
					{
						if( !strncmp( object_in_collision.c_str(), UAV_STRING, strlen(UAV_STRING) ) )
						{						
							sscanf(object_in_collision.c_str(), "uav_%d::%s", &bar_3_attached_uav_id, uav_rest_of_msg);
							sprintf (claw_0_collision_string, "claw_0_%d::claw_0_%d_collision", bar_3_attached_uav_id, bar_3_attached_uav_id);
							sprintf (claw_1_collision_string, "claw_1_%d::claw_1_%d_collision", bar_3_attached_uav_id, bar_3_attached_uav_id);
							if( !strncmp( uav_rest_of_msg, claw_0_collision_string, strlen(claw_0_collision_string) ) || !strncmp( uav_rest_of_msg, claw_1_collision_string, strlen(claw_1_collision_string) ) )
							{													
								ROS_INFO("Bar 3 in contact with uav %d gripper", bar_3_attached_uav_id);							
								bar_3_state = ATTACHED_TO_UAV;
								free_counter_3 = 0;
								
								break;
							}
							else
							{
								ROS_WARN("Bar 3 in contact with uav %d, but is not with the gripper", bar_3_attached_uav_id);
								if(bar_3_state == ATTACHED_TO_UAV && (i == states_size -1))
								{								
									free_counter_3++;
									ROS_WARN("Attached bar 3 is going to be free (%d/%d)", free_counter_3, CYCLES_TO_SET_FREE);
									if(free_counter_3 == CYCLES_TO_SET_FREE)
										bar_3_state = FREE;
								}				
							}	
						}
						else
						{
							if(bar_3_state == ATTACHED_TO_UAV && (i == states_size -1))
							{								
								free_counter_3++;
								ROS_WARN("Attached bar 3 is going to be free (%d/%d)", free_counter_3, CYCLES_TO_SET_FREE);
								if(free_counter_3 == CYCLES_TO_SET_FREE)
									bar_3_state = FREE;
							}
						}
						// If the bar is not in contact with the UAV gripper (is FREE), check if it's in contact with any base
						if(bar_3_state == FREE)
						{
							for(int i = 0; i<states_size; i++)
							{							
								if( !strncmp( object_in_collision.c_str(), BAR_BASES_GROUP_STRING, strlen(BAR_BASES_GROUP_STRING) ) )
								{	
									// Set bar 3 state as attached to a base
									bar_3_state = ATTACHED_TO_BASE;	
									
									// Get the base and the floor (3 per base) that the bar is in contact
									sscanf(object_in_collision.c_str(), "bar_bases::center_link::center_link_collision_bar_base_%d_floor_%d", &attached_base_id, &attached_base_floor_id);
									ROS_INFO("Bar 3 in contact with base %d floor %d, setting pose artificially...", attached_base_id, attached_base_floor_id);
									// Note: This msg is show only one time because to pass to FREE need to be first in ATTACHED_TO UAV state
									
									// Set the bar pose artificially in the base position using the set_link_state topic
									set_the_bar_on_the_base(bar_id, attached_base_id, attached_base_floor_id);
									
									break;
								}
							}
						}
					}
					break;

		  default: 
					//~ ROS_ERROR("ERROR: bar_id (= %d) incorrect", bar_id);
					;
	  } 
  }
}

//! This Topic receive all link pose in Gazebo. If some bar/s is marked 
//! as ATTACHED_TO_UAV. then it is set at the wirst_1 grasp pose, reading the 
//! wirst_1 pose, adding a offset and sending that throw the 
//! "/gazebo/set_link_state" to the attached bar.
void getLinkStatesTopicCallback(const gazebo_msgs::LinkStates msg)
{
    gazebo_msgs::LinkState link_state;	// Link state message to send to a attached bar to situate this in the grasp pose
    int links_size = msg.name.size();	// Number of links in this message (already in Gazebo)
	int wirst_1_link_index = -1;		// Index of the wirst_1 link in the message
	char wirst_1_link_string[100];		// Wirst_1 (uav_id dependence) Gazebo string
	
	// For Debug 
	//~ for(int i = 0; i < links_size; i++)
	//~ {
		//~ ROS_INFO("Link %d : %s", i, msg.name[i].c_str());
	//~ }
	
	// If some bar is attached to the UAV set in the grasp pose
	if(bar_1_state == ATTACHED_TO_UAV)
	{
		ROS_INFO("Setting bar_1 pose artificially to the UAV %d gripper", bar_1_attached_uav_id);
		
		// Set the wirst_1 link string by the uav_id
		sprintf (wirst_1_link_string, "uav_%d::wirst_1_%d", bar_1_attached_uav_id, bar_1_attached_uav_id);
		
		// Search the wirst_1 link index
		for(int i=0; i<links_size; i++)
		{
			if(!strcmp(wirst_1_link_string, msg.name[i].c_str()))
			{
				wirst_1_link_index = i;
				
				break;
			}
		}
		
		// Fill the bar link and reference frame
		link_state.link_name = BAR_1_LINK_STRING;
		link_state.reference_frame = "world";
		
		// Set the bar pose in the grasp pose
		link_state.pose = msg.pose[wirst_1_link_index];
        add_offset_from_wrist_1(link_state);
		
		// Publish the link pose to Gazebo
		set_link_state_pub.publish(link_state);
	}
	
	if(bar_2_state == ATTACHED_TO_UAV)
	{
		ROS_INFO("Setting bar_2 pose artificially to the UAV  %d gripper", bar_2_attached_uav_id);

		sprintf (wirst_1_link_string, "uav_%d::wirst_1_%d", bar_2_attached_uav_id, bar_2_attached_uav_id);
		
		for(int i=0; i<links_size; i++)
		{
			if(!strcmp(wirst_1_link_string, msg.name[i].c_str()))
			{
				wirst_1_link_index = i;
				
				break;
			}
		}
		
		link_state.link_name = BAR_2_LINK_STRING;
		link_state.reference_frame = "world";
		
		link_state.pose = msg.pose[wirst_1_link_index];
        add_offset_from_wrist_1(link_state);
		
		set_link_state_pub.publish(link_state);
	}
	
	if(bar_3_state == ATTACHED_TO_UAV)
	{
		ROS_INFO("Setting bar_3 pose artificially to the UAV %d gripper", bar_3_attached_uav_id);
		
		sprintf (wirst_1_link_string, "uav_%d::wirst_1_%d", bar_3_attached_uav_id, bar_3_attached_uav_id);

		for(int i=0; i<links_size; i++)
		{
			if(!strcmp(wirst_1_link_string, msg.name[i].c_str()))
			{
				wirst_1_link_index = i;
				
				break;
			}
		}
		
		link_state.link_name = BAR_3_LINK_STRING;
		link_state.reference_frame = "world";
		
		link_state.pose = msg.pose[wirst_1_link_index];
        add_offset_from_wrist_1(link_state);
		
		set_link_state_pub.publish(link_state);
	}
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "bars_gazebo_controller");
  ros::NodeHandle n;
  
  // Bars contact sensors topic subscriber
  ros::Subscriber contact_sub = n.subscribe("/bars_contact_sensor", 10, contactsTopicCallback);
 
  // Gazebo get link states topic subscriber
  ros::Subscriber link_state_sub = n.subscribe("/gazebo/link_states", 1, getLinkStatesTopicCallback);
   
  // Gazebo set link states topic publisher
  set_link_state_pub = n.advertise<gazebo_msgs::LinkState>("/gazebo/set_link_state", 1);
  
  // Moveit attached objects topic publisher
  planning_scene_diff_pub = n.advertise<moveit_msgs::PlanningScene>("/planning_scene", 1);
  
  // Attaching flags
  bool bar_1_already_attached_in_moveit = false;
  bool bar_2_already_attached_in_moveit = false;
  bool bar_3_already_attached_in_moveit = false;
  
  // Init bars states as not attached
  bar_1_state = FREE;
  bar_2_state = FREE;
  bar_3_state = FREE;

  // Spinning: spin() is valid, but with the spinOnce() loop we can control easy the loop rate
  //~ ros::spin();
  ros::Rate r(10);
  while(ros::ok())
  {
	  // Set attached bars to moveit planning scene
	  if(bar_1_state == ATTACHED_TO_UAV)
	  {
		if(!bar_1_already_attached_in_moveit)
			attach_bar_to_the_gripper_in_moveit(1);
		bar_1_already_attached_in_moveit = true;
	  }
	  else
	  {
		if(bar_1_already_attached_in_moveit)
			detach_bar_from_the_gripper_in_moveit(1);
		bar_1_already_attached_in_moveit = false;
	  }
	  if(bar_2_state == ATTACHED_TO_UAV)
	  {
		if(!bar_2_already_attached_in_moveit)
			attach_bar_to_the_gripper_in_moveit(2);
		bar_2_already_attached_in_moveit = true;
	  }
	  else
	  {
		if(bar_2_already_attached_in_moveit)
			detach_bar_from_the_gripper_in_moveit(2);
		bar_2_already_attached_in_moveit = false;
	  }
	  if(bar_3_state == ATTACHED_TO_UAV)
	  {
		if(!bar_3_already_attached_in_moveit)
			attach_bar_to_the_gripper_in_moveit(3);
		bar_3_already_attached_in_moveit = true;
	  }
	  else
	  {
		if(bar_3_already_attached_in_moveit)
			detach_bar_from_the_gripper_in_moveit(3);
		bar_3_already_attached_in_moveit = false;
	  }
	  
	  // Read topics
	  ros::spinOnce();
	  
	  // Sleep
	  r.sleep();
  }

  return 0;
}
