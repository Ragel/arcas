#include "ros/ros.h"
#include "gazebo_msgs/ContactsState.h"
#include "gazebo_msgs/GetLinkState.h"
#include "gazebo_msgs/SetLinkState.h"

#define BAR_1_STRING "bar_1::bar_link::bar_link_collision"
#define BAR_2_STRING "bar_2::bar_link::bar_link_collision"
#define BAR_3_STRING "bar_3::bar_link::bar_link_collision"

#define GRIPPER_0_UAV_1_STRING "uav_1::claw_0::claw_0_collision"
#define GRIPPER_1_UAV_1_STRING "uav_1::claw_1::claw_1_collision"

// Bar 1 attaching state
int bar_1_state;
#define FREE 		0
#define ATTACHED 	1

// Gazebo service clients
ros::ServiceClient get_link_state_srv_client;
gazebo_msgs::GetLinkState get_link_state;
ros::ServiceClient set_link_state_srv_client;
gazebo_msgs::SetLinkState set_link_state;

/**
 * This topic receive the contactsState from Gazebo. Each msg is from one bar and it contains the links in contact
 */
void contactsTopicCallback(const gazebo_msgs::ContactsState msg)
{
  int states_size = msg.states.size();
  int bar_id = 0;
  if(states_size>0)
  {
	  // Debug
	  //~ ROS_INFO("States size: [%d]", states_size);
	  //~ for(int i = 0; i<states_size; i++)
	  //~ {
		//~ ROS_INFO("State %d --> Collision1_name: [%s]", i, msg.states[i].collision1_name.c_str());
		//~ ROS_INFO("State %d --> Collision2_name: [%s]", i, msg.states[i].collision2_name.c_str());
	  //~ }
	  
	  // Check what bar is it
	  if(!strcmp(BAR_1_STRING, msg.states[0].collision1_name.c_str()))
		bar_id = 1;
	  if(!strcmp(BAR_2_STRING, msg.states[0].collision1_name.c_str()))
		bar_id = 2;
	  if(!strcmp(BAR_3_STRING, msg.states[0].collision1_name.c_str()))
		bar_id = 3;
	  
	  switch(bar_id)
	  {
		  case 1:	
					//~ ROS_INFO("Bar 1 in contact");
					// Read all the contacts
					for(int i = 0; i<states_size; i++)
					{
						if( (!strcmp(GRIPPER_0_UAV_1_STRING, msg.states[0].collision2_name.c_str())) || (!strcmp(GRIPPER_1_UAV_1_STRING, msg.states[0].collision2_name.c_str())))
						{	
							//~ ROS_INFO("Bar 1 in contact with uav_1 gripper");
							bar_1_state = ATTACHED;
							
							get_link_state.request.link_name = "uav_1::wirst_1";
							get_link_state.request.reference_frame = "world";
							set_link_state.request.link_state.link_name = "bar_1::bar_link";
							set_link_state.request.link_state.reference_frame = "world";
							
							
							if(!get_link_state_srv_client.call(get_link_state))
							{
								ROS_ERROR("Error calling Gazebo Get Link State Service");
							}
							else
							{
								ROS_INFO("%f", get_link_state.response.link_state.pose.position.x);
								set_link_state.request.link_state.pose = get_link_state.response.link_state.pose;
								set_link_state.request.link_state.pose.position.z = get_link_state.response.link_state.pose.position.z - 0.05;
								if(!set_link_state_srv_client.call(set_link_state))
								{
									ROS_ERROR("Error calling Gazebo Set Link State Service");
								}
							}
							
							break;
						}
					}
					
					break;
					
		  case 2:	ROS_INFO("Bar 2 in contact");
					break;
		  case 3:	ROS_INFO("Bar 3 in contact");
					break;

		  default: ROS_INFO("ERROR!!!!");
	  } 
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "bars_gazebo_controller");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("/bars_contact_sensor", 1000, contactsTopicCallback);
  get_link_state_srv_client = n.serviceClient<gazebo_msgs::GetLinkState>("/gazebo/get_link_state");
  set_link_state_srv_client = n.serviceClient<gazebo_msgs::SetLinkState>("/gazebo/set_link_state"); 
  
  // Init vars
  bar_1_state = FREE;

  ros::spin();

  return 0;
}
