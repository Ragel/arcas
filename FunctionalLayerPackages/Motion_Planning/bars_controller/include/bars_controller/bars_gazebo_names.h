// Robot definitions
#define UAV_STRING	"uav_" 

// Bases for bars definitions
#define BAR_BASES_GROUP_STRING	"bar_bases"

// Bars definitions
#define BAR_1_LINK_STRING "bar_1::bar_link"
#define BAR_2_LINK_STRING "bar_2::bar_link"
#define BAR_3_LINK_STRING "bar_3::bar_link"

#define BAR_1_COL_STRING "bar_1::bar_link::bar_link_collision"
#define BAR_2_COL_STRING "bar_2::bar_link::bar_link_collision"
#define BAR_3_COL_STRING "bar_3::bar_link::bar_link_collision"
