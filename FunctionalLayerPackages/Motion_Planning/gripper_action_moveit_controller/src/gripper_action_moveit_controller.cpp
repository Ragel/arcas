#include <ros/ros.h>
#include <ros/time.h>
#include <actionlib/server/simple_action_server.h>
#include <control_msgs/GripperCommandAction.h>

#include <arcas_msgs/ArmControlReferencesStamped.h>
#include <arcas_msgs/ArmStateEstimationStamped.h>

#define PI 3.14159265

#define SLEEPING_TIME 2.0	//[sec] Sleep between executed references safe factor

class GripperController
{
public:

	GripperController(std::string name, std::string robot_id) :
	as_(nh_, name, false),
	action_name_(name)
	{
		as_.registerGoalCallback(boost::bind(&GripperController::goalCB, this));
		as_.registerPreemptCallback(boost::bind(&GripperController::preemptCB, this));


		char pose_arm_topic[50];
		sprintf(pose_arm_topic, "/aal_%s/arm_state_estimation", robot_id.c_str());
		sub_pose_arm_ = nh_.subscribe(pose_arm_topic, 1, &GripperController::getState, this);

		char move_arm_topic[50];
		sprintf(move_arm_topic, "/aal_%s/arm_control_references", robot_id.c_str());
		pub_move_arm_ = nh_.advertise<arcas_msgs::ArmControlReferencesStamped>(move_arm_topic, 1, this);

		has_active_goal_ = false;

		as_.start();
		
		ROS_INFO_STREAM("Node ready!");
	}

	~GripperController(void)
	{
	}

	void goalCB()
	{
		ROS_INFO_STREAM("Gripper Action Controller: received new trajectory");
		if(has_active_goal_)
		{
			ROS_INFO_STREAM("Gripper Action Controller: last trajectory stopped to execute the newest");
			// Marks the current goal as canceled.
			as_.setPreempted();
			has_active_goal_ = false;
		}
		
		// Accept the new trajectory
		i_ = 0;
		goal_ = as_.acceptNewGoal()->command;
		
	    // Execute the trajectory
		executeTrajectory();
	}

	void preemptCB()
	{
		ROS_INFO("%s: Preempted", action_name_.c_str());
		as_.setPreempted();
		has_active_goal_ = false;
	}

	void getState(const arcas_msgs::ArmStateEstimationStamped& arm)
	{
		arm_state = arm.arm_state_estimation;
	}

	void executeTrajectory()
	{
		has_active_goal_ = true;
		
		
		// ARM REFS = CURRENT POS
		arm_ref.arm_control_references.position_ref[0] = arm_state.position[0];	
		arm_ref.arm_control_references.position_ref[1] = arm_state.position[1];
		arm_ref.arm_control_references.position_ref[2] = arm_state.position[2];
		arm_ref.arm_control_references.position_ref[3] = arm_state.position[3];
		arm_ref.arm_control_references.position_ref[4] = arm_state.position[4];
		arm_ref.arm_control_references.position_ref[5] = arm_state.position[5];
		
		// GRIPPER
		arm_ref.arm_control_references.position_ref[6] = goal_.position;
		arm_ref.arm_control_references.velocity_ref[6] = 1.0;
		//~ ROS_INFO("GRIPPER: current = %f, ref= %f", arm_state.position[6], goal_.position);
			
		// Console Debugging
		//~ printPositionInfo();
		//~ printCmdInfo();
				
		// Send reference to simulated robot-arm
		pub_move_arm_.publish(arm_ref);
		
		// Execution time --> Sleep
		//~ ROS_INFO("Gripper Action Controller: sleeping %f sec", SLEEPING_TIME);
		ros::Duration(SLEEPING_TIME).sleep();
		
		// Set goal as succees
		as_.setSucceeded();
		has_active_goal_ = false;
	}
	
	void printPositionInfo()
	{
		ROS_INFO_STREAM("Arm Action Controller: Estimated Position:"
				<<"\n" <<  "Shoulder Y" << ": " << arm_state.position[0]
				<<"\n" <<  "Shoulder P" << ": " << arm_state.position[1]
				<<"\n" <<  "Elbow P" << ": " << arm_state.position[2]
				<<"\n" <<  "Elbow R" << ": " << arm_state.position[3]
				<<"\n" <<  "Wirst P" << ": " << arm_state.position[4]
				<<"\n" <<  "Wirts R" << ": " << arm_state.position[5]);
				
	}

	void printCmdInfo()
	{
		ROS_INFO_STREAM("Arm Action Controller: ref to execute:"
				<<"\n" <<  "Shoulder Y" << ": " << arm_ref.arm_control_references.position_ref[0] << "rad, " << arm_ref.arm_control_references.velocity_ref[0] << "rad/s"
				<<"\n" <<  "Shoulder P" << ": " << arm_ref.arm_control_references.position_ref[1] << "rad, " << arm_ref.arm_control_references.velocity_ref[1] << "rad/s"
				<<"\n" <<  "Elbow P" << ": " << arm_ref.arm_control_references.position_ref[2] << "rad, " << arm_ref.arm_control_references.velocity_ref[2] << "rad/s"
				<<"\n" <<  "Elbow R" << ": " << arm_ref.arm_control_references.position_ref[3] << "rad, " << arm_ref.arm_control_references.velocity_ref[3] << "rad/s"
				<<"\n" <<  "Wirst P" << ": " << arm_ref.arm_control_references.position_ref[4] << "rad, " << arm_ref.arm_control_references.velocity_ref[4] << "rad/s"
				<<"\n" <<  "Wirts R" << ": " << arm_ref.arm_control_references.position_ref[5] << "rad, " << arm_ref.arm_control_references.velocity_ref[5] << "rad/s");
	}
	



protected:

  ros::NodeHandle nh_;
  actionlib::SimpleActionServer<control_msgs::GripperCommandAction> as_;
  std::string action_name_;
  int i_;
  ros::Time t_inicio;
  control_msgs::GripperCommand goal_;
  ros::Subscriber sub_pose_arm_;
  ros::Publisher pub_move_arm_;
  arcas_msgs::ArmStateEstimation arm_state;
  arcas_msgs::ArmControlReferencesStamped arm_ref;
  bool has_active_goal_;
  double elapse_time;
};

int main(int argc, char** argv)
{
   if (argc < 2)
   {
      std::cout << "This program need one input parameter.\n"<<
            "The first input parameter is the number of the UAV." << std::endl;
      return -1;
   }
	
  std::string node_name = "gripper_action_moveit_controller_";
  node_name.append(argv[1]);	
  ros::init(argc, argv, node_name);
  
  std::string action_name = "gripper_commands_action_";
  action_name.append(argv[1]);

  GripperController gripper_control(action_name, argv[1]);
  ros::spin();

  return 0;
}
