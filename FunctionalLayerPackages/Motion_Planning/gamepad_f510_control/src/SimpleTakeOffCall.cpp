/*
 * gamepad_f510_control_node.cpp
 *
 *  Created on: 07/04/15
 *      Author: ricardo r.
*/

// ROS includes
#include <ros/ros.h>
#include <iostream>
#include <signal.h>


// Include the motion_action_handler land and take-off client
#include <motion_action_handler/actions/takeoff_action_client.h>

int main(int argc, char **argv)
{

   if (argc < 2)
   {
      std::cout << "This program need one input parameter.\n"<<
            "The first input parameter is the number of the UAV." << std::endl;
      return -1;
   }

   // The UAV ID is stored in a global variable
   std::string uavID="ual_";
   uavID.append(std::string(argv[1]));

   ros::init(argc, argv, "SimpleTakeOffCall");
   ros::NodeHandle n(uavID);


   // Initialize ActionClient
   TakeOffActionWrapper takeoffActionclient(uavID);


	sleep(2); 
	ROS_INFO_STREAM("Take-Off Action Sent");
	takeoffActionclient.takeOff();
	sleep(2); 

   return 0;
}
