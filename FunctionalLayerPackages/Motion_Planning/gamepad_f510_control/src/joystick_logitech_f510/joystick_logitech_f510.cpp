/*
 * joystick.cpp
 *
 *  Created on: Mar 22, 2012
 *      Author: catec
 */
#include<iostream>

#include "joystick_logitech_f510.h"


int CJoystick::open_joystick(char* joystick) {
	joystick_fd = open(joystick, O_RDONLY | O_NONBLOCK); /* read write for force feedback? */
	if (joystick_fd < 0)
		return joystick_fd;

	/* maybe ioctls to interrogate features here? */

	return joystick_fd;
}

int CJoystick::read_joystick_event(struct js_event *jse) {
	int bytes;

	bytes = read(joystick_fd, jse, sizeof(*jse));

	if (bytes == -1)
		return 0;

	if (bytes == sizeof(*jse))
		return 1;

	printf("Unexpected bytes from joystick:%d\n", bytes);

	return -1;
}

void CJoystick::close_joystick() {
	close(joystick_fd);
}

int CJoystick::get_joystick_status(struct wwvi_js_event* wjse) {
	int rc;
	struct js_event jse;
	if (joystick_fd < 0)
		return -1;

	// memset(wjse, 0, sizeof(*wjse));
	while ((rc = read_joystick_event(&jse) == 1)) {
		jse.type &= ~JS_EVENT_INIT; /* ignore synthetic events */
		if (jse.type == JS_EVENT_AXIS) {
			switch (jse.number) {

			case 5:
			    //~ printf("3x: %d\n",wjse->stick3_x);
				wjse->stick3_x = (((jse.value+32767)*2048)/-65534) + 1024;
				break;
			case 4:
				//~ printf("3y: %d\n",wjse->stick3_y);
				wjse->stick3_y = (((jse.value+32767)*2048)/-65534) + 1024;
				break;
			case 3:
				//~ printf("x: %d\n",wjse->stick_x);
				wjse->stick_x = (((jse.value+32767)*2048)/-65534) + 1024;
				break;
			case 1:
				//~ printf("y: %d\n",wjse->stick_y);
				wjse->stick_y = (((jse.value+32767)*2048)/-65534) + 1024;
				break;
			case 2:
				//~ printf("2x: %d\n",wjse->stick2_x);
				wjse->stick2_x = (((jse.value+32767)*2048)/-65534) + 1024;
				break;
			case 0:
				//~ printf("2y: %d\n",wjse->stick2_y);
				wjse->stick2_y = (((jse.value+32767)*2048)/-65534) + 1024;
				break;
			default:
				break;
			}
		} else if (jse.type == JS_EVENT_BUTTON) {
			if (jse.number < 10) {
				switch (jse.value) {
				case 0:
				case 1:
					wjse->button[jse.number] = jse.value;
					break;
				default:
					break;
				}
			}
		}
	}
	// printf("%d\n", wjse->stick1_y);
	return 0;
}
