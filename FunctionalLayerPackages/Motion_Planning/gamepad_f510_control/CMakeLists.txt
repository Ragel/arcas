cmake_minimum_required(VERSION 2.8.3)
project(gamepad_f510_control)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  motion_action_handler
  arcas_actions_msgs
  geometry_msgs
  roscpp
  actionlib
)

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES ual
  CATKIN_DEPENDS motion_action_handler arcas_actions_msgs arcas_msgs roscpp
#  DEPENDS system
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  include
  ${motion_action_handler_INCLUDE_DIRS}
  ${catkin_INCLUDE_DIRS}
)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-write-strings")

add_library(joystick_logitech_f510 src/joystick_logitech_f510/joystick_logitech_f510.cpp)

add_executable(gamepad_f510_control_node src/gamepad_f510_control_node.cpp)
add_dependencies(gamepad_f510_control_node arcas_msgs_generate_messages_cpp arcas_actions_msgs_generate_messages_cpp)
target_link_libraries(gamepad_f510_control_node ${catkin_LIBRARIES} joystick_logitech_f510)

add_executable(takeOffCall src/SimpleTakeOffCall.cpp)
add_dependencies(takeOffCall arcas_msgs_generate_messages_cpp arcas_actions_msgs_generate_messages_cpp)
target_link_libraries(takeOffCall ${catkin_LIBRARIES})

add_executable(landCall src/SimpleLandCall.cpp)
add_dependencies(landCall arcas_msgs_generate_messages_cpp arcas_actions_msgs_generate_messages_cpp)
target_link_libraries(landCall ${catkin_LIBRARIES})
