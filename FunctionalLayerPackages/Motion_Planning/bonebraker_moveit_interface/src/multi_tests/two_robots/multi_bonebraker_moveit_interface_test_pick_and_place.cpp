#include <ros/ros.h>
// Moveit - move group interface classes
#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
// Moveit- debug publishers
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
// Moveit - planning scene collision objects classes
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
// Mesh class definitions
#include <geometric_shapes/shape_operations.h>
// Quadrotor action clients (motion_action_handler) to land and take-off
#include <motion_action_handler/actions/land_action_client.h>
#include <motion_action_handler/actions/takeoff_action_client.h>
// Arm action client (motion_action_handler) to extend arm
#include <motion_action_handler/actions/extend_arm_action_client.h>
// Topic: Quad position references
#include <arcas_msgs/QuadControlReferencesStamped.h>
// Topic: Quad position estimation
#include <arcas_msgs/QuadStateEstimationWithCovarianceStamped.h>
// Topic: Arm position estimation
#include <arcas_msgs/ArmStateEstimationStamped.h>

// Plannings group names
#define QUADROTOR_1_PLANNING_GROUP 	"quadrotor_1"
#define QUADROTOR_2_PLANNING_GROUP 	"quadrotor_2"
#define ARM_1_PLANNING_GROUP 		"arm_1"
#define ARM_2_PLANNING_GROUP 		"arm_2"
#define GRIPPER_1_PLANNING_GROUP 	"gripper_1"
#define GRIPPER_2_PLANNING_GROUP 	"gripper_2"

// OMPL Planner Selection (only listed RRT types)
#define RRT_OMPL_PLANNER 			"RRTkConfigDefault"
#define RRT_CONNECT_OMPL_PLANNER 	"RRTConnectkConfigDefault"
#define RRT_STAR_OMPL_PLANNER 		"RRTstarkConfigDefault"

// Plan and Execute Blockable
#define BLOCKING		true
#define NON_BLOCKING	false


// UAV 1 zero positio
#define UAV_1_ZERO_POS_X 				-6.0	// base_link WRT bar_center
#define UAV_1_ZERO_POS_Y 				-6.0
#define UAV_1_ZERO_POS_Z 				1.0
#define UAV_1_ZERO_POS_YAW 				0.0

// UAV 2 zero position
#define UAV_2_ZERO_POS_X 				-6.0	// base_link WRT bar_center
#define UAV_2_ZERO_POS_Y 				-4.0
#define UAV_2_ZERO_POS_Z 				1.0
#define UAV_2_ZERO_POS_YAW 				0.0

// Pick bar 1 pose parameters
#define PICK_BAR_1_POS_X 					-7.0			// bar_center
#define PICK_BAR_1_POS_Y 					0.0
#define PICK_BAR_1_POS_Z 					1.10
#define PICK_BAR_1_POS_YAW 					M_PI

// Pick bar 2 pose parameters
#define PICK_BAR_2_POS_X 					-7.0			// bar_center
#define PICK_BAR_2_POS_Y 					-0.50
#define PICK_BAR_2_POS_Z 					1.10
#define PICK_BAR_2_POS_YAW 					M_PI

// Place base 1 pose parameters
#define PLACE_BASE_1_POS_X 				6.0			// center between bases
#define PLACE_BASE_1_POS_Y 				5.0
#define PLACE_BASE_1_POS_Z 				1.10
#define PLACE_BASE_1_POS_YAW 			0.0

// Place base 2 pose parameters
#define PLACE_BASE_2_POS_X 				6.0			// center between bases
#define PLACE_BASE_2_POS_Y 				4.5
#define PLACE_BASE_2_POS_Z 				1.10
#define PLACE_BASE_2_POS_YAW 			0.0

// UAV increment pos aproximation with respect the pick bar pos
#define UAV_APROX_POS_dX 				0.0	// base_link WRT bar_center
#define UAV_APROX_POS_dY 				0.0
#define UAV_APROX_POS_dZ 				0.75
#define UAV_APROX_POS_dYAW 				-M_PI/4.0

// Arm increment pos aproximation with respect the pick bar pos
#define ARM_APROX_POS_dX 				0.0	// wirst_1 WRT bar_center
#define ARM_APROX_POS_dY 				0.0
#define ARM_APROX_POS_dZ 				0.25
#define ARM_APROX_POS_dYAW 				(UAV_APROX_POS_dYAW - M_PI/4.0)	// by default is at PI/4 respect the UAV frame 
#define ARM_GRASP_POS_dZ 				0.060

// Count to set the UAV as stabilizated (1 seg if quad_state topic is at 100 Hz)
#define UAV_STABILIZATION_COUNT 		100

// Max planning attempts
#define UAV_MAX_PLANNING_ATTEMPTS 		10
#define ARM_MAX_PLANNING_ATTEMPTS 		10
#define GRIPPER_MAX_PLANNING_ATTEMPTS 	10

// UAV workspace [meters wrt the world frame]
#define UAV_WS_X_MIN		-10.0
#define UAV_WS_Y_MIN		-10.0
#define UAV_WS_Z_MIN		-0.5
#define UAV_WS_X_MAX		10.0
#define UAV_WS_Y_MAX		10.0
#define UAV_WS_Z_MAX		5.0


//! Callback to get the current state of the quadrotor
bool uav_1_pos_init;
arcas_msgs::QuadStateEstimationWithCovarianceStamped quad_1_state_estimation;
void quad_1_StateEstimationCallback(const arcas_msgs::QuadStateEstimationWithCovarianceStampedConstPtr &st)
{
   quad_1_state_estimation = *st;
   uav_1_pos_init = true;
   //~ ROS_INFO("Reading position!!");
}

//! Callback to get the current state of the quadrotor
bool uav_2_pos_init;
arcas_msgs::QuadStateEstimationWithCovarianceStamped quad_2_state_estimation;
void quad_2_StateEstimationCallback(const arcas_msgs::QuadStateEstimationWithCovarianceStampedConstPtr &st)
{
   quad_2_state_estimation = *st;
   uav_2_pos_init = true;
   //~ ROS_INFO("Reading position!!");
}

//! Callback to get the current state of the arm
arcas_msgs::ArmStateEstimation arm_state_1;
void arm_1_StateEstimationCallback(const arcas_msgs::ArmStateEstimationStamped& arm)
{
	arm_state_1 = arm.arm_state_estimation;
	//~ ROS_INFO("Reading position!!");
}

//! Callback to get the current state of the arm
arcas_msgs::ArmStateEstimation arm_state_2;
void arm_2_StateEstimationCallback(const arcas_msgs::ArmStateEstimationStamped& arm)
{
	arm_state_2 = arm.arm_state_estimation;
	//~ ROS_INFO("Reading position!!");
}

//! Check if the UAV is stabilizated (This function must be not blocking because the topic must be read)
bool checkUav_1_Stabilization()
{
	double roll = quad_1_state_estimation.quad_state_estimation_with_covariance.attitude.roll;
	double pitch = quad_1_state_estimation.quad_state_estimation_with_covariance.attitude.pitch;
	
	if(fabs(roll) <= 0.002 && fabs(pitch) <= 0.002)
		return true;
	else
		return false;
}

//! Check if the UAV is stabilizated (This function must be not blocking because the topic must be read)
bool checkUav_2_Stabilization()
{
	double roll = quad_2_state_estimation.quad_state_estimation_with_covariance.attitude.roll;
	double pitch = quad_2_state_estimation.quad_state_estimation_with_covariance.attitude.pitch;
	
	if(fabs(roll) <= 0.002 && fabs(pitch) <= 0.002)
		return true;
	else
		return false;
}

//! Plan and execute the path to a target pose for a UAV group [BLOCKING BY DEFAULT]
bool plan_and_execute_to_uav(moveit::planning_interface::MoveGroup &uav_group, double x_target, double y_target, double z_target, double yaw_target, bool blocking = BLOCKING)
{
  bool success = false;
  int planning_attempts = 0;
  moveit::planning_interface::MoveGroup::Plan plan;
  std::vector<double> target_values;
  
  // Try planning
  while(!success && planning_attempts <= UAV_MAX_PLANNING_ATTEMPTS)
  {
	  // Set planning start pose
	  uav_group.setStartStateToCurrentState();
	  
	  // First get the current set of joint values for the group.
	  uav_group.getCurrentState()->copyJointGroupPositions(uav_group.getCurrentState()->getRobotModel()->getJointModelGroup(uav_group.getName()), target_values);
		
	  // Set the goal pose = position + orientation
	  target_values[0] = x_target;			// X [m]
	  target_values[1] = y_target;			// Y [m]
	  target_values[2] = z_target;			// Z [m]
	  target_values[3] = yaw_target;		// yaw [rad]

	  // Plan for this group
	  uav_group.setJointValueTarget(target_values);
	  success = uav_group.plan(plan);

	  ROS_INFO("Visualizing plan for the uav group %s",success?"":"FAILED");
	  
	  planning_attempts++;
  }
  
  // If get a plan successfully, execute it
  if(success)
  {
	  // Request to execute the plan, blocking by default
	  if(blocking)
		success = uav_group.execute(plan);
	  else
		success = uav_group.asyncExecute(plan);
	  
	  // Check the result
	  ROS_INFO("Executed plan for the uav group %s",success?"":"FAILED");
	  if(!success)
	  {
		  return false;
	  }
	  
  }
  else
  {
	  ROS_ERROR("UAV planning failed %d times, Exiting test program...", UAV_MAX_PLANNING_ATTEMPTS);
	  return false;
  }
  
  return true;			
}


//! Plan and execute the path to a target pose for a arm group [BLOCKING BY DEFAULT]
bool plan_and_execute_to_arm(moveit::planning_interface::MoveGroup &arm_group, double x_target, double y_target, double z_target, double roll_target, double pitch_target, double yaw_target, bool blocking = BLOCKING)
{
  bool success = false;
  int planning_attempts = 0;
  geometry_msgs::Pose target_pose;
  moveit::planning_interface::MoveGroup::Plan plan;
  tf::Quaternion q;
  q.setRPY(roll_target, pitch_target, yaw_target);

  // Set planning target pose, /wirst_1 frame respect to the /world frame (note: by default if uav frame is parallel to world frame and at the picked_up arm pose, wirst_1 is turned PI/4 in yaw)
  while(!success && planning_attempts <= ARM_MAX_PLANNING_ATTEMPTS)
  {
	  // Set planning start pose
	  arm_group.setStartStateToCurrentState();

	  //~ printf("QUATERNION: X: %f, Y: %f, Z: %f, W: %f\n", q.x(), q.y(), q.z(), q.w());
	  target_pose.orientation.x = q.x();
	  target_pose.orientation.y = q.y();
	  target_pose.orientation.z = q.z();
	  target_pose.orientation.w = q.w();
	  target_pose.position.x = x_target;
	  target_pose.position.y = y_target;
	  target_pose.position.z = z_target;
	  arm_group.setPoseTarget(target_pose);
	  
	  // Plan for this group
	  success = arm_group.plan(plan);
	  // Check result
	  ROS_INFO("Visualizing plan for the arm group (pose goal) %s",success?"":"FAILED");    
  	
  	  planning_attempts++;
  }

  if(success)
  {
	  // Request to execute the plan, blocking by default
	  if(blocking)
		success = arm_group.execute(plan);
	  else
		success = arm_group.asyncExecute(plan);
	  
	  // Check the result
	  ROS_INFO("Executed for the arm group %s",success?"":"FAILED");    
	  // If we want both plan and execute --> arm_group.move(); 
	  if(!success)
	  {
		  return false;
	  }
  }
    else
  {
	  ROS_ERROR("Arm planning failed %d times, Exiting test program...", ARM_MAX_PLANNING_ATTEMPTS);
	  return false;
  }
  
  return true;
}
  
//! Plan and execute the path to open a gripper group [BLOCKING BY DEFAULT]
bool open_gripper(moveit::planning_interface::MoveGroup &gripper_group, bool blocking = BLOCKING)
{
  bool success = false;
  int planning_attempts = 0;
  moveit::planning_interface::MoveGroup::Plan plan;
  std::vector<double> target_values;
  
  while(!success && planning_attempts <= GRIPPER_MAX_PLANNING_ATTEMPTS)
  {
	  // Set planning start pose
	  gripper_group.setStartStateToCurrentState();
	  
	  // First get the current set of joint values for the group.
	  gripper_group.getCurrentState()->copyJointGroupPositions(gripper_group.getCurrentState()->getRobotModel()->getJointModelGroup(gripper_group.getName()), target_values);
		
	  // Set the goal pose (open)
	  target_values[0] = 0.79;		// X [m]
	  target_values[1] = -0.79;		// Y [m]
		
	  // Plan for this group
	  gripper_group.setJointValueTarget(target_values);
	  success = gripper_group.plan(plan);

	  ROS_INFO("Visualizing plan for the gripper group %s",success?"":"FAILED");
	  
	  planning_attempts++;
  }

  if(success)
  {
	  // Request to execute the plan, blocking by default
	  if(blocking)
		success = gripper_group.execute(plan);
	  else
		success = gripper_group.asyncExecute(plan);
	  
	  // Check the result
	  ROS_INFO("Executed plan for the gripper group %s",success?"":"FAILED");    
	  if(!success)
	  {
		  return false;
	  }
  }
  else
  {
	  ROS_ERROR("Gripper planning failed %d times, Exiting test program...", GRIPPER_MAX_PLANNING_ATTEMPTS);
	  return false;
  }
  
  return true;
}

//! Plan and execute the path to close a gripper group [BLOCKING BY DEFAULT]
bool close_gripper(moveit::planning_interface::MoveGroup &gripper_group, bool blocking = BLOCKING)
{
  bool success = false;
  int planning_attempts = 0;
  moveit::planning_interface::MoveGroup::Plan plan;
  std::vector<double> target_values;
  
  while(!success && planning_attempts <= GRIPPER_MAX_PLANNING_ATTEMPTS)
  {
	  // Set planning start pose
	  gripper_group.setStartStateToCurrentState();
	  
	  // First get the current set of joint values for the group.
	  gripper_group.getCurrentState()->copyJointGroupPositions(gripper_group.getCurrentState()->getRobotModel()->getJointModelGroup(gripper_group.getName()), target_values);
		
	  // Set the goal pose (open)
	  target_values[0] = 0.0;		// X [m]
	  target_values[1] = 0.0;		// Y [m]
		
	  // Plan for this group
	  gripper_group.setJointValueTarget(target_values);
	  success = gripper_group.plan(plan);

	  ROS_INFO("Visualizing plan for the gripper group %s",success?"":"FAILED");
	  
	  planning_attempts++;
  }

  if(success)
  {
	  // Request to execute the plan, blocking by default
	  if(blocking)
		success = gripper_group.execute(plan);
	  else
		success = gripper_group.asyncExecute(plan);
		
	  // Check the result
	  ROS_INFO("Executed plan for the gripper group %s",success?"":"FAILED");    
	  if(!success)
	  {
		  return false;
	  }
  }
  else
  {
	  ROS_ERROR("Gripper planning failed %d times, Exiting test program...", GRIPPER_MAX_PLANNING_ATTEMPTS);
	  return false;
  }
  
  return true;
}
  


int main(int argc, char **argv)
{
  ros::init(argc, argv, "bonebraker_moveit_interface");
  ros::NodeHandle node_handle;  
  ros::AsyncSpinner spinner(1);
  spinner.start();

  // quad control reference publisher
  ros::Publisher quad_1_control_ref_pub = node_handle.advertise<arcas_msgs::QuadControlReferencesStamped>("/ual_1/quad_control_references", 1);
  arcas_msgs::QuadControlReferencesStamped quad_1_control_ref;
  ros::Publisher quad_2_control_ref_pub = node_handle.advertise<arcas_msgs::QuadControlReferencesStamped>("/ual_2/quad_control_references", 1);
  arcas_msgs::QuadControlReferencesStamped quad_2_control_ref;
    
  // This sleep is ONLY to allow Rviz to come up
  sleep(5.0);

  //! MoveGroup and PlanningScene Setup
  //! ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  // The :move_group_interface:`MoveGroup` class can be easily setup using just the name of the group you would like to control and plan for.
  moveit::planning_interface::MoveGroup uav_group_1(QUADROTOR_1_PLANNING_GROUP);
  moveit::planning_interface::MoveGroup uav_group_2(QUADROTOR_2_PLANNING_GROUP);
  moveit::planning_interface::MoveGroup arm_group_1(ARM_1_PLANNING_GROUP);
  moveit::planning_interface::MoveGroup arm_group_2(ARM_2_PLANNING_GROUP);
  moveit::planning_interface::MoveGroup gripper_group_1(GRIPPER_1_PLANNING_GROUP);
  moveit::planning_interface::MoveGroup gripper_group_2(GRIPPER_2_PLANNING_GROUP);

  // We will use the :planning_scene_interface:`PlanningSceneInterface class to deal directly with the world.
  moveit::planning_interface::PlanningSceneInterface planning_scene_interface;  

  //! Configuring the Planning
  //! ^^^^^^^^^^^^^^^^^^^^^^^^
  // Set the ompl planner to be used
  uav_group_1.setPlannerId(RRT_CONNECT_OMPL_PLANNER);
  uav_group_2.setPlannerId(RRT_CONNECT_OMPL_PLANNER);
  arm_group_1.setPlannerId(RRT_CONNECT_OMPL_PLANNER);
  gripper_group_1.setPlannerId(RRT_CONNECT_OMPL_PLANNER);
  arm_group_2.setPlannerId(RRT_CONNECT_OMPL_PLANNER);
  gripper_group_2.setPlannerId(RRT_CONNECT_OMPL_PLANNER);
    
  // Set the workspace bounding box. The box is specified in the planning frame (/world)
  uav_group_1.setWorkspace(UAV_WS_X_MIN, UAV_WS_Y_MIN, UAV_WS_Z_MIN, UAV_WS_X_MAX, UAV_WS_Y_MAX, UAV_WS_Z_MAX);
  uav_group_2.setWorkspace(UAV_WS_X_MIN, UAV_WS_Y_MIN, UAV_WS_Z_MIN, UAV_WS_X_MAX, UAV_WS_Y_MAX, UAV_WS_Z_MAX);
  arm_group_1.setWorkspace(UAV_WS_X_MIN, UAV_WS_Y_MIN, UAV_WS_Z_MIN, UAV_WS_X_MAX, UAV_WS_Y_MAX, UAV_WS_Z_MAX);
  arm_group_2.setWorkspace(UAV_WS_X_MIN, UAV_WS_Y_MIN, UAV_WS_Z_MIN, UAV_WS_X_MAX, UAV_WS_Y_MAX, UAV_WS_Z_MAX);
  
  // Set Planning time: maximum amount of time to use when planning
  uav_group_1.setPlanningTime(10.0);
  uav_group_2.setPlanningTime(10.0);
  arm_group_1.setPlanningTime(10.0);
  arm_group_2.setPlanningTime(10.0);
  
  // Set Planning attempts: the number of times the motion plan is to be computed from scratch before the shortest solution is returned
  uav_group_1.setNumPlanningAttempts(10);
  uav_group_2.setNumPlanningAttempts(10);
  arm_group_1.setNumPlanningAttempts(10);
  arm_group_2.setNumPlanningAttempts(10);
  
  // Set the uav tolerance
  //~ uav_group_1.setGoalOrientationTolerance(0.001);
  //~ uav_group_1.setGoalPositionTolerance(0.01);
  //~ uav_group_2.setGoalOrientationTolerance(0.001);
  //~ uav_group_2.setGoalPositionTolerance(0.01);
  
  
  //! Configuring the Planning Scene
  //! ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  /* Sleep to let planning scene to be loaded*/
  sleep(2.0);
  
  // Adding a MESH collision object to the planning scene
  // 
  // First, we will define the collision object message.
  moveit_msgs::CollisionObject collision_object_;
  collision_object_.header.frame_id = uav_group_1.getPlanningFrame();
  
  // The id of the object is used to identify it
  collision_object_.id = "mesh";
  
  // Define the mesh to add to the world
  // shapes::Mesh* m = shapes::createMeshFromResource("package://bonebraker_moveit/Media/models/stage/pipes.dae");
  //~ shapes::Mesh* m = shapes::createMeshFromResource("package://bonebraker_moveit/Media/models/stage/pipes_tables.stl");
  shapes::Mesh* m = shapes::createMeshFromResource("package://bonebraker_moveit/Media/models/stage/pipes_tables_trusses.stl");
  shape_msgs::Mesh co_mesh;
  shapes::ShapeMsg co_mesh_msg;
  shapes::constructMsgFromShape(m,co_mesh_msg);
  co_mesh = boost::get<shape_msgs::Mesh>(co_mesh_msg);
  collision_object_.meshes.push_back(co_mesh);

  geometry_msgs::Pose mesh_pose;
  mesh_pose.orientation.w = 1.0;
  mesh_pose.position.x =  0.0;
  mesh_pose.position.y = 0.0;
  mesh_pose.position.z =  0.0;
  collision_object_.mesh_poses.push_back(mesh_pose);

  collision_object_.operation = collision_object_.ADD;

  std::vector<moveit_msgs::CollisionObject> collision_objects_;  
  collision_objects_.push_back(collision_object_);  

  // Now, let's add the collision object into the world
  ROS_INFO("Add a MESH into the world");  
  planning_scene_interface.addCollisionObjects(collision_objects_);

  // Sleep so we have time to see the object in RViz
  sleep(2.0);
  
  //! Bonebraker takeoff and land actions 
  //! ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  uav_1_pos_init = false;
  // Topic subscriber to get the quad position. Only for read the first position to takeoff
  ros::Subscriber quad_1_state_estimation_sub = node_handle.subscribe("/ual_1/quad_state_estimation", 1, &quad_1_StateEstimationCallback);
  ros::Subscriber quad_2_state_estimation_sub = node_handle.subscribe("/ual_2/quad_state_estimation", 1, &quad_2_StateEstimationCallback);

  printf("Waiting to get uav_1 intial position...");
  fflush(stdout);
  while(!uav_1_pos_init)
  {
	  sleep(1);
	  printf(".");
	  fflush(stdout);
  }
  printf(" [OK]\n");
  
  printf("Waiting to get uav_2 intial position...");
  fflush(stdout);
  while(!uav_2_pos_init)
  {
	  sleep(1);
	  printf(".");
	  fflush(stdout);
  }
  printf(" [OK]\n");
  
  // Send actual pos estimation as ref
  quad_1_control_ref.quad_control_references.position_ref.x = quad_1_state_estimation.quad_state_estimation_with_covariance.position.x;
  quad_1_control_ref.quad_control_references.position_ref.y = quad_1_state_estimation.quad_state_estimation_with_covariance.position.y;
  quad_1_control_ref.quad_control_references.position_ref.z = 1.0;
  quad_1_control_ref.quad_control_references.heading = quad_1_state_estimation.quad_state_estimation_with_covariance.attitude.yaw;
  quad_1_control_ref.quad_control_references.velocity_ref = 1.0; // it should be high because a little velocity sature so much the controller
  quad_1_control_ref_pub.publish(quad_1_control_ref);

  // Taking-off ActionClient
  TakeOffActionWrapper takeoffActionclient_1("ual_1");
  sleep(3); // Needed between initialization and using it
  ROS_INFO_STREAM("Take-Off Action Sent to UAV 1");
  takeoffActionclient_1.takeOff();
  sleep(2);
	
  // Land ActionClient (Not used here)
  LandActionWrapper landActionClient("ual_1");

  // Send actual pos estimation as ref
  quad_2_control_ref.quad_control_references.position_ref.x = quad_2_state_estimation.quad_state_estimation_with_covariance.position.x;
  quad_2_control_ref.quad_control_references.position_ref.y = quad_2_state_estimation.quad_state_estimation_with_covariance.position.y;
  quad_2_control_ref.quad_control_references.position_ref.z = 1.0;
  quad_2_control_ref.quad_control_references.heading = quad_2_state_estimation.quad_state_estimation_with_covariance.attitude.yaw;
  quad_2_control_ref.quad_control_references.velocity_ref = 1.0; // it should be high because a little velocity sature so much the controller
  quad_2_control_ref_pub.publish(quad_2_control_ref);

  // Taking-off ActionClient
  TakeOffActionWrapper takeoffActionclient_2("ual_2");
  sleep(3); // Needed between initialization and using it
  ROS_INFO_STREAM("Take-Off Action Sent to UAV 2");
  takeoffActionclient_2.takeOff();
  sleep(2);
  
  //! Path constraints for the UAV (not roll and pitch for planning)
  //! ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  // For the multi-uav moveit configuration is not necessary because the actuated variables is a 4 dof group, not roll & pitch


  /// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ///
  /// $$$$$$$$$$$$$$             UAV 1 - PICK BAR 1          $$$$$$$$$$$$$$$$$ ///
  /// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ///
  
  //! UAV 1 to aprox position
  //! ^^^^^^^^^^^^^^^^^^^^^^^
  ROS_INFO(">> UAV 1:");
  if(!plan_and_execute_to_uav(uav_group_1, PICK_BAR_1_POS_X + UAV_APROX_POS_dX, PICK_BAR_1_POS_Y + UAV_APROX_POS_dY, PICK_BAR_1_POS_Z + UAV_APROX_POS_dZ, PICK_BAR_1_POS_YAW + UAV_APROX_POS_dYAW))
  {
	  exit(0);
  }
  
  //! Arm 1 extension
  //! ^^^^^^^^^^^^^
  // Arm state topic
  ros::Subscriber sub_pose_arm_1 = node_handle.subscribe("/aal_1/arm_state_estimation", 1, &arm_1_StateEstimationCallback);
  // Arm extension ActionClient
  AALExtensionActionWrapper aalExtensionActionWrapper_1("1");
  printf("Exetending the arm 1 ...");
  fflush(stdout);
  sleep(2.0);
  aalExtensionActionWrapper_1.extendArm();
  while(arm_state_1.arm_state != arcas_msgs::ArmStateEstimation::EXTENDED) 
  {
	  sleep(1);
	  printf("."); 
	  fflush(stdout);
  }
  printf(" [OK]\n");

  //! Arm 1 to aprox position 
  //! ^^^^^^^^^^^^^^^^^^^^^^^
  // Wait to UAV stabilization
  int stabilization_index = 0;
  printf("Waiting for UAV 1 stabilization..");
  fflush(stdout);
  while(stabilization_index <= UAV_STABILIZATION_COUNT)
  {
	 usleep(10000); 
	 
	 if(checkUav_1_Stabilization())
	 {	
		stabilization_index++;
		printf(".");
		fflush(stdout);
	 }
	 else
	 {
		 stabilization_index = 0;
	 }
  }
  printf(" [OK]\n");
  
  ROS_INFO(">> ARM 1:");
  if(!plan_and_execute_to_arm(arm_group_1, PICK_BAR_1_POS_X + ARM_APROX_POS_dX, PICK_BAR_1_POS_Y + ARM_APROX_POS_dY, PICK_BAR_1_POS_Z + ARM_APROX_POS_dZ, 0.0, M_PI/2.0, PICK_BAR_1_POS_YAW + ARM_APROX_POS_dYAW))
  {
	  exit(0);
  }
  
  //! Open the gripper 1
  //! ^^^^^^^^^^^^^^^^^^
  if(!open_gripper(gripper_group_1))
  {
	  exit(0);
  }
	
  //! UAV 1 to grasp position
  //! ^^^^^^^^^^^^^^^^^^^^^^^
  // Wait to UAV stabilization
  stabilization_index = 0;
  printf("Waiting for UAV 1 stabilization..");
  fflush(stdout);
  while(stabilization_index <= UAV_STABILIZATION_COUNT)
  {
	 usleep(10000); 
	 
	 if(checkUav_1_Stabilization())
	 {	
		stabilization_index++;
		printf(".");
		fflush(stdout);
	 }
	 else
	 {
		 stabilization_index = 0;
	 }
  }
  printf(" [OK]\n");
  ROS_INFO(">> UAV 1:");
  if(!plan_and_execute_to_uav(uav_group_1, PICK_BAR_1_POS_X + UAV_APROX_POS_dX, PICK_BAR_1_POS_Y + UAV_APROX_POS_dY, PICK_BAR_1_POS_Z + UAV_APROX_POS_dZ - ARM_APROX_POS_dZ + ARM_GRASP_POS_dZ, PICK_BAR_1_POS_YAW + UAV_APROX_POS_dYAW))
  {
	  exit(0);
  }
 
  //! Close the gripper 1
  //! ^^^^^^^^^^^^^^^^^^^
  if(!close_gripper(gripper_group_1))
  {
	  exit(0);
  }

  //! UAV 1 to aprox position
  //! ^^^^^^^^^^^^^^^^^^^^^^^
  ROS_INFO(">> UAV 1:");

  //~ gripper_group_1.setStartStateToCurrentState();
  //~ arm_group_1.setStartStateToCurrentState();

  if(!plan_and_execute_to_uav(uav_group_1, PICK_BAR_1_POS_X + UAV_APROX_POS_dX, PICK_BAR_1_POS_Y + UAV_APROX_POS_dY, PICK_BAR_1_POS_Z + UAV_APROX_POS_dZ, PICK_BAR_1_POS_YAW + UAV_APROX_POS_dYAW))
  {
	  exit(0);
  }

  /// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ///
  /// $$$$$$$$$$$$$$             UAV 1 - PLACE BAR 1          $$$$$$$$$$$$$$$$ ///
  /// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ///
  
  //! UAV 1 to base aprox position
  //! ^^^^^^^^^^^^^^^^^^^^^^^
  ROS_INFO(">> UAV 1:");
  
  //~ gripper_group_1.setStartStateToCurrentState();
  //~ arm_group_1.setStartStateToCurrentState();
  //~ 
  if(!plan_and_execute_to_uav(uav_group_1, PLACE_BASE_1_POS_X + UAV_APROX_POS_dX, PLACE_BASE_1_POS_Y + UAV_APROX_POS_dY, PLACE_BASE_1_POS_Z + UAV_APROX_POS_dZ, PLACE_BASE_1_POS_YAW + UAV_APROX_POS_dYAW))
  {
	  exit(0);
  }

  // Wait to UAV stabilization
  stabilization_index = 0;
  printf("Waiting for UAV 1 stabilization..");
  fflush(stdout);
  while(stabilization_index <= UAV_STABILIZATION_COUNT)
  {
	 usleep(10000); 
	 
	 if(checkUav_1_Stabilization())
	 {	
		stabilization_index++;
		printf(".");
		fflush(stdout);
	 }
	 else
	 {
		 stabilization_index = 0;
	 }
  }
  printf(" [OK]\n");

  //! UAV 1 to base position
  //! ^^^^^^^^^^^^^^^^^^^^^^^
  ROS_INFO(">> UAV 1:");
  if(!plan_and_execute_to_uav(uav_group_1, PLACE_BASE_1_POS_X + UAV_APROX_POS_dX, PLACE_BASE_1_POS_Y + UAV_APROX_POS_dY, PLACE_BASE_1_POS_Z + UAV_APROX_POS_dZ - ARM_APROX_POS_dZ + ARM_GRASP_POS_dZ, PLACE_BASE_1_POS_YAW + UAV_APROX_POS_dYAW))
  {
	  exit(0);
  }
  
  //! Open the gripper 1
  //! ^^^^^^^^^^^^^^^^^^
  if(!open_gripper(gripper_group_1))
  {
	  exit(0);
  }

  //! UAV 1 to base aprox position
  //! ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  ROS_INFO(">> UAV 1:");
  if(!plan_and_execute_to_uav(uav_group_1, PLACE_BASE_1_POS_X + UAV_APROX_POS_dX, PLACE_BASE_1_POS_Y + UAV_APROX_POS_dY, PLACE_BASE_1_POS_Z + UAV_APROX_POS_dZ, PLACE_BASE_1_POS_YAW + UAV_APROX_POS_dYAW))
  {
	  exit(0);
  }
  
  //! UAV 1 to ZERO position
  //! ^^^^^^^^^^^^^^^^^^^^^^
  ROS_INFO(">> UAV 1:");
  if(!plan_and_execute_to_uav(uav_group_1, UAV_1_ZERO_POS_X, UAV_1_ZERO_POS_Y, UAV_1_ZERO_POS_Z, UAV_1_ZERO_POS_YAW))
  {
	  exit(0);
  }

  /// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ///
  /// $$$$$$$$$$$$$$             UAV 2 - PICK BAR 2          $$$$$$$$$$$$$$$$$ ///
  /// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ///
  
  //! UAV 2 to aprox position
  //! ^^^^^^^^^^^^^^^^^^^^^^^
  ROS_INFO(">> UAV 2:");
  if(!plan_and_execute_to_uav(uav_group_2, PICK_BAR_2_POS_X + UAV_APROX_POS_dX, PICK_BAR_2_POS_Y + UAV_APROX_POS_dY, PICK_BAR_2_POS_Z + UAV_APROX_POS_dZ, PICK_BAR_2_POS_YAW + UAV_APROX_POS_dYAW))
  {
	  exit(0);
  }
  
  //! Arm 2 extension
  //! ^^^^^^^^^^^^^
  // Arm state topic
  ros::Subscriber sub_pose_arm_2 = node_handle.subscribe("/aal_2/arm_state_estimation", 1, &arm_2_StateEstimationCallback);
  // Arm extension ActionClient
  AALExtensionActionWrapper aalExtensionActionWrapper_2("2");
  printf("Exetending the arm 2 ...");
  fflush(stdout);
  sleep(2.0);
  aalExtensionActionWrapper_2.extendArm();
  while(arm_state_2.arm_state != arcas_msgs::ArmStateEstimation::EXTENDED) 
  {
	  sleep(1);
	  printf("."); 
	  fflush(stdout);
  }
  printf(" [OK]\n");

  //! Arm 2 to aprox position 
  //! ^^^^^^^^^^^^^^^^^^^^^^^
  // Wait to UAV stabilization
  stabilization_index = 0;
  printf("Waiting for UAV 2 stabilization..");
  fflush(stdout);
  while(stabilization_index <= UAV_STABILIZATION_COUNT)
  {
	 usleep(10000); 
	 
	 if(checkUav_2_Stabilization())
	 {	
		stabilization_index++;
		printf(".");
		fflush(stdout);
	 }
	 else
	 {
		 stabilization_index = 0;
	 }
  }
  printf(" [OK]\n");
  
  ROS_INFO(">> ARM 2:");
  if(!plan_and_execute_to_arm(arm_group_2, PICK_BAR_2_POS_X + ARM_APROX_POS_dX, PICK_BAR_2_POS_Y + ARM_APROX_POS_dY, PICK_BAR_2_POS_Z + ARM_APROX_POS_dZ, 0.0, M_PI/2.0, PICK_BAR_2_POS_YAW + ARM_APROX_POS_dYAW))
  {
	  exit(0);
  }
  
  //! Open the gripper 2
  //! ^^^^^^^^^^^^^^^^^^
  if(!open_gripper(gripper_group_2))
  {
	  exit(0);
  }
	
  //! UAV 2 to grasp position
  //! ^^^^^^^^^^^^^^^^^^^^^^^
  // Wait to UAV stabilization
  stabilization_index = 0;
  printf("Waiting for UAV 2 stabilization..");
  fflush(stdout);
  while(stabilization_index <= UAV_STABILIZATION_COUNT)
  {
	 usleep(10000); 
	 
	 if(checkUav_2_Stabilization())
	 {	
		stabilization_index++;
		printf(".");
		fflush(stdout);
	 }
	 else
	 {
		 stabilization_index = 0;
	 }
  }
  printf(" [OK]\n");
  ROS_INFO(">> UAV 2:");
  if(!plan_and_execute_to_uav(uav_group_2, PICK_BAR_2_POS_X + UAV_APROX_POS_dX, PICK_BAR_2_POS_Y + UAV_APROX_POS_dY, PICK_BAR_2_POS_Z + UAV_APROX_POS_dZ - ARM_APROX_POS_dZ + ARM_GRASP_POS_dZ, PICK_BAR_2_POS_YAW + UAV_APROX_POS_dYAW))
  {
	  exit(0);
  }
 
  //! Close the gripper 2
  //! ^^^^^^^^^^^^^^^^^^^
  if(!close_gripper(gripper_group_2))
  {
	  exit(0);
  }

  //! UAV 2 to aprox position
  //! ^^^^^^^^^^^^^^^^^^^^^^^
  ROS_INFO(">> UAV 2:");
  if(!plan_and_execute_to_uav(uav_group_2, PICK_BAR_2_POS_X + UAV_APROX_POS_dX, PICK_BAR_2_POS_Y + UAV_APROX_POS_dY, PICK_BAR_2_POS_Z + UAV_APROX_POS_dZ, PICK_BAR_2_POS_YAW + UAV_APROX_POS_dYAW))
  {
	  exit(0);
  }


  /// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ///
  /// $$$$$$$$$$$$$$             UAV 2 - PLACE BAR 2          $$$$$$$$$$$$$$$$ ///
  /// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ ///
  
  //! UAV 2 to base aprox position
  //! ^^^^^^^^^^^^^^^^^^^^^^^
  ROS_INFO(">> UAV 2:");
  if(!plan_and_execute_to_uav(uav_group_2, PLACE_BASE_2_POS_X + UAV_APROX_POS_dX, PLACE_BASE_2_POS_Y + UAV_APROX_POS_dY, PLACE_BASE_2_POS_Z + UAV_APROX_POS_dZ, PLACE_BASE_2_POS_YAW + UAV_APROX_POS_dYAW))
  {
	  exit(0);
  }
  
  // Wait to UAV stabilization
  stabilization_index = 0;
  printf("Waiting for UAV 2 stabilization..");
  fflush(stdout);
  while(stabilization_index <= UAV_STABILIZATION_COUNT)
  {
	 usleep(10000); 
	 
	 if(checkUav_2_Stabilization())
	 {	
		stabilization_index++;
		printf(".");
		fflush(stdout);
	 }
	 else
	 {
		 stabilization_index = 0;
	 }
  }
  printf(" [OK]\n");

  //! UAV 2 to base position
  //! ^^^^^^^^^^^^^^^^^^^^^^^
  ROS_INFO(">> UAV 2:");
  if(!plan_and_execute_to_uav(uav_group_2, PLACE_BASE_2_POS_X + UAV_APROX_POS_dX, PLACE_BASE_2_POS_Y + UAV_APROX_POS_dY, PLACE_BASE_2_POS_Z + UAV_APROX_POS_dZ - ARM_APROX_POS_dZ + ARM_GRASP_POS_dZ, PLACE_BASE_2_POS_YAW + UAV_APROX_POS_dYAW))
  {
	  exit(0);
  }
  
  //! Open the gripper 2
  //! ^^^^^^^^^^^^^^^^^^
  if(!open_gripper(gripper_group_2))
  {
	  exit(0);
  }

  //! UAV 2 to base aprox position
  //! ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  ROS_INFO(">> UAV 2:");
  if(!plan_and_execute_to_uav(uav_group_2, PLACE_BASE_2_POS_X + UAV_APROX_POS_dX, PLACE_BASE_2_POS_Y + UAV_APROX_POS_dY, PLACE_BASE_2_POS_Z + UAV_APROX_POS_dZ, PLACE_BASE_2_POS_YAW + UAV_APROX_POS_dYAW))
  {
	  exit(0);
  }
  
  //! UAV 2 to ZERO position
  //! ^^^^^^^^^^^^^^^^^^^^^^
  ROS_INFO(">> UAV 2:");
  if(!plan_and_execute_to_uav(uav_group_2, UAV_2_ZERO_POS_X, UAV_2_ZERO_POS_Y, UAV_2_ZERO_POS_Z, UAV_2_ZERO_POS_YAW))
  {
	  exit(0);
  }
  
  //! Exit
  //! ^^^^
  ros::shutdown();  
  return 0;
}

