#include "ros/ros.h"
#include "bonebraker_moveit_interface/RequestAerialSpace.h"
#include "bonebraker_moveit_interface/LiberateAerialSpace.h"

bool aerialSpaceFree;
int uav_1_table;
int uav_2_table;


bool request_aerial_space(	bonebraker_moveit_interface::RequestAerialSpace::Request  &req,
							bonebraker_moveit_interface::RequestAerialSpace::Response &res)
{
	if(aerialSpaceFree)
	{
		switch(req.uav_id)
		{
			case 1:		if(uav_2_table != req.table_id)
						{
							aerialSpaceFree = false;
							uav_1_table = req.table_id;
							res.result = true;
						}
						else
						{
							res.result = false;
							return false;
						}
						
						break;
			
			case 2:		if(uav_1_table != req.table_id)
						{
							aerialSpaceFree = false;
							uav_2_table = req.table_id;
							res.result = true;
						}
						else
						{
							res.result = false;
							return false;
						}
						
						break;
						
			default: 	ROS_ERROR("Actually it's only implemented for 2 UAVs (uav_id = 1 or 2)");
						exit(0);
		}
		
		return true;
	}
	else
	{
		return false;
	}
}

bool liberate_aerial_space(	bonebraker_moveit_interface::LiberateAerialSpace::Request  &req,
							bonebraker_moveit_interface::LiberateAerialSpace::Response &res)
{
	aerialSpaceFree = true;
	
	if(req.liberate_table)
	{
		switch(req.uav_id)
		{
			case 1:		uav_1_table = 0;
						break;
			
			case 2:		uav_2_table = 0;
						break;
						
			default: 	ROS_ERROR("Actually it's only implemented for 2 UAVs (uav_id = 1 or 2)");
						exit(0);
		}
	}
	
	return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "aerial_space_server");
  ros::NodeHandle n;
  
  aerialSpaceFree = true;
  uav_1_table = 0;
  uav_2_table = 0;

  ros::ServiceServer service_request_aerial_space = n.advertiseService("request_aerial_space", request_aerial_space);
  ros::ServiceServer service_liberate_aerial_space = n.advertiseService("liberate_aerial_space", liberate_aerial_space);
  ROS_INFO("Aerial Space Server ready!");
  ros::spin();

  return 0;
}
