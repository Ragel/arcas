cmake_minimum_required(VERSION 2.8.3)
project(bonebraker_moveit_interface)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  cmake_modules
  moveit_core
  moveit_ros_perception
  moveit_ros_planning
  moveit_ros_planning_interface
  moveit_ros_warehouse
  roscpp
  rospy
  std_msgs
  tf
  warehouse_ros
  arcas_actions_msgs
  arcas_msgs
  motion_action_handler 
  message_generation
)

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)


## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()

################################################
## Declare ROS messages, services and actions ##
################################################

## To declare and build messages, services or actions from within this
## package, follow these steps:
## * Let MSG_DEP_SET be the set of packages whose message types you use in
##   your messages/services/actions (e.g. std_msgs, actionlib_msgs, ...).
## * In the file package.xml:
##   * add a build_depend and a run_depend tag for each package in MSG_DEP_SET
##   * If MSG_DEP_SET isn't empty the following dependencies might have been
##     pulled in transitively but can be declared for certainty nonetheless:
##     * add a build_depend tag for "message_generation"
##     * add a run_depend tag for "message_runtime"
## * In this file (CMakeLists.txt):
##   * add "message_generation" and every package in MSG_DEP_SET to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * add "message_runtime" and every package in MSG_DEP_SET to
##     catkin_package(CATKIN_DEPENDS ...)
##   * uncomment the add_*_files sections below as needed
##     and list every .msg/.srv/.action file to be processed
##   * uncomment the generate_messages entry below
##   * add every package in MSG_DEP_SET to generate_messages(DEPENDENCIES ...)

## Generate messages in the 'msg' folder
# add_message_files(
#   FILES
#   Message1.msg
#   Message2.msg
# )

## Generate services in the 'srv' folder
 add_service_files(
   FILES
   RequestAerialSpace.srv
   LiberateAerialSpace.srv
)

## Generate actions in the 'action' folder
# add_action_files(
#   FILES
#   Action1.action
#   Action2.action
# )

# Generate added messages and services with any dependencies listed here
 generate_messages(
   DEPENDENCIES
   std_msgs
 )

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES bonebraker_moveit_interface
#  CATKIN_DEPENDS cmake_modules moveit_core moveit_ros_perception moveit_ros_planning moveit_ros_planning_interface moveit_ros_warehouse roscpp rospy std_msgs tf warehouse_ros
#  DEPENDS system_lib
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

## Declare a cpp library
# add_library(bonebraker_moveit_interface
#   src/${PROJECT_NAME}/bonebraker_moveit_interface.cpp
# )

## Declare a cpp executable
# add_executable(bonebraker_moveit_interface_node src/bonebraker_moveit_interface_node.cpp)
#~ add_executable(test_interface src/bonebraker_moveit_interface_test.cpp)
#~ target_link_libraries(test_interface
  #~ ${catkin_LIBRARIES}
#~ )
#~ 
#~ add_executable(test_pick src/bonebraker_moveit_interface_test_pick.cpp)
#~ target_link_libraries(test_pick
  #~ ${catkin_LIBRARIES}
#~ )

#add_executable(BonebrakerServer src/bonebraker_moveit_interface_server.cpp)
#target_link_libraries(
#	BonebrakerServer ${catkin_LIBRARIES}

#)

add_executable(test_pick_and_place src/single_tests/bonebraker_moveit_interface_test_pick_and_place.cpp)
target_link_libraries(test_pick_and_place ${catkin_LIBRARIES})

add_executable(test_multi_robot_2 src/multi_tests/two_robots/multi_bonebraker_moveit_interface_test.cpp)
target_link_libraries(test_multi_robot_2 ${catkin_LIBRARIES})

add_executable(test_multi_robot_pick_and_place_1 src/multi_tests/one_robot/multi_bonebraker_moveit_interface_test_pick_and_place.cpp)
target_link_libraries(test_multi_robot_pick_and_place_1 ${catkin_LIBRARIES})

add_executable(final_demo src/multi_tests/one_robot/multi_bonebraker_moveit_interface_final_demo.cpp)
target_link_libraries(final_demo ${catkin_LIBRARIES})


#add_executable(test_multi_robot_2_pick src/multi_tests/two_robots/multi_bonebraker_moveit_interface_test_pick.cpp)
#target_link_libraries(test_multi_robot_2_pick
#  ${catkin_LIBRARIES}
#)

#add_executable(test_multi_robot_2_pick_and_place src/multi_tests/two_robots/multi_bonebraker_moveit_interface_test_pick_and_place.cpp)
#target_link_libraries(test_multi_robot_2_pick_and_place
#  ${catkin_LIBRARIES}
#)


#add_executable(aerial_space_server src/multi_tests/aerial_space_server.cpp)
#target_link_libraries(aerial_space_server
#  ${catkin_LIBRARIES}
#)


#add_executable(test_multi_robot_1_pick_and_place src/multi_tests/one_robot/multi_bonebraker_moveit_interface_test_pick_and_place.cpp)
#target_link_libraries(test_multi_robot_1_pick_and_place
#  ${catkin_LIBRARIES}
#)

#add_executable(test_multi_robot_1_test_arm src/multi_tests/one_robot/multi_bonebraker_moveit_interface_test_arm.cpp)
#target_link_libraries(test_multi_robot_1_test_arm
#  ${catkin_LIBRARIES}
#)

#add_executable(pick_and_place_uavA src/multi_tests/two_robots/pick_and_place_uavA.cpp)
#target_link_libraries(pick_and_place_uavA
#  ${catkin_LIBRARIES}
#)
#add_executable(pick_and_place_uavB src/multi_tests/two_robots/pick_and_place_uavB.cpp)
#target_link_libraries(pick_and_place_uavB
#  ${catkin_LIBRARIES}
#)


## Add cmake target dependencies of the executable/library
## as an example, message headers may need to be generated before nodes
# add_dependencies(bonebraker_moveit_interface_node bonebraker_moveit_interface_generate_messages_cpp)

## Specify libraries to link a library or executable target against
# target_link_libraries(bonebraker_moveit_interface_node
#   ${catkin_LIBRARIES}
# )

#############
## Install ##
#############

# all install targets should use catkin DESTINATION variables
# See http://ros.org/doc/api/catkin/html/adv_user_guide/variables.html

## Mark executable scripts (Python etc.) for installation
## in contrast to setup.py, you can choose the destination
# install(PROGRAMS
#   scripts/my_python_script
#   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark executables and/or libraries for installation
# install(TARGETS bonebraker_moveit_interface bonebraker_moveit_interface_node
#   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark cpp header files for installation
# install(DIRECTORY include/${PROJECT_NAME}/
#   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
#   FILES_MATCHING PATTERN "*.h"
#   PATTERN ".svn" EXCLUDE
# )

## Mark other files for installation (e.g. launch and bag files, etc.)
# install(FILES
#   # myfile1
#   # myfile2
#   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
# )

#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
# catkin_add_gtest(${PROJECT_NAME}-test test/test_bonebraker_moveit_interface.cpp)
# if(TARGET ${PROJECT_NAME}-test)
#   target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
# endif()

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
