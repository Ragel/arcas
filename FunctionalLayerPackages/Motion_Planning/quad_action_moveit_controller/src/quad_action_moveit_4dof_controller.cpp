#include <ros/ros.h>
#include <actionlib/server/action_server.h>
#include <pthread.h>

#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <control_msgs/FollowJointTrajectoryAction.h>

#include <tf/transform_datatypes.h>

// Quadrotor
//  .. position references
#include <arcas_msgs/QuadControlReferencesStamped.h>
//  .. position estimation
#include <arcas_msgs/QuadStateEstimationWithCovarianceStamped.h>
//  .. motion_action_handler to land and take-off client
#include <motion_action_handler/actions/land_action_client.h>
#include <motion_action_handler/actions/takeoff_action_client.h>

#define NON_NULL_TIME  		0.1 	//[sec] A defined non zero time for the velocity reference calculation
#define TIME_SAFE_FACTOR 	1.5		//[sec] Sleep between executed references safe factor
#define VEL_MIN_SAT 		0.1		//[m/sec] Minimum velocity for not saturate the controllers too much
#define MIN_DIST_TO_REACH   0.1		//[m] Minimum distance to set the last position executed as reached 

std::string get_action_topic_name(std::string robot_id)
{
	std::string action_topic = "uav_trajectory_action_";
	action_topic.append(robot_id);
	
	return action_topic;	 
}

class Controller{
private:
	typedef actionlib::ActionServer<control_msgs::FollowJointTrajectoryAction> ActionServer;
	typedef ActionServer::GoalHandle GoalHandle;
public:
	Controller(ros::NodeHandle &n, TakeOffActionWrapper &takeoffActionclient, std::string robot_id) :
				node_handle(n),
				action_server_(node_handle, get_action_topic_name(robot_id), boost::bind(&Controller::goalCB, this, _1), boost::bind(&Controller::cancelCB, this, _1), false),
				has_active_goal_(false)
	{
		creato=0;
		quad_control_ref_stop.quad_control_references.position_ref.x = -6.00;
		quad_control_ref_stop.quad_control_references.position_ref.y = -6.00;
		quad_control_ref_stop.quad_control_references.position_ref.z = 1.00;
		quad_control_ref_stop.quad_control_references.heading = 0.00;
		quad_control_ref_stop.quad_control_references.velocity_ref = 0.0;
		char quad_control_topic[50];
		sprintf(quad_control_topic, "/ual_%s/quad_control_references", robot_id.c_str());
		quad_control_ref_pub = node_handle.advertise<arcas_msgs::QuadControlReferencesStamped>(quad_control_topic, 1);
		action_server_.start();
		
		takeoffActionclientPtr = &takeoffActionclient;
		
		first_controlled_joint = "joint_p_x_";
		first_controlled_joint.append(robot_id);
		
		ROS_INFO_STREAM("Node ready!");
	}
	
	//! Callback to get the current state of the quadrotor
	void quadStateEstimationCallback(const arcas_msgs::QuadStateEstimationWithCovarianceStampedConstPtr &st)
	{
	   quad_state_estimation = *st;
	}
	
private:
	ros::NodeHandle node_handle;
	ActionServer action_server_;
	trajectory_msgs::JointTrajectoryPoint lastPosition;
	arcas_msgs::QuadControlReferencesStamped quad_control_ref_stop;					// quad control reference to stop the quad in the last position if execution is stopped
	arcas_msgs::QuadControlReferencesStamped quad_control_ref;						// quad control reference at execution
	ros::Publisher quad_control_ref_pub;											// quad control reference publisher
	arcas_msgs::QuadStateEstimationWithCovarianceStamped quad_state_estimation;		// quad control position estimation
	
	pthread_t trajectoryExecutor;													// trajectory execution thread handler
	int creato;																		// flag to know if a execution thread has been created and already exist

	bool has_active_goal_;															// flag to know if a goal is in execution
	GoalHandle active_goal_;														// goal handler (advertise if cancel, succeces, ...)
	trajectory_msgs::JointTrajectory_<std::allocator<void> > toExecute;				// Multi DOF trajectory msg received
	double elapse_time;
	
	TakeOffActionWrapper* takeoffActionclientPtr;
	
	std::string first_controlled_joint;	// Controled joint (first in the group) for multi-robot moveit implementation ('join_p_x_ID' instead of 'virtual_floating_joint')
	
	void cancelCB(GoalHandle gh){
		if (active_goal_ == gh)
		{
			// Stops the controller.
			if(creato){
				ROS_INFO_STREAM("Stop thread");
				pthread_cancel(trajectoryExecutor);
				creato=0;
			}
			
			// Send actual pos estimation as ref
			quad_control_ref_stop.quad_control_references.position_ref.x = quad_state_estimation.quad_state_estimation_with_covariance.position.x;
			quad_control_ref_stop.quad_control_references.position_ref.y = quad_state_estimation.quad_state_estimation_with_covariance.position.y;
			quad_control_ref_stop.quad_control_references.position_ref.z = quad_state_estimation.quad_state_estimation_with_covariance.position.z;
			quad_control_ref_stop.quad_control_references.heading = quad_state_estimation.quad_state_estimation_with_covariance.attitude.yaw;
			quad_control_ref_stop.quad_control_references.velocity_ref = 1.0; // it should be high because a little velocity sature so much the controller
			quad_control_ref_pub.publish(quad_control_ref_stop);

			// Marks the current goal as canceled.
			active_goal_.setCanceled();
			has_active_goal_ = false;
		}
	}

	void goalCB(GoalHandle gh){
		if (has_active_goal_)
		{
			// Stops the controller.
			if(creato){
				pthread_cancel(trajectoryExecutor);
				creato=0;
			}
			
			// Send actual pos estimation as ref
			quad_control_ref_stop.quad_control_references.position_ref.x = quad_state_estimation.quad_state_estimation_with_covariance.position.x;
			quad_control_ref_stop.quad_control_references.position_ref.y = quad_state_estimation.quad_state_estimation_with_covariance.position.y;
			quad_control_ref_stop.quad_control_references.position_ref.z = quad_state_estimation.quad_state_estimation_with_covariance.position.z;
			quad_control_ref_stop.quad_control_references.heading = quad_state_estimation.quad_state_estimation_with_covariance.attitude.yaw;
			quad_control_ref_stop.quad_control_references.velocity_ref = 1.0; // it should be high because a little velocity sature so much the controller
			quad_control_ref_pub.publish(quad_control_ref_stop);

			// Marks the current goal as canceled.
			active_goal_.setCanceled();
			has_active_goal_ = false;
		}

		gh.setAccepted();
		active_goal_ = gh;
		has_active_goal_ = true;
		toExecute = gh.getGoal()->trajectory;

		// Run execution thread --> Quadrotor trajectory execution
		if(pthread_create(&trajectoryExecutor, NULL, threadWrapper, this)==0){
			creato=1;
			ROS_INFO_STREAM("Thread for trajectory execution created");
		} else {
			ROS_INFO_STREAM("Thread creation failed!");
		}

	}

	static void* threadWrapper(void* arg) {
		Controller * mySelf=(Controller*)arg;
		mySelf->executeTrajectory();
		return NULL;
	}

	void executeTrajectory(){
		if((toExecute.joint_names[0] == first_controlled_joint) && toExecute.points.size()>0)
		{
			ROS_INFO("Quad Action Controller: %d points to execute", toExecute.points.size());
			
			//~ ROS_INFO("DEBUG: joint_names = [%s], [%s], [%s], [%s]", toExecute.joint_names[0].c_str(), toExecute.joint_names[1].c_str(), toExecute.joint_names[2].c_str(), toExecute.joint_names[3].c_str());
			
			
			if(quad_state_estimation.quad_state_estimation_with_covariance.flying_state == arcas_msgs::QuadStateEstimationWithCovariance::LANDED)
			{
				ROS_INFO("Quad Action Controller: Quadrotor landed, executing taking-off first..");
				
				// Send actual pos estimation as ref
				quad_control_ref_stop.quad_control_references.position_ref.x = quad_state_estimation.quad_state_estimation_with_covariance.position.x;
				quad_control_ref_stop.quad_control_references.position_ref.y = quad_state_estimation.quad_state_estimation_with_covariance.position.y;
				quad_control_ref_stop.quad_control_references.position_ref.z = quad_state_estimation.quad_state_estimation_with_covariance.position.z;
				quad_control_ref_stop.quad_control_references.heading = quad_state_estimation.quad_state_estimation_with_covariance.attitude.yaw;
				quad_control_ref_stop.quad_control_references.velocity_ref = 1.0;
				quad_control_ref_pub.publish(quad_control_ref_stop);
				
				// Send take-off
				takeoffActionclientPtr->takeOff();
				
				// Take-off time --> Sleep
				ros::Duration(1.0).sleep();
			}
			
			for(int k=0; k<toExecute.points.size(); k++)
			{
				ROS_INFO("Quad Action Controller: Executing point %d", k);
				
				// Trajectory point recepcition
				trajectory_msgs::JointTrajectoryPoint punto = toExecute.points[k];
				double v_ref;
				
				//~ ROS_INFO("TIME_FROM_START: %f", punto.time_from_start.toNSec()/1.0e9);
				//~ ROS_INFO("V direct: %f, %f, %f, %f", punto.velocities[0], punto.velocities[1], punto.velocities[2], punto.velocities[3]);
				if(k != 0)
				{
					elapse_time = getElapseTime(toExecute.points[k], toExecute.points[k-1]);
					v_ref = getVelRef(toExecute.points[k], toExecute.points[k-1], elapse_time);
				}
				else
				{
					elapse_time = 0.0;
					v_ref = 1.0;
				}
				//~ ROS_INFO("Vref: %f", v_ref);
				
				
				// If the vel reference is minor than VEL_MIN_SAT the controller can be saturated too much
				if(v_ref <= VEL_MIN_SAT)
					v_ref = VEL_MIN_SAT;
				
				bool executed=true;

				executed=publishTranslationComand(punto, v_ref);
				
				//save last position if executed
				if(executed)
				{
					lastPosition = punto;
				}
			}
		}
		
		// Wait to reach final position
		while(!isCloseToFinalPosition() && has_active_goal_)
		{
			usleep(10000);
		}
		
		// update flags
		active_goal_.setSucceeded();
		
		has_active_goal_=false;
		creato=0;
		ROS_INFO("Quad Action Controller: Execution finished");
	}
	
	bool publishTranslationComand(trajectory_msgs::JointTrajectoryPoint punto, double v_ref)
	{
		// Create the reference message
		quad_control_ref.quad_control_references.position_ref.x = punto.positions[0];
		quad_control_ref.quad_control_references.position_ref.y = punto.positions[1];
		quad_control_ref.quad_control_references.position_ref.z = punto.positions[2];
		quad_control_ref.quad_control_references.heading 		= punto.positions[3];
		quad_control_ref.quad_control_references.velocity_ref 	= v_ref;
		
		//~ printPositionInfo();
		
		printCmdInfo();
		
		quad_control_ref_pub.publish(quad_control_ref);
		
		// Execution time --> Sleep
		ros::Duration(TIME_SAFE_FACTOR * elapse_time).sleep();

		return true;
	}
	
	//! Return the yaw angle for the quaternion q
	double getYawFromQ(geometry_msgs::Quaternion q)
	{
	  double roll, pitch, yaw;
	  tf::Quaternion q_(q.x, q.y, q.z, q.w);
	  tf::Matrix3x3(q_).getRPY(roll, pitch, yaw);
	  
	  return yaw;
	}
	
	//! Return the elapse time (sec) between two waypoints
	double getElapseTime(trajectory_msgs::JointTrajectoryPoint P, trajectory_msgs::JointTrajectoryPoint P_1)
	{  
	  return (double)(P.time_from_start.toNSec() - P_1.time_from_start.toNSec())/1.0e9;
	}
	
	//! Return the estimated velocity reference for a waypoints
	double getVelRef(trajectory_msgs::JointTrajectoryPoint P, trajectory_msgs::JointTrajectoryPoint P_1, double T)
	{  
	  double dx = P.positions[0] - P_1.positions[0];
	  double dy = P.positions[1] - P_1.positions[1];
	  double dz = P.positions[2] - P_1.positions[2];

	  if(T >= 0.0001)
		return sqrt(dx*dx+dy*dy+dz*dz)/T;
	  else
		return sqrt(dx*dx+dy*dy+dz*dz)/NON_NULL_TIME;
	}

	void printPositionInfo(){
		ROS_INFO_STREAM("Quad Action Controller: Estimated Position:\n"
				<<"x: " << quad_state_estimation.quad_state_estimation_with_covariance.position.x
				<<" y: " << quad_state_estimation.quad_state_estimation_with_covariance.position.y
				<<" z: " << quad_state_estimation.quad_state_estimation_with_covariance.position.z
				<<" yaw: " 	<< quad_state_estimation.quad_state_estimation_with_covariance.attitude.yaw);
	}

	void printCmdInfo(){
		ROS_INFO_STREAM("Quad Action Controller: cmd to execute:\n"
				<<"x: " << quad_control_ref.quad_control_references.position_ref.x
				<<" y: " << quad_control_ref.quad_control_references.position_ref.y
				<<" z: " << quad_control_ref.quad_control_references.position_ref.z
				<<" yaw: " 	<< quad_control_ref.quad_control_references.heading
				<<" vref: " << quad_control_ref.quad_control_references.velocity_ref
				<<" elapse_time: " << TIME_SAFE_FACTOR * elapse_time);
	}
	
	//! Return true if quad is very close to the lastPosition
	bool isCloseToFinalPosition()
	{
		float ax = quad_state_estimation.quad_state_estimation_with_covariance.position.x;
		float ay = quad_state_estimation.quad_state_estimation_with_covariance.position.y;
		float az = quad_state_estimation.quad_state_estimation_with_covariance.position.z;
		float cx = lastPosition.positions[0];
		float cy = lastPosition.positions[1];
		float cz = lastPosition.positions[2];
		
		ROS_INFO("Distance to reach final position: %f m", sqrt((ax-cx)*(ax-cx) + (ay-cy)*(ay-cy) + (az-cz)*(az-cz)));
		
		if( (ax-cx)*(ax-cx) + (ay-cy)*(ay-cy) + (az-cz)*(az-cz) <= MIN_DIST_TO_REACH * MIN_DIST_TO_REACH )
			return true;
		else
			return false;
	}
};

int main(int argc, char** argv)
{
   if (argc < 2)
   {
      std::cout << "This program need one input parameter.\n"<<
            "The first input parameter is the number of the UAV." << std::endl;
      return -1;
   }
	
	std::string node_name = "quad_action_moveit_controller_";
	node_name.append(argv[1]);	
	ros::init(argc, argv, node_name);
	ros::NodeHandle node;//("~");
	
	// Taking-off ActionClient
	std::string tkoff_client_name = "ual_";
	tkoff_client_name.append(argv[1]);
	TakeOffActionWrapper takeoffActionclient(tkoff_client_name);

	// Action controller thread execution manager
	Controller control(node, takeoffActionclient, argv[1]);

	// Topic subscriber to get the quad position. It's called by main::spin() but the callback is to the  Controller control::quadStateEstimationCallback member function
    char quad_state_topic[50];
    sprintf(quad_state_topic, "/ual_%s/quad_state_estimation", std::string(argv[1]).c_str());
	ros::Subscriber quad_state_estimation_sub = node.subscribe(quad_state_topic, 1, &Controller::quadStateEstimationCallback, &control);

	// Spin to read topics
	ros::spin();

	return 0;
}
