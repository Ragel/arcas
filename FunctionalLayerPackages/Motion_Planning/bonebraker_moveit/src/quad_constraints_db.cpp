#include <moveit/warehouse/planning_scene_storage.h>
#include <moveit/warehouse/constraints_storage.h>
#include <moveit/warehouse/state_storage.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/robot_state/conversions.h>
#include <moveit/kinematic_constraints/utils.h>
#include <boost/algorithm/string/join.hpp>
#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/math/constants/constants.hpp>
#include <ros/ros.h>
#include <tf/LinearMath/Quaternion.h>

static const std::string ROBOT_DESCRIPTION="robot_description";

int main(int argc, char **argv)
{
	ros::init(argc, argv, "moveit_warehouse_quad_constraints", ros::init_options::AnonymousName);

	boost::program_options::options_description desc;
	desc.add_options()
	 ("help", "Show help message")
	 ("host", boost::program_options::value<std::string>(), "Host for the MongoDB.")
	 ("port", boost::program_options::value<std::size_t>(), "Port for the MongoDB.");

	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
	boost::program_options::notify(vm);

	if (vm.count("help"))
	{
	 std::cout << desc << std::endl;
	 return 1;
	}

	ros::AsyncSpinner spinner(1);
	spinner.start();

	ros::NodeHandle nh;
	planning_scene_monitor::PlanningSceneMonitor psm(ROBOT_DESCRIPTION);
	if (!psm.getPlanningScene())
	{
	 ROS_ERROR("Unable to initialize PlanningSceneMonitor");
	 return 1;
	}

	bool done = false;
	unsigned int attempts = 0;
	bool isTheQuadrotor = false;
	while (!done && attempts < 5)
	{
	 attempts++;
	 try
	 {
	   moveit_warehouse::PlanningSceneStorage pss(vm.count("host") ? vm["host"].as<std::string>() : "",
												  vm.count("port") ? vm["port"].as<std::size_t>() : 0);
	   moveit_warehouse::ConstraintsStorage cs(vm.count("host") ? vm["host"].as<std::string>() : "",
											   vm.count("port") ? vm["port"].as<std::size_t>() : 0);
	   moveit_warehouse::RobotStateStorage rs(vm.count("host") ? vm["host"].as<std::string>() : "",
											  vm.count("port") ? vm["port"].as<std::size_t>() : 0);
	   pss.reset();
	   cs.reset();
	   rs.reset();

	   // add default planning scenes
	   moveit_msgs::PlanningScene psmsg;
	   psm.getPlanningScene()->getPlanningSceneMsg(psmsg);
	   psmsg.name = "default";
	   pss.addPlanningScene(psmsg);
	   ROS_INFO("Added default scene: '%s'", psmsg.name.c_str());

	   moveit_msgs::RobotState rsmsg;
	   robot_state::robotStateToRobotStateMsg(psm.getPlanningScene()->getCurrentState(), rsmsg);
	   rs.addRobotState(rsmsg, "default");
	   ROS_INFO("Added default state");

	   const std::vector<std::string> &gnames = psm.getRobotModel()->getJointModelGroupNames();
	   for (std::size_t i = 0 ; i < gnames.size() ; ++i)
	   {
		 const robot_model::JointModelGroup *jmg = psm.getRobotModel()->getJointModelGroup(gnames[i]);
		 
		 
		 ///***********************DEMO PROGRAM MODIFICATIONS********************************* 
		 /// If the group is not a chain finish that iteration
		 //~ if (!jmg->isChain())
		   //~ continue;
		 
		 /// Get group's links name 
		 const std::vector<std::string> &lnames = jmg->getLinkModelNames();
		 if (lnames.empty())
		   continue;
		 
		 /// Debugging
		 //~ printf("CONSTRAINT_DEBUG: Group index: %d\n", i);
		 //~ printf("CONSTRAINT_DEBUG: Group Last link name: %s\n", (lnames.back()).c_str());
		 
		 /// SETTING THE HORIZONTAL CONTRAINT ONLY TO THE QUADROTOR ('base_link' was the last link name)
		 if(!strcmp((lnames.back()).c_str(),"base_link"))
		 	isTheQuadrotor = true;
		 else
		 	isTheQuadrotor = false;
		 	
		 if(isTheQuadrotor)	
		 {
			 moveit_msgs::OrientationConstraint ocm;
			 ocm.link_name = lnames.back();
			 ocm.header.frame_id = psm.getRobotModel()->getModelFrame();
			 
			 tfScalar roll = 0.0;
			 tfScalar pitch = 0.0;
			 tfScalar yaw = 0.0;
			 tf::Quaternion q;
			 q.setRPY(roll,pitch,yaw);
			 //~ printf("QUATERNION: X: %f, Y: %f, Z: %f, W: %f\n", q.x(), q.y(), q.z(), q.w());

			 ocm.orientation.x = q.x();
			 ocm.orientation.y = q.y();
			 ocm.orientation.z = q.z();
			 ocm.orientation.w = q.w();
			 ocm.absolute_x_axis_tolerance = 0.1;
			 ocm.absolute_y_axis_tolerance = 0.1;
			 ocm.absolute_z_axis_tolerance = boost::math::constants::pi<double>();
			 ocm.weight = 1.0;
			 moveit_msgs::Constraints cmsg;
			 cmsg.orientation_constraints.resize(1, ocm);
			 cmsg.name = ocm.link_name + ":rp_zero";
			 cs.addConstraints(cmsg, psm.getRobotModel()->getName(), jmg->getName());
			 ROS_INFO("Added default constraint: '%s' to /%s respect to %s", cmsg.name.c_str(), ocm.link_name.c_str(), ocm.header.frame_id.c_str());
		 }
		 ///*********************************************************************************
	   }
	   done = true;
	   ROS_INFO("Default MoveIt! Warehouse was reset. Done.");
	 }
	 catch(mongo_ros::DbConnectException &ex)
	 {
	   ROS_WARN("MongoDB does not appear to be initialized yet. Waiting for a few seconds before trying again ...");
	   ros::WallDuration(15.0).sleep();
	 }
	}

	ros::shutdown();

	return 0;
}
