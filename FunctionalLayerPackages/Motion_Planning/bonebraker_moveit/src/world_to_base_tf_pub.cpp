#include <string>
#include "ros/ros.h"
#include "nav_msgs/Odometry.h"    
#include <tf/transform_broadcaster.h>

 nav_msgs::Odometry odom_msg;
void copy_odom_msg(const nav_msgs::Odometry odom) 
{
	//~ ROS_INFO("Odom msg received");
	odom_msg = odom;
}

int main(int argc, char** argv) 
{
    ros::init(argc, argv, "world_to_base_tf_pub");
    ros::NodeHandle n;
    ros::Rate loop_rate(100);
	
	// gazebo -> p3d -> topic -> Odometry msg
	ros::Subscriber sub_odom = n.subscribe("/ual_1/ground_truth/state", 1, copy_odom_msg); 
    
    // tf broadcaster
    tf::TransformBroadcaster broadcaster;

    // message declarations
    geometry_msgs::TransformStamped odom_trans;
    odom_trans.header.frame_id = "world";
    odom_trans.child_frame_id = "base_link_1";

    while (ros::ok()) 
    {
        // Reading topic
        ros::spinOnce(); 
        
        // parse and update transform
        odom_trans.header.stamp = ros::Time::now();
        odom_trans.transform.translation.x = odom_msg.pose.pose.position.x;
        odom_trans.transform.translation.y = odom_msg.pose.pose.position.y;
        odom_trans.transform.translation.z = odom_msg.pose.pose.position.z;
        odom_trans.transform.rotation = odom_msg.pose.pose.orientation;

        //send the joint state and transform
        broadcaster.sendTransform(odom_trans);

        // This will adjust as needed per iteration
        loop_rate.sleep();
    }


    return 0;
}
