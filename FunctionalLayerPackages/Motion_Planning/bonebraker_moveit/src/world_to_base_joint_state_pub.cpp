#include <string>
#include "ros/ros.h"
#include "nav_msgs/Odometry.h"    
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/JointState.h>

 nav_msgs::Odometry odom_msg;
void copy_odom_msg(const nav_msgs::Odometry odom) 
{
	//~ ROS_INFO("Odom msg received");
	odom_msg = odom;
}

int main(int argc, char** argv) 
{
   if (argc < 2)
   {
      std::cout << "This program need one input parameter.\n"<<
            "The first input parameter is the number of the UAV." << std::endl;
      return -1;
   }
	
	std::string node_name = "world_to_base_joint_state_pub_";
	node_name.append(argv[1]);	
	ros::init(argc, argv, node_name);
    ros::NodeHandle n;
    ros::Rate loop_rate(100);
	
	double roll, pitch, yaw;
	
	// gazebo -> p3d -> topic -> Odometry msg
	char quad_state_topic[50];
	sprintf(quad_state_topic, "/ual_%s/ground_truth/state", std::string(argv[1]).c_str());
	ros::Subscriber sub_odom = n.subscribe(quad_state_topic, 1, copy_odom_msg); 
    
	sensor_msgs::JointState joint_state;
    joint_state.header.frame_id = "world";
    joint_state.name.resize(6);
    joint_state.position.resize(6);
    joint_state.name[0]="joint_p_x_";
    joint_state.name[1]="joint_p_y_";
    joint_state.name[2]="joint_p_z_";
    joint_state.name[3]="joint_r_z_";
    joint_state.name[4]="joint_r_y_";
    joint_state.name[5]="joint_r_x_";
    joint_state.name[0].append(argv[1]);
    joint_state.name[1].append(argv[1]);
    joint_state.name[2].append(argv[1]);
    joint_state.name[3].append(argv[1]);
    joint_state.name[4].append(argv[1]);
    joint_state.name[5].append(argv[1]);
   	char joint_state_topic[50];
	sprintf(joint_state_topic, "/quad_%s/joint_states", std::string(argv[1]).c_str()); 
	ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>(joint_state_topic, 1);

    while (ros::ok()) 
    {
        // Reading topic
        ros::spinOnce(); 
        
        // parse and update transform
		joint_state.header.stamp = ros::Time::now();
        joint_state.position[0] = odom_msg.pose.pose.position.x;
		joint_state.position[1] = odom_msg.pose.pose.position.y;
		joint_state.position[2] = odom_msg.pose.pose.position.z;
		//~ joint_state.position[3] = tf::getYaw(odom_msg.pose.pose.orientation); // odom_msg.pose.pose.orientation not normalized, so: 
		tf::Quaternion q(odom_msg.pose.pose.orientation.x,odom_msg.pose.pose.orientation.y,odom_msg.pose.pose.orientation.z,odom_msg.pose.pose.orientation.w);
		tf::Quaternion q_ = q.normalize();
		//~ joint_state.position[3] = tf::getYaw(q_); // If we want also roll and pitch:
		tf::Matrix3x3(q).getRPY(roll, pitch, yaw);
		joint_state.position[3] = yaw;
		joint_state.position[4] = pitch;
		joint_state.position[5] = roll;
		
		
		joint_pub.publish(joint_state);
		
        // This will adjust as needed per iteration
        loop_rate.sleep();
    }


    return 0;
}
