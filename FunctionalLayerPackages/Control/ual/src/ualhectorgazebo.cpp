#include <ual/ualhectorgazebo.h>
#include <tf/transform_datatypes.h>
#include <stdlib.h>

std::string int2String( int toConvert){
   char res[10];
   sprintf(res,"%d",toConvert);
   return std::string(res);
}

UALHectorGazebo::UALHectorGazebo(ros::NodeHandle *n, int uavId):
   generalCommunications(n,uavId),
   actual_state(LANDED),
   landActionServer(std::string("land"), int2String(uavId)),
   takeoffActionServer(std::string("take_off"), int2String(uavId))
{
//   update_timer_ = n->createTimer(ros::Duration(1/UPDATE_RATE),
//                                  boost::bind(&UALHectorGazebo::updateLoop, this, _1));


   //Minimmum time for takeoff/land
   takeoff_land_minimmum_time_.fromSec(4);

   lastUpdatePID = ros::Time::now();


   //Initialize PID
   pid_x.Init(5,0,2,0.0,0.0,2,-2);
   pid_y.Init(5,0,2,0.0,0.0,2,-2);
   pid_z.Init(1.0,0,0.0,0.0,0.0,2,-2);
   pid_yaw.Init(2.5,0.0,0.0,0.0,0.0,2.0,-2.0);

   velocity_control_pub_ = n->advertise<geometry_msgs::Twist> ("cmd_vel", 0);
   odometry_sub_ = n->subscribe("ground_truth/state",
               1000, &UALHectorGazebo::odometryCallback,
                        this);

   send_loop_timer_ = n->createTimer(ros::Duration(1/SEND_RATE),
               boost::bind(&UALHectorGazebo::sendLoop, this, _1));
}

void UALHectorGazebo::setTwist(geometry_msgs::Twist t)
{
   twist_command_to_send_ = t;
}

void UALHectorGazebo::odometryCallback(const nav_msgs::Odometry::ConstPtr& odom)
{

   //Calculate lineal acceleration
   tf::Vector3 lastVelocity(
            last_quad_state_estimation.quad_state_estimation_with_covariance.linear_velocity.x,
            last_quad_state_estimation.quad_state_estimation_with_covariance.linear_velocity.y,
            last_quad_state_estimation.quad_state_estimation_with_covariance.linear_velocity.z);

   tf::Vector3 actualVelocity(odom->twist.twist.linear.x,
                        odom->twist.twist.linear.y,
                        odom->twist.twist.linear.z);

   tf::Vector3 acceleration = (lastVelocity -actualVelocity) * 100;



   last_quad_state_estimation.quad_state_estimation_with_covariance.altitude =
         odom->pose.pose.position.z;

   last_quad_state_estimation.quad_state_estimation_with_covariance.angular_velocity.roll = odom->twist.twist.angular.x;
   last_quad_state_estimation.quad_state_estimation_with_covariance.angular_velocity.pitch = odom->twist.twist.angular.y;
   last_quad_state_estimation.quad_state_estimation_with_covariance.angular_velocity.yaw = odom->twist.twist.angular.z;

   last_quad_state_estimation.quad_state_estimation_with_covariance.attitude_commands.roll = 0;
   last_quad_state_estimation.quad_state_estimation_with_covariance.attitude_commands.pitch = 0;
   last_quad_state_estimation.quad_state_estimation_with_covariance.attitude_commands.yaw = 0;

   last_quad_state_estimation.quad_state_estimation_with_covariance.linear_acceleration.x = acceleration.getX();
   last_quad_state_estimation.quad_state_estimation_with_covariance.linear_acceleration.y = acceleration.getY();
   last_quad_state_estimation.quad_state_estimation_with_covariance.linear_acceleration.z = acceleration.getZ();

   last_quad_state_estimation.quad_state_estimation_with_covariance.linear_velocity.x = actualVelocity.getX();
   last_quad_state_estimation.quad_state_estimation_with_covariance.linear_velocity.y = actualVelocity.getY();
   last_quad_state_estimation.quad_state_estimation_with_covariance.linear_velocity.z = actualVelocity.getZ();


   last_quad_state_estimation.quad_state_estimation_with_covariance.position.x = odom->pose.pose.position.x;
   last_quad_state_estimation.quad_state_estimation_with_covariance.position.y = odom->pose.pose.position.y;
   last_quad_state_estimation.quad_state_estimation_with_covariance.position.z = odom->pose.pose.position.z;


   tf::Quaternion orientation(odom->pose.pose.orientation.x,
               odom->pose.pose.orientation.y,
               odom->pose.pose.orientation.z,
               odom->pose.pose.orientation.w);

   /*
    *If do not work propertly, have to change solution numer parameter.
    */
   tf::Matrix3x3(orientation).getRPY(last_quad_state_estimation.quad_state_estimation_with_covariance.attitude.roll,
                     last_quad_state_estimation.quad_state_estimation_with_covariance.attitude.pitch,
                     last_quad_state_estimation.quad_state_estimation_with_covariance.attitude.yaw);


   //Update Header
   last_quad_state_estimation.header.stamp = ros::Time::now();
   last_quad_state_estimation.header.seq++;
   last_quad_state_estimation.header.frame_id = "/world";

   ros::TimerEvent tel;
   updateLoop(tel);
}
void UALHectorGazebo::sendLoop(const ros::TimerEvent& te)
{
   velocity_control_pub_.publish(twist_command_to_send_);
}


void UALHectorGazebo::updateLoop(const ros::TimerEvent& te)
{


   QuadStateEstimationWithCovarianceStamped last_quad_state =
         last_quad_state_estimation;

   last_quad_state.quad_state_estimation_with_covariance.flying_state = actual_state;

   if (buffer_.size() < PURE_DELAY * UPDATE_RATE) {
      buffer_.push_back(last_quad_state);
   } else {
      buffer_.push_back(last_quad_state);
      buffer_.erase(buffer_.begin());
   }
   if (buffer_.begin() == buffer_.end() || PURE_DELAY <= 0.0) {
      generalCommunications.setQuadStateEstimation(last_quad_state);
   } else {
      generalCommunications.setQuadStateEstimation(*buffer_.begin());
   }

   landActionServer.uavUpdateState(actual_state);
   takeoffActionServer.uavUpdateState(actual_state, generalCommunications.getQuadControlReferences().quad_control_references.position_ref.z , last_quad_state.quad_state_estimation_with_covariance.position.z);

   geometry_msgs::Twist velocityCommand;
   tf::Vector3 wanted_position(0,0,0);
   tf::Vector3 rotated_position(0,0,0);

   if(actual_state==TAKING_OFF)
   {
      if((ros::Time::now() - takeoff_land_time_callback_) > takeoff_land_minimmum_time_
            && last_quad_state_estimation.quad_state_estimation_with_covariance.position.z >= (TAKE_OFF_Z-0.05))
      {
         actual_state = FLYING;
      }
      else
      {
         wanted_position.setX(last_quad_state_estimation.quad_state_estimation_with_covariance.position.x);
         wanted_position.setY(last_quad_state_estimation.quad_state_estimation_with_covariance.position.y);
         wanted_position.setZ(TAKE_OFF_Z);

      }
   }
   else if(actual_state==LANDING)
   {

      if((ros::Time::now() - takeoff_land_time_callback_) > takeoff_land_minimmum_time_
            && last_quad_state_estimation.quad_state_estimation_with_covariance.position.z <= (LAND_Z+0.05))
      {
         actual_state = LANDED;
      }
      else
      {
         wanted_position.setX(last_quad_state_estimation.quad_state_estimation_with_covariance.position.x);
         wanted_position.setY(last_quad_state_estimation.quad_state_estimation_with_covariance.position.y);
         wanted_position.setZ(LAND_Z);
      }
   }else
   {
      wanted_position.setX(generalCommunications.getQuadControlReferences().quad_control_references.position_ref.x);
      wanted_position.setY(generalCommunications.getQuadControlReferences().quad_control_references.position_ref.y);
      wanted_position.setZ(generalCommunications.getQuadControlReferences().quad_control_references.position_ref.z);
   }


   double headingPIDError=0;

   switch(actual_state)
   {
   case LANDED:

      if(takeoffActionServer.hasReceivedTakeoffAction())
      {
         actual_state = TAKING_OFF;
      }else
      {
         actual_state = LANDED;
      }

      velocityCommand.linear.x = 0;
      velocityCommand.linear.y = 0;
      velocityCommand.linear.z = -0.1;

      break;
   case TAKING_OFF:
   case LANDING:
   case FLYING:
      if(actual_state==FLYING && landActionServer.hasReceivedLandAction())
      {
         //std::cerr << "Request to land" << std::endl;
         actual_state = LANDING;
      }else
      {
         actual_state = actual_state;
      }

      pid_x.SetCmd(generalCommunications.getQuadControlReferences().
                   quad_control_references.velocity_ref);
      pid_y.SetCmd(generalCommunications.getQuadControlReferences().
                   quad_control_references.velocity_ref);
      pid_z.SetCmd(generalCommunications.getQuadControlReferences().
                   quad_control_references.velocity_ref);


      //Rotate Position to pid control
      static tf::Vector3 tf_pos;
      tf_pos.setX(last_quad_state_estimation.
                  quad_state_estimation_with_covariance.position.x);
      tf_pos.setY(last_quad_state_estimation.
                  quad_state_estimation_with_covariance.position.y);
      tf_pos.setZ(last_quad_state_estimation.
                  quad_state_estimation_with_covariance.position.z);

      static tf::Quaternion rotation;
      rotation.setRPY(last_quad_state_estimation.
                      quad_state_estimation_with_covariance.attitude.roll,
                      last_quad_state_estimation.
                      quad_state_estimation_with_covariance.attitude.pitch,
                      last_quad_state_estimation.
                      quad_state_estimation_with_covariance.attitude.yaw);

      static tf::Transform transformMatrix;
      transformMatrix.setOrigin(tf_pos);
      transformMatrix.setRotation(rotation);

      //Final rotated position (Command)
      rotated_position = transformMatrix.inverse() * wanted_position;


      //Calculate elapsed time
      static ros::Duration rosTimeIncrement = ros::Time::now() - lastUpdatePID;
      static common::Time timeIncrement(rosTimeIncrement.sec,rosTimeIncrement.nsec);
      lastUpdatePID = ros::Time::now();

      velocityCommand.linear.x = pid_x.Update(rotated_position.getX()*(-1),
                                              timeIncrement);
      velocityCommand.linear.y = pid_y.Update(rotated_position.getY()*(-1),
                                              timeIncrement);
      velocityCommand.linear.z = pid_z.Update((wanted_position.getZ()-tf_pos.getZ())*(-1),
                                              timeIncrement);

      headingPIDError = (generalCommunications.getQuadControlReferences().quad_control_references.heading-
                         last_quad_state_estimation.quad_state_estimation_with_covariance.attitude.yaw)*(-1);



      //if(headingPIDError>3.14159)
      //{
      //std::cerr << "Error en yaw (" << generalCommunications.getQuadControlReferences().quad_control_references.heading <<";" <<
      //		last_quad_state_estimation.quad_state_estimation_with_covariance.attitude.yaw  << ") : " << headingPIDError << std::endl;
      //}

      while(headingPIDError>3.14159)
         headingPIDError-= 2*3.14159;

      while(headingPIDError<-3.14159)
         headingPIDError += 2*3.14159;


      velocityCommand.angular.z = pid_yaw.Update(
               headingPIDError,
               timeIncrement);


      /*wanted_position.setX(5.0);
         wanted_position.setY(0.0);
         wanted_position.setZ(0.0);
         rotated_position = transformMatrix.inverse() * wanted_position;*/

      //	std::cerr << "error: " << velocityCommand.linear.x << " - " << velocityCommand.linear.y << ";" << std::endl;
      //			  tf_pos.getZ()<< std::endl;
      break;
   default:
      actual_state = actual_state;
      break;
   }

   //Limit velocity
   tf::Vector3 actualVel(velocityCommand.linear.x,
                         velocityCommand.linear.y,
                         velocityCommand.linear.z);
   if(generalCommunications.getQuadControlReferences().quad_control_references.velocity_ref>0 &&
         actualVel.length()>generalCommunications.getQuadControlReferences().quad_control_references.velocity_ref)
   {
      actualVel =
            (actualVel/actualVel.length())*generalCommunications.getQuadControlReferences().quad_control_references.velocity_ref;
   }

   velocityCommand.linear.x = actualVel.getX();
   velocityCommand.linear.y = actualVel.getY();
   velocityCommand.linear.z = actualVel.getZ();

   //Update command to send.
   setTwist(velocityCommand);
}
