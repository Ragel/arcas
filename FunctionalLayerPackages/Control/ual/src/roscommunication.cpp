#include <ual/roscommunication.h>

boost::mutex RosCommunication::mutex;

RosCommunication::RosCommunication(ros::NodeHandle *n, int uavId):
   _last_seq(0)
{
   quad_state_pub_ =
         n->advertise<arcas_msgs::QuadStateEstimationWithCovarianceStamped> ("quad_state_estimation", 0);

   quad_control_references_sub_ = n->subscribe("quad_control_references",
                                               1, &RosCommunication::quadControlRefCallback,
                                               this);

   send_loop_timer_ = n->createTimer(ros::Duration(1/SEND_RATE),
                                     boost::bind(&RosCommunication::sendLoop, this, _1));
}

QuadControlReferencesStamped RosCommunication::getQuadControlReferences()
{
   return last_quad_control_references_;
}

void RosCommunication::setQuadStateEstimation(QuadStateEstimationWithCovarianceStamped q_state)
{
   mutex.lock();
   quad_state_to_send_.push_back(q_state);
   mutex.unlock();
}

void RosCommunication::quadControlRefCallback(const arcas_msgs::QuadControlReferencesStamped::ConstPtr& q)
{
   last_quad_control_references_ = *q;

   if(last_quad_control_references_.quad_control_references.position_ref.z < TAKE_OFF_Z)
   {
      last_quad_control_references_.quad_control_references.position_ref.z = TAKE_OFF_Z;
   }

   if(last_quad_control_references_.quad_control_references.velocity_ref	> MAXIMUM_VELOCITY)
   {
      last_quad_control_references_.quad_control_references.velocity_ref = MAXIMUM_VELOCITY;
   }

   if(last_quad_control_references_.quad_control_references.position_ref.z > MAX_HEIGHT)
   {
      last_quad_control_references_.quad_control_references.position_ref.z = MAX_HEIGHT;
   }

   while(last_quad_control_references_.quad_control_references.heading > M_PI)
      last_quad_control_references_.quad_control_references.heading -= 2*M_PI;

   while(last_quad_control_references_.quad_control_references.heading < -M_PI)
      last_quad_control_references_.quad_control_references.heading += 2*M_PI;
}
void RosCommunication::sendLoop(const ros::TimerEvent& te)
{
   try {
   bool sent = false;
   while(!sent)
   {
      arcas_msgs::QuadStateEstimationWithCovarianceStamped state;
      //std::cerr << "RosCommunication::sendLoop size"<< quad_state_to_send_.size() << std::endl;
      if(quad_state_to_send_.size() > 0){
         mutex.lock();
         state = quad_state_to_send_[0];
         if(state.header.seq!= _last_seq)
         {
            quad_state_pub_.publish(state);
            _last_seq = state.header.seq;
            sent = true;
         }
         quad_state_to_send_.erase(quad_state_to_send_.begin());
         mutex.unlock();
      }
      if(quad_state_to_send_.size() == 0){
         sent = true;
      }
   }
   } catch(std::bad_alloc &e) {
      std::cerr << "Exception: "<< e.what() << std::endl;
   }
}
