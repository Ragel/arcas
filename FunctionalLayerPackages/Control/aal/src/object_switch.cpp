#include "aal/object_switch.h"
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <string>

ObjectSwitch::ObjectSwitch(ros::NodeHandle n, double distThreshold,
                           double rollThreshold,
                           double pitchThreshold,
                           double yawThreshold,
                           int filterSize,
                           char* file):
    activedSafety_(false)
  ,distThreshold_(distThreshold)
  ,rollThreshold_(rollThreshold)
  ,pitchThreshold_(pitchThreshold)
  ,yawThreshold_(yawThreshold)
  ,filterSize_(filterSize)
{
    FILE *fp;
    int f1;
    char f2[40];
    double tx, ty, tz, qx, qy, qz, qw;
    fp = fopen(file, "r");
    fscanf(fp, "%*[^\n]\n", NULL); //skip the first line
    while (fscanf(fp, "%d %s %lf %lf %lf %lf %lf %lf %lf\n",
                  &f1, f2, &tx, &ty, &tz, &qx, &qy, &qz, &qw) == 9){
        rosViconIdMap_.insert( std::pair<uint8_t, std::string>(
                                   (uint8_t)f1, std::string(f2)));
        ROS_INFO("Added to ROS VICON ID MAP: %d %s", f1, f2);

        tf2::Quaternion quat(qx,qy,qz,qw);
        rosIdQuatMap_.insert(std::pair<uint8_t, tf2::Quaternion >(
                                 (uint8_t)f1, quat));
        quat = rosIdQuatMap_.find((uint8_t)f1)->second;
        tf2::Vector3 vect(tx,ty,tz);
        rosIdTranslationMap_.insert(
                    std::pair<uint8_t, tf2::Vector3 >(
                        (uint8_t)f1, vect));
        vect = rosIdTranslationMap_.find((uint8_t)f1)->second;
        ROS_INFO("Its transformation (TX TY TZ - QX QY QZ QW) "
                 "is:\n\t %lf %lf %lf %lf %lf %lf %lf", vect.x(), vect.y(), vect.z(),
                 quat.getX(), quat.getY(), quat.getZ(), quat.getW());
    }
    fclose(fp);

    std::map<uint8_t, std::string>::iterator it = rosViconIdMap_.begin();
    do {
        ROS_INFO("Creating sub and pub for the object identifier %d", (uint)it->first);
        std::stringstream subTopic;
        subTopic << "/perception/object_state_estimation_" << (uint)it->first;
        ROS_INFO("Sub topic: %s", subTopic.str().c_str());
        ros::Subscriber sub = n.subscribe(std::string(subTopic.str()), 0,
                                          &ObjectSwitch::objectStatesCallback, this);
        objs_states_sub_.insert( std::pair<uint8_t,ros::Subscriber> (it->first, sub));

        std::stringstream pubTopic;
        pubTopic << "/safety/object_state_estimation_" << (uint)it->first;
        ROS_INFO("Pub topic: %s", pubTopic.str().c_str());
        ros::Publisher pub = n.advertise<arcas_msgs::
                ObjectStateEstimationWithCovarianceStamped>(std::string(pubTopic.str()), 0);
        objs_states_pub_.insert( std::pair<uint8_t,ros::Publisher> (it->first, pub));
        it++;
    } while ( it != rosViconIdMap_.end());

    sleep(1);
    memset(&viconData_,0,sizeof(TviconData));
    viconUDPReceiver_ = new ViconUDPReceiver(VICON_PORT,&viconData_);
    udpReceiverThread_ = new boost::thread(
                boost::bind(&ViconUDPReceiver::thread, viconUDPReceiver_));
}

ObjectSwitch::~ObjectSwitch()
{
    if(udpReceiverThread_ != NULL){
        delete udpReceiverThread_;
        udpReceiverThread_ = NULL;
    }
    if(viconUDPReceiver_ != NULL){
        delete viconUDPReceiver_;
        viconUDPReceiver_ = NULL;
    }
}


void ObjectSwitch::transform(arcas_msgs::ObjectStateEstimationWithCovarianceStamped &viconObj,
                             const arcas_msgs::ObjectStateEstimationWithCovarianceStamped percepObj)
{
    //Transform vicon to correct frame (optical_frame)
    //   tf2::Vector3 vec = rosIdTranslationMap_.find(
    //            percepObj.object_state_estimation_with_covariance.identifier)->second;
    //   tf2::Quaternion quat = rosIdQuatMap_.find(
    //            percepObj.object_state_estimation_with_covariance.identifier)->second;
    //   //ROS_INFO("ObjectSwitch::transform - Using TF: %lf %lf %lf %lf %lf %lf %lf", vec.x(),
    //   //         vec.y(), vec.z(),quat.getX(),quat.getY(),quat.getZ(),quat.getW());

    tf::TransformListener listener;
    tf::StampedTransform transform;
    try {
        listener.waitForTransform("/optical_frame", "/map", ros::Time(0), ros::Duration(1.0) );
        listener.lookupTransform("/optical_frame", "/map", ros::Time(0), transform);
    } catch (tf::TransformException ex) {
        ROS_ERROR("%s",ex.what());
    }
    tf2::Vector3 origin(viconObj.object_state_estimation_with_covariance.position.x,
                        viconObj.object_state_estimation_with_covariance.position.y,
                        viconObj.object_state_estimation_with_covariance.position.z);
    tf2::Transform tf(tf2::Quaternion(transform.getRotation().x(),
                                      transform.getRotation().y(),
                                      transform.getRotation().z(),
                                      transform.getRotation().w())
                      , tf2::Vector3(transform.getOrigin().x(),
                                     transform.getOrigin().y(),
                                     transform.getOrigin().z()));
    tf2::Vector3 posT = tf * origin;
    //       ROS_INFO("ObjectSwitch::transform - Transforming position: %f %f %f", origin.x(),
    //                origin.y(), origin.z());
    //       ROS_INFO("ObjectSwitch::transform - Transformed position: %f %f %f", posT.x(),
    //                posT.y(), posT.z());
    viconObj.object_state_estimation_with_covariance.position.x = posT.x();
    viconObj.object_state_estimation_with_covariance.position.y = posT.y();
    viconObj.object_state_estimation_with_covariance.position.z = posT.z();

    tf2::Quaternion origingOrientationQuat = getFromEuler123(
                viconObj.object_state_estimation_with_covariance.attitude.roll,
                viconObj.object_state_estimation_with_covariance.attitude.pitch,
                viconObj.object_state_estimation_with_covariance.attitude.yaw);
    tf2::Quaternion transformedQuat = tf * origingOrientationQuat;
    viconObj.object_state_estimation_with_covariance.attitude.roll =
            getRoll123FromQuat(transformedQuat);
    viconObj.object_state_estimation_with_covariance.attitude.pitch =
            getPitch123FromQuat(transformedQuat);
    viconObj.object_state_estimation_with_covariance.attitude.yaw =
            getYaw123FromQuat(transformedQuat);

    tf::Transform viconTF;
    static tf::TransformBroadcaster br;
    viconTF.setOrigin(tf::Vector3(origin.x(), origin.y(),origin.z()));
    viconTF.setRotation(tf::Quaternion(origingOrientationQuat.x(),
                                       origingOrientationQuat.y(),
                                       origingOrientationQuat.z(),
                                       origingOrientationQuat.w()));
    std::stringstream viconTF_ID;
    viconTF_ID << "/vicon_obj_" << (int)percepObj.object_state_estimation_with_covariance.identifier;
    br.sendTransform(tf::StampedTransform(viconTF, ros::Time::now(),
                                          "/map", viconTF_ID.str().c_str()));

//    std::stringstream viconCamTF_ID;
//    tf::Transform viconCamTF;
//    viconCamTF.setOrigin(tf::Vector3(posT.x(), posT.y(),posT.z()));
//    viconCamTF.setRotation(tf::Quaternion(transformedQuat.x(),
//                                       transformedQuat.y(),
//                                       transformedQuat.z(),
//                                       transformedQuat.w()));
//    viconCamTF_ID << "/viconCam_obj_" << (int)percepObj.object_state_estimation_with_covariance.identifier;
//    br.sendTransform(tf::StampedTransform(viconCamTF, ros::Time::now(),
//                                          "/optical_frame", viconCamTF_ID.str().c_str()));

}

tf2::Quaternion ObjectSwitch::getFromEuler123(double roll, double pitch, double yaw)
{
    // Assuming the angles are in radians.
    double c1 = (double) (cos(roll * 0.5f));
    double s1 = (double) (sin(roll * 0.5f));
    double c2 = (double) (cos(pitch * 0.5f));
    double s2 = (double) (sin(pitch * 0.5f));
    double c3 = (double) (cos(yaw * 0.5f));
    double s3 = (double) (sin(yaw * 0.5f));
    double w = roundDecimal(c1 * c2 * c3 - s1 * s2 * s3, 5);
    double x = roundDecimal(s1 * c2 * c3 + c1 * s2 * s3, 5);
    double y = roundDecimal(c1 * s2 * c3 - s1 * c2 * s3, 5);
    double z = roundDecimal(c1 * c2 * s3 + s1 * s2 * c3, 5);
    return tf2::Quaternion(x,y,z,w);
}

double ObjectSwitch::getRoll123FromQuat(tf2::Quaternion quaternion)
{
    double roll = roundDecimal(atan2(-2 * (quaternion.getY() * quaternion.getZ()
                                           - quaternion.getW() * quaternion.getX()),
                                     quaternion.getW() * quaternion.getW() - quaternion.getX()
                                     * quaternion.getX() - quaternion.getY()
                                     * quaternion.getY() + quaternion.getZ()
                                     * quaternion.getZ()), 5);
    return roll;
}

double ObjectSwitch::getPitch123FromQuat(tf2::Quaternion quaternion)
{
    double pitch = roundDecimal(asin(2 * (quaternion.getX() * quaternion.getZ()
                                          + quaternion.getW() * quaternion.getY())), 5);
    return pitch;
}

double ObjectSwitch::getYaw123FromQuat(tf2::Quaternion quaternion)
{
    double yaw = roundDecimal(atan2(-2 * (quaternion.getX() * quaternion.getY()
                                          - quaternion.getW() * quaternion.getZ()),
                                    quaternion.getW() * quaternion.getW() + quaternion.getX()
                                    * quaternion.getX() - quaternion.getY()
                                    * quaternion.getY() - quaternion.getZ()
                                    * quaternion.getZ()), 5);
    return yaw;
}

double ObjectSwitch::roundDecimal(const double number, const int decimals)
{
    int intPart = number;
    double decimalPart = number - intPart;
    double numberDecimals = pow(10, decimals);
    double decimalRound = round(decimalPart * numberDecimals);

    return static_cast<double> (intPart + (decimalRound / numberDecimals));
}

void ObjectSwitch::objectStatesCallback(
        const arcas_msgs::ObjectStateEstimationWithCovarianceStampedPtr &o)
{
    arcas_msgs::ObjectStateEstimationWithCovarianceStamped percepObj = *o;
    arcas_msgs::ObjectStateEstimationWithCovarianceStamped safeObj;
    ros::Publisher pub;

    ///Know the associated id
    std::string viconId;
    std::map<uint8_t, std::string> ::iterator it;
    it = rosViconIdMap_.find(percepObj.object_state_estimation_with_covariance.identifier);
    if(it != rosViconIdMap_.end()){
        viconId = it->second;
    } else {
        ROS_ERROR("ObjectSwitch::objectStatesCallback - This ROS identifier is not registered: %d"
                  , percepObj.object_state_estimation_with_covariance.identifier);
        return;
    }

    ///Know the associated obj in Vicon in the same frame that ROS
    arcas_msgs::ObjectStateEstimationWithCovarianceStamped viconObj;
    std::map<std::string, arcas_msgs::ObjectStateEstimationWithCovarianceStamped> map =
            viconUDPReceiver_->getViconObjMap();
    std::map<std::string,
            arcas_msgs::ObjectStateEstimationWithCovarianceStamped> ::iterator itObj;
    itObj = map.find(viconId);
    if(itObj != map.end()){
        viconObj = itObj->second;
    } else {
        if(activedSafety_)
        {
            ROS_ERROR("ObjectSwitch::objectStatesCallback - There is not Vicon Object: %s",
                      viconId.c_str());
            return;
        } else {
            ROS_WARN("ObjectSwitch::objectStatesCallback - There is not Vicon Object: %s",
                     viconId.c_str());
        }
    }
    ///Transform to the correct frame using the map
    transform(viconObj, percepObj);

    ///Apply the threshold and Send the safety obj
    pub = objs_states_pub_.find(
                percepObj.object_state_estimation_with_covariance.identifier)->second;
    uint8_t current_rosId = percepObj.object_state_estimation_with_covariance.identifier;
    if(activedSafety_)
    {
        std::map<uint8_t, bool> ::iterator itlastObjWasVicon;
        itlastObjWasVicon = lastObjWasVicon_.find(current_rosId);
        bool lastObjWasVicon = false;
        if(itlastObjWasVicon != lastObjWasVicon_.end()){
            lastObjWasVicon = itlastObjWasVicon->second;
        }

        if((distance(percepObj.object_state_estimation_with_covariance.position,
                     viconObj.object_state_estimation_with_covariance.position) > distThreshold_)
                || ((percepObj.object_state_estimation_with_covariance.attitude.roll -
                     viconObj.object_state_estimation_with_covariance.attitude.roll) > rollThreshold_)
                || ((percepObj.object_state_estimation_with_covariance.attitude.pitch -
                     viconObj.object_state_estimation_with_covariance.attitude.pitch) > pitchThreshold_)
                || ((percepObj.object_state_estimation_with_covariance.attitude.yaw -
                     viconObj.object_state_estimation_with_covariance.attitude.yaw) > yawThreshold_))
        {
            //publish Vicon
            viconObj.header = percepObj.header;
            viconObj.object_state_estimation_with_covariance.identifier =
                    percepObj.object_state_estimation_with_covariance.identifier;
            viconObj.object_state_estimation_with_covariance.object_type =
                    percepObj.object_state_estimation_with_covariance.object_type;
            safeObj = viconObj;

            if(lastObjWasVicon){
                isThereSwitch_[current_rosId] = false;
            } else {
                ROS_INFO("[ObjectSwitch_List::objectStatesCallback] - Switching to the VICON measurements "
                         "for the object %d", current_rosId);
                isThereSwitch_[current_rosId] = true;
                lastObjWasVicon_[current_rosId] = true;
            }
        } else {
            //publish Perception
            safeObj = percepObj;

            if(lastObjWasVicon){
                ROS_INFO("[ObjectSwitch_List::objectStatesCallback] - Switching to the "
                         "perception measurements for the object %d", current_rosId);
                isThereSwitch_[current_rosId] = true;
                lastObjWasVicon_[current_rosId] = false;
            } else {
                isThereSwitch_[current_rosId] = false;
            }
        }

        std::map<uint8_t, std::deque<arcas_msgs::ObjectStateEstimationWithCovarianceStamped> > ::iterator itlast;
        itlast = last_.find(current_rosId);
        std::deque<arcas_msgs::ObjectStateEstimationWithCovarianceStamped> currentLast;
        if(itlast != last_.end()){
            currentLast = itlast->second;
        }
        if(currentLast.size() > filterSize_){
            currentLast.pop_front();
        }

        if(isThereSwitch_.find(current_rosId)->second){
            ROS_INFO("[ObjectSwitch_List::objectStatesCallback] - Initializing "
                     "filter for the object %d", current_rosId);
            applyTimes_[current_rosId] = filterSize_;
        }

        currentLast.push_back(safeObj);
        last_[current_rosId] = currentLast;

        int currentApplyTimes = applyTimes_[current_rosId];
        //                ROS_INFO("[ObjectSwitch_List::objectStatesCallback] - Getting "
        //                         "currentApplyTimes %d for the object %d",currentApplyTimes, current_rosId);
        if(currentApplyTimes != 0){
            ROS_INFO("[ObjectSwitch::objectStatesCallback] - Applying filter time: %d of the object %d",
                     currentApplyTimes, current_rosId);
            double mX = 0;
            double mY = 0;
            double mZ = 0;
            double mR = 0;
            double mP = 0;
            double mYw = 0;
            for(unsigned int i = 0; i < currentLast.size(); i++){
                mX += currentLast[i].object_state_estimation_with_covariance.position.x;
                mY += currentLast[i].object_state_estimation_with_covariance.position.y;
                mZ += currentLast[i].object_state_estimation_with_covariance.position.z;

                mR += currentLast[i].object_state_estimation_with_covariance.attitude.roll;
                mP += currentLast[i].object_state_estimation_with_covariance.attitude.pitch;
                mYw += currentLast[i].object_state_estimation_with_covariance.attitude.yaw;
            }
            //Average position
            safeObj.object_state_estimation_with_covariance.position.x = mX/currentLast.size();
            safeObj.object_state_estimation_with_covariance.position.y = mY/currentLast.size();
            safeObj.object_state_estimation_with_covariance.position.z = mZ/currentLast.size();
            //Average attitude
            mR = mR/currentLast.size();
            mP = mP/currentLast.size();
            mYw = mYw/currentLast.size();
            //         while(mR > M_PI/2)
            //            mR -= M_PI;
            //         while(mR < -M_PI/2)
            //            mR += M_PI;
            //         while(mP > M_PI/2)
            //            mP -= M_PI;
            //         while(mP < -M_PI/2)
            //            mP += M_PI;
            //         while(mYw > M_PI)
            //            mYw -= 2*M_PI;
            //         while(mYw < -M_PI)
            //            mYw += 2*M_PI;
            safeObj.object_state_estimation_with_covariance.attitude.roll = mR;
            safeObj.object_state_estimation_with_covariance.attitude.pitch = mP;
            safeObj.object_state_estimation_with_covariance.attitude.yaw = mYw;

            currentApplyTimes--;
            applyTimes_[current_rosId] = currentApplyTimes;
        }
    } else {
        safeObj = percepObj;
    }

    pub.publish(safeObj);
}

double ObjectSwitch::distance(arcas_msgs::Position pos1, arcas_msgs::Position pos2)
{
    return sqrt(pow(pos1.x-pos2.x,2) + pow(pos1.y-pos2.y,2) + pow(pos1.z-pos2.z,2));
}


int ObjectSwitch::filterSize() const
{
    return filterSize_;
}

void ObjectSwitch::setFilterSize(int filterSize)
{
    filterSize_ = filterSize;
}

double ObjectSwitch::yawThreshold() const
{
    return yawThreshold_;
}

void ObjectSwitch::setYawThreshold(double yawThreshold)
{
    yawThreshold_ = yawThreshold;
}

double ObjectSwitch::pitchThreshold() const
{
    return pitchThreshold_;
}

void ObjectSwitch::setPitchThreshold(double pitchThreshold)
{
    pitchThreshold_ = pitchThreshold;
}

double ObjectSwitch::rollThreshold() const
{
    return rollThreshold_;
}

void ObjectSwitch::setRollThreshold(double rollThreshold)
{
    rollThreshold_ = rollThreshold;
}

double ObjectSwitch::distThreshold() const
{
    return distThreshold_;
}

void ObjectSwitch::setDistThreshold(double distThreshold)
{
    distThreshold_ = distThreshold;
}

bool ObjectSwitch::activedSafety() const
{
    return activedSafety_;
}

void ObjectSwitch::setActivedSafety(bool activedSafety)
{
    activedSafety_ = activedSafety;
    //applyTimes_ = 0; //no memory from the previous state
}
