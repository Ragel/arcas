#ifndef VICONUDPRECEIVER_H
#define VICONUDPRECEIVER_H

#include <iostream>
#include "aal/CUDPServer.h"
#include <arcas_msgs/ObjectStateEstimationWithCovarianceStamped.h>
#include "proxyTypes.h"
#include <ros/ros.h>

class ViconUDPReceiver
{
public:
    ViconUDPReceiver(int port, TviconData *data):
        port(port),
        rxData(data)
    {
        std::cerr << "Vicon UDP Receiver: starting server at port " << port << std::endl;
    }

    void thread()
    {
        struct sockaddr_in si_other;
        unsigned int slen = sizeof(si_other);
        TviconData *received_state;
        char receiveBuffer[1024];
        _udp_server.connect(port);

        while(true)
        {
            unsigned int readed;
            readed = _udp_server.receive(&receiveBuffer,sizeof(receiveBuffer),
                                         (sockaddr*)&si_other, (socklen_t*)&slen);

            if(readed < sizeof(TviconProxy) + sizeof(int)){
                std::cerr << "Vicon Receiver: Error reading udp datagram... size: " << readed
                          << " Espected: " << sizeof(TviconProxy) << std::endl;
            }
            else
            {
                received_state = (TviconData*)&receiveBuffer;
                //Fill message
                memcpy(rxData, received_state,sizeof(int)+sizeof(TviconProxy)*received_state->structNumber);
                memcpy(rxData, received_state,sizeof(TviconData));
                for(int i=0; i<received_state->structNumber; i++){
                    arcas_msgs::ObjectStateEstimationWithCovarianceStamped obj;
                    obj.object_state_estimation_with_covariance.position.x = rxData->data[i].dPosition[0]/1000.0;
                    obj.object_state_estimation_with_covariance.position.y = rxData->data[i].dPosition[1]/1000.0;
                    obj.object_state_estimation_with_covariance.position.z = rxData->data[i].dPosition[2]/1000.0;
                    obj.object_state_estimation_with_covariance.attitude.roll = rxData->data[i].dAttitude[0];
                    obj.object_state_estimation_with_covariance.attitude.pitch = rxData->data[i].dAttitude[1];
                    obj.object_state_estimation_with_covariance.attitude.yaw = rxData->data[i].dAttitude[2];

                    std::map<std::string, arcas_msgs::ObjectStateEstimationWithCovarianceStamped> ::iterator it;
                    it = current_objs_.find(std::string(rxData->data[i].name));
                    if(it == current_objs_.end()){
                        ROS_INFO("Vicon Receiver - Inserting new object: %s", rxData->data[i].name);
                        current_objs_.insert(
                                    std::pair<std::string,
                                    arcas_msgs::ObjectStateEstimationWithCovarianceStamped>(
                                        std::string(rxData->data[i].name),obj));
                        ROS_INFO("Vicon Receiver - Map Size: %d", (int)current_objs_.size());
                    } else {
                        //ROS_INFO("Vicon Receiver - Updating object: %s", rxData->data[i].name);
                        it->second = obj;
                        //ROS_INFO("Vicon Receiver - Map Size: %d", current_objs_.size());
                    }
                }
                //std::cerr << "Qnx Receiver: Receiving TviconProxy size: " << sizeof(TviconData) << std::endl;
            }
        }
    }

    std::map<std::string, arcas_msgs::ObjectStateEstimationWithCovarianceStamped> getViconObjMap(){
        //ROS_INFO("Vicon UDP Receiver Map Size: %d", current_objs_.size());
        return current_objs_;
    }

private:
    int port;
    CUDPServer _udp_server;
    TviconData *rxData;

    /*!
   * \brief Current objects from Vicon.
   */
    std::map<std::string, arcas_msgs::ObjectStateEstimationWithCovarianceStamped> current_objs_;
};

#endif // VICONUDPRECEIVER_H
