/*!
 * \file
 * \brief It switchs the object state between the estimation from Vicon or
 * perception based on a configurable threshold.
 */

#pragma once

#define VICON_PORT 11002 //11003 used in ROS

#include <arcas_msgs/ObjectStateEstimationWithCovarianceStamped.h>
#include <aal/Viconudpreceiver.h>
#include <boost/thread.hpp>
#include <ros/ros.h>
#include <tf2/LinearMath/Transform.h>
#include <arcas_msgs/object_list.h>


/*!
 * \brief Control the arm in gazebo
 */
class ObjectSwitch_List
{
public:
   ObjectSwitch_List(ros::NodeHandle n, double distThreshold,
                double rollThreshold, double pitchThreshold,
                double yawThreshold,
                int filterSize, char* file);
   virtual ~ObjectSwitch_List();

   double distThreshold() const;
   void setDistThreshold(double distThreshold);

   double rollThreshold() const;
   void setRollThreshold(double rollThreshold);

   double pitchThreshold() const;
   void setPitchThreshold(double pitchThreshold);

   double yawThreshold() const;
   void setYawThreshold(double yawThreshold);

   int filterSize() const;
   void setFilterSize(int filterSize);

   bool activedSafety() const;
   void setActivedSafety(bool activedSafety);

private:
   void transform(arcas_msgs::ObjectStateEstimationWithCovarianceStamped &viconObj,
                  const arcas_msgs::ObjectStateEstimationWithCovarianceStamped percepObj);
   static tf2::Quaternion getFromEuler123(double roll, double pitch, double yaw);
   static double getRoll123FromQuat(tf2::Quaternion quaternion);
   static double getPitch123FromQuat(tf2::Quaternion quaternion);
   static double getYaw123FromQuat(tf2::Quaternion quaternion);
   static double roundDecimal(const double number, const int decimals);
   static double distance(arcas_msgs::Position pos1,
                                 arcas_msgs::Position pos2);
   /*!
    * \brief Receives the object state estimation from perception nodes.
    * \param joint states sended from gazebo
    */
   void objectStatesCallback(
         const arcas_msgs::object_listPtr &o);

   bool activedSafety_;

   /*!
    * \brief In meters
    */
   double distThreshold_;
   /*!
    * \brief In radians
    */
   double rollThreshold_;
   double pitchThreshold_;
   double yawThreshold_;

   /// Vicon reception
   ViconUDPReceiver* viconUDPReceiver_;
   TviconData viconData_;
   boost::thread *udpReceiverThread_;

   /*!
    * \brief Subscribers to object state topics
    */
   std::map<uint8_t,ros::Subscriber> objs_states_sub_;

   /*!
    * \brief Publishers of the safety objects
    */
   std::map<uint8_t,ros::Publisher> objs_states_pub_;

   /*!
    * \brief Associated ROS and Vicon identifiers.
    */
   std::map<uint8_t, std::string> rosViconIdMap_;

   /*!
    * \brief The object ROS identifiers associated with
    * the transformation to apply to the object from Vicon.
    */
   std::map<uint8_t, tf2::Quaternion > rosIdQuatMap_;
   std::map<uint8_t, tf2::Vector3 > rosIdTranslationMap_;

   /// Average Filter: key rosId
   std::map<uint8_t,bool> isThereSwitch_;
   std::map<uint8_t,bool> lastObjWasVicon_;
   std::map<uint8_t,int> applyTimes_;
   std::map<uint8_t,std::deque<arcas_msgs::ObjectStateEstimationWithCovarianceStamped> > last_;
   int filterSize_;

};

