#ifndef PROXYTYPES_H
#define PROXYTYPES_H

#include "AutopilotGlobal_types.h"

//#pragma pack(1) In workstation, it crashes the string variables
typedef struct
{
    TState state;
    char  name[32];
    float dAttitude[3];
    float dPosition[3];
    unsigned int uiHeartbeat;

}TviconProxy;

typedef struct
{
    int structNumber;
    TviconProxy data[6];
}TviconData;


#endif // PROXYTYPES_H
