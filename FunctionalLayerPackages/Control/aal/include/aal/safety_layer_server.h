/*************************************************************************
 *
 * FADA-CATEC
 * __________________
 *
 *  [2013] FADA-CATEC
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of FADA-CATEC and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to FADA-CATEC
 * and its suppliers and may be covered by Europe and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from FADA-CATEC.
 *
 * Created on: 11-Nov-2014
 * Engineer: Yamnia Rodriguez
 * Email: yrodriguez@catec.aero
 */

#pragma once

#include<ros/ros.h>
#include<actionlib/server/action_server.h>
#include<arcas_actions_msgs/SafetyLayerAction.h>
#include "aal/object_switch_list.h"

class SafetyLayerServer
{

public:
   SafetyLayerServer(ObjectSwitch_List &safetySwitch) :
      as_(nh_,"safety_aal",false)
    ,safetySwitch(safetySwitch)
   {
      hasGoal =false;
      as_.registerGoalCallback(boost::bind(&SafetyLayerServer::goalCB, this, _1));
      as_.registerCancelCallback(boost::bind(&SafetyLayerServer::preemptCB, this, _1));
      as_.start();
   }

   ~SafetyLayerServer()
   {

   }

   bool getHasGoal()
   {
      return hasGoal;
   }

   void finishAction()
   {
      hasGoal = false;
      goal_handle_.setSucceeded();
   }

private:
   void goalCB(actionlib::ServerGoalHandle<arcas_actions_msgs::SafetyLayerAction> goal_handle)
   {
      //TakeOff advertise;
      if(hasGoal)
      {
         goal_handle.setRejected();
      }else
      {
         goal_handle_ = goal_handle;
         hasGoal = true;
         switch(goal_handle.getGoal()->action)
         {
         case arcas_actions_msgs::SafetyLayerGoal::STOP:
         {
            ROS_INFO("[SafetyLayerAction::goalCB] - AAL action received: STOP");
            safetySwitch.setActivedSafety(false);
            break;
         }
         case arcas_actions_msgs::SafetyLayerGoal::INIT:
         {
            ROS_INFO("[SafetyLayerAction::goalCB] - AAL action received: INIT:\n\t"
                     "Distance threshold: %f\n\t"
                     "Roll threshold: %f\n\t"
                     "Pitch threshold: %f\n\t"
                     "Yaw threshold: %f\n\t"
                     "Filter size: %d\n\t",
                     goal_handle.getGoal()->distThreshold, goal_handle.getGoal()->rollThreshold,
                     goal_handle.getGoal()->pitchThreshold, goal_handle.getGoal()->yawThreshold,
                     goal_handle.getGoal()->filterSize);
            safetySwitch.setDistThreshold(goal_handle.getGoal()->distThreshold);
            safetySwitch.setRollThreshold(goal_handle.getGoal()->rollThreshold);
            safetySwitch.setPitchThreshold(goal_handle.getGoal()->pitchThreshold);
            safetySwitch.setYawThreshold(goal_handle.getGoal()->yawThreshold);
            safetySwitch.setFilterSize(goal_handle.getGoal()->filterSize);
            safetySwitch.setActivedSafety(true);
            break;
         }
         default:
         {
            ROS_WARN("[SafetyLayerAction::goalCB] - AAL action received: %d. "
                     "It is not implemented", goal_handle.getGoal()->action);
         }
         }

         goal_handle.setAccepted();
         hasGoal = false;
      }

   }
   void preemptCB(actionlib::ServerGoalHandle<arcas_actions_msgs::SafetyLayerAction> goal_handle)
   {
      ROS_INFO("[SafetyLayerAction::preemptCB] - It does not have functionality");
   }


protected:

   ros::NodeHandle nh_;
   actionlib::ActionServer<arcas_actions_msgs::SafetyLayerAction> as_;
   actionlib::ServerGoalHandle<arcas_actions_msgs::SafetyLayerAction> goal_handle_;
   bool hasGoal;

   ObjectSwitch_List &safetySwitch;
};
