/*!
 * \file
 * \brief The main class for the perception aided quad pose estimation node.
 */

#pragma once
#include <ros/ros.h>
#include <arcas_module_main/AARCASModuleMain.h>
#include <ro_slam_perception/ro_slam_perception_communication.h>
#include <ro_slam_perception/actions/manipulation_server.h>

/*!
 * \brief The execution rate of this node.
 */
#define MODULE_RATE 100.0

/*!
 * \brief The ROS namespace for this node.
 */
#define MODULE_NAMESPACE "perception"

/*!
 * \brief The general namespace to ARCAS project.
 */
using namespace arcas_msgs;


/*!
 * \brief The main class for the perception aided quad pose estimation node.
 *
 * The code has to be added in the mainLoop() method.
 */
class RoSlamPerception : public AARCASModuleMain,
      public RoSlamPerceptionCommunication
{
public:
   RoSlamPerception(ros::NodeHandle *n, int objs);
   ~RoSlamPerception();
private:
   void mainLoop(const ros::TimerEvent& te);

   /*!
    * \brief Action server for manipulation task.
    */
   ManipulationServer manipulation_action_server_;

};
