/*!
 * \file
 * \brief The communication class for the state estimation node.
 */
#pragma once
#include <ros/ros.h>
#include <arcas_msgs/QuadStateEstimationWithCovarianceStamped.h>
#include <arcas_msgs/ObjectStateEstimationWithCovarianceStamped.h>

/*!
 * \brief The topic name for the quad state message from qnx.
 */
#define QUAD_STATE_QNX_TOPIC "quad_state_estimation"

/*!
 * \brief The topic name to publish the quad aided state message.
 */
#define QUAD_STATE_AIDED_TOPIC "quad_aided_state_estimation"

/*!
 * \brief The topic name to publish the quad aided state message.
 */
#define OBJECT_STATE_AIDED_TOPIC "object_aided_state_estimation"

/*!
 * \brief The general message namespace on ARCAS project.
 */
using namespace arcas_msgs;

class RoSlamPerceptionCommunication
{
public:
   /*!
   * \brief RoSlamPerceptionCommunication
   * \param n
   * \param objs Object number that will be estimated.
   * It indicated the number of the publishers and data to create
   * in the class like vectors.
   * \warning The publisher i should be publish the
   * data i.
   */
  RoSlamPerceptionCommunication(ros::NodeHandle *n, int objs = 1);

  /*!
   * \brief This method will be called at constant rate.
   */
  void publishAll();

private:
  /*!
   * \brief Callback to receive Quad State Estimation data
   * \param s The received message of the quad state estimation from ROS.
   */
  void quadStateCallback(const QuadStateEstimationWithCovarianceStampedConstPtr &s);

  /*!
   * \brief Ros subscriber to receive quad state estimation
   */
  ros::Subscriber quad_state_qnx_subscriber_;

  /*!
   * \brief ROS Publisher to publish the aided quad state estimation.
   */
  ros::Publisher quad_state_aided_publisher_;

  /*!
   * \brief ROS Publisher to publish the object state estimation.
   */
  std::vector<ros::Publisher> object_state_aided_publisher_;

protected:

  int objs_;

  std::vector<uint8_t> moved_obj_;

  /*!
   * \brief This is the quad data that have to be send.
   */
  QuadStateEstimationWithCovarianceStamped quad_aided_state_;

  /*!
   * \brief This is the object data that have to be send.
   */
  std::vector<ObjectStateEstimationWithCovarianceStamped> object_aided_state_;

  /*!
   * \brief Used to hold the last quad state estimation message from qnx
   */
  QuadStateEstimationWithCovarianceStamped quad_last_qnx_state_;

};

