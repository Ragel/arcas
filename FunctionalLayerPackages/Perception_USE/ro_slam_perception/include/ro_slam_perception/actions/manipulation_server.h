/*************************************************************************
 *
 * FADA-CATEC
 * __________________
 *
 *  [2013] FADA-CATEC
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of FADA-CATEC and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to FADA-CATEC
 * and its suppliers and may be covered by Europe and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from FADA-CATEC.
 *
 * Created on: 23-Oct-2012
 * Engineer: Jonathan Ruiz Páez
 * Email: jruiz@catec.aero
 */

#pragma once

#include<ros/ros.h>
#include<actionlib/server/action_server.h>
#include<arcas_actions_msgs/ManipulationAction.h>


class ManipulationServer
{

public:
   ManipulationServer() :
      as_(nh_,"manipulation_server_ro_slam",false)
   {
      hasGoal =false;
      as_.registerGoalCallback(boost::bind(&ManipulationServer::goalCB, this, _1));
      as_.registerCancelCallback(boost::bind(&ManipulationServer::preemptCB, this, _1));
      as_.start();
   }

   ~ManipulationServer()
   {

   }

   bool getHasGoal()
   {
      return hasGoal;
   }

   /*!
    * \brief getIdentifier
    * \return The object identifier that will be moved.
    * \warning This method
    * should be called when the getHasGoal() returns true.
    */
   uint8_t getIdentifier()
   {
      return object_identifier_to_delete;
   }

   void finishAction()
   {
      hasGoal = false;
      goal_handle_.setSucceeded();
   }

private:
   void goalCB(actionlib::ServerGoalHandle<arcas_actions_msgs::ManipulationAction> goal_handle)
   {
      //TakeOff advertise;
      if(hasGoal)
      {
         goal_handle.setRejected();
      }else
      {
         goal_handle_ = goal_handle;
         hasGoal = true;
         object_identifier_to_delete = goal_handle.getGoal()->bar_id;
         ROS_INFO("[ManipulationServer::goalCB] - RO-SLAM - Identifier received: %d",
                  object_identifier_to_delete);
         goal_handle.setAccepted();
      }

   }
   void preemptCB(actionlib::ServerGoalHandle<arcas_actions_msgs::ManipulationAction> goal_handle)
   {
      ROS_INFO("[ManipulationServer::preemptCB] - RO-SLAM - It does not have functionality");
   }


protected:

   ros::NodeHandle nh_;
   actionlib::ActionServer<arcas_actions_msgs::ManipulationAction> as_;
   actionlib::ServerGoalHandle<arcas_actions_msgs::ManipulationAction> goal_handle_;
   bool hasGoal;
   uint8_t object_identifier_to_delete;
};
