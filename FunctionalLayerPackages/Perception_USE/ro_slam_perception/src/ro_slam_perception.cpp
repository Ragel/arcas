#include <ro_slam_perception/ro_slam_perception.h>

RoSlamPerception::RoSlamPerception(ros::NodeHandle *n, int objs):
   AARCASModuleMain(n, MODULE_RATE),
   RoSlamPerceptionCommunication(n,objs)
{

}

RoSlamPerception::~RoSlamPerception()
{

}

void RoSlamPerception::mainLoop(const ros::TimerEvent& te)
{

   //TODO: Your code here

   //TODO: Update messages to send
   //EXAMPLE
   QuadStateEstimationWithCovarianceStamped idle_quad_aided_state;
   quad_aided_state_ = idle_quad_aided_state;
   object_aided_state_.clear();
   ObjectStateEstimationWithCovarianceStamped idle_obj_state;
   object_aided_state_.push_back(idle_obj_state);

   if(manipulation_action_server_.getHasGoal()){
      int identifier_to_delete = manipulation_action_server_.getIdentifier();
      moved_obj_.push_back(identifier_to_delete);

      //TODO: Your code here
      //Filter your data with identifier_to_delete


      manipulation_action_server_.finishAction();
   }

   //Send message
   this->publishAll();
}

