#include<ro_slam_perception/ro_slam_perception.h>

int main(int argc, char** argv)
{

   ros::init(argc, argv, "ro_slam_perception");

   ros::NodeHandle n(MODULE_NAMESPACE);

   RoSlamPerception ro_slam_perception(&n, 1);

   ros::spin();

   //Finish.

   return 0;
}
