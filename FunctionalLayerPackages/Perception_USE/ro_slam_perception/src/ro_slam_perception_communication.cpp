#include <ro_slam_perception/ro_slam_perception_communication.h>

RoSlamPerceptionCommunication::RoSlamPerceptionCommunication(ros::NodeHandle *n, int objs):
   objs_(objs)
{
   /*
    Initialize subscribers
    */
   quad_state_qnx_subscriber_ = n->subscribe(QUAD_STATE_QNX_TOPIC, 1,
                                             &RoSlamPerceptionCommunication::quadStateCallback,
                                             this);

   /*
    Initialize publishers
    */
   quad_state_aided_publisher_ = n->
         advertise<QuadStateEstimationWithCovarianceStamped>(QUAD_STATE_AIDED_TOPIC,1);
   for(int i = 0; i< objs_; i++){
      std::ostringstream topic;
      topic << OBJECT_STATE_AIDED_TOPIC << "_" << i;
      ros::Publisher obj_publisher_ = n->
            advertise<ObjectStateEstimationWithCovarianceStamped>(topic.str(),1);
      object_state_aided_publisher_.push_back(obj_publisher_);
   }
}

void RoSlamPerceptionCommunication::quadStateCallback(const QuadStateEstimationWithCovarianceStampedConstPtr &s)
{
   quad_last_qnx_state_ = *s;
}

void RoSlamPerceptionCommunication::publishAll()
{
   quad_state_aided_publisher_.publish(quad_aided_state_);
   for(int i = 0; i< objs_; i++){
      if(std::find(moved_obj_.begin(), moved_obj_.end(), i)==moved_obj_.end()){
         object_state_aided_publisher_[i].publish(object_aided_state_[i]);
      }
   }
}
